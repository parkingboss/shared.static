import { Address, Property, Payload } from '.';
import { Items } from '../utils/helpers';

export interface PropertySummary extends Payload {
    locations: {
        item: string;
        items: Items<Property>;
    };
    addresses: {
        item: string;
        items: Items<Address>;
    };
}