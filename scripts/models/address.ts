import { Property } from '.';
import { DateString } from '../utils/date_string';
import { Items } from '../utils/helpers';
import { File } from './file';

export function propertyAddress(property: Property, addresses: Items<Address>): Address {
  if (typeof property.address === 'string') {
    return addresses[property.address];
  } else {
    return property.address;
  }
}

export function cityAndState(address: Address): string {
  return `${address.city}, ${address.state}`;
}

export function latLng(address: Address): { lat: number, lng: number } {
  return { lat: address.latitude, lng: address.longitude };
}

export interface Address {
  id: string;
  uuid: string;
  name: string;
  street: string;
  city: string;
  state: string;
  country: string | null;
  postal: string;
  timezone: {
    id: string,
    iana: string,
    standard: null | {
        name: string,
        offset: string,
    },
    description: null | string,
  };
  latitude: number;
  longitude: number;
  rules: string;
  disclaimer: string;
  instructions: string;
  map: {
    url: null | string,
    qr: string,
    image: string,
  };
  photo: null | File;
  logo: null | File;
  updated: DateString;
}
