import { DateString } from '../utils/date_string';

export interface Payload {
    generated: DateString;
    version: number;
    viewpoint: DateString;
    server: string;
}