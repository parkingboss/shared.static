import { DateString } from '../utils/date_string';
import { Address } from './address';

export interface Property {
    id: string;
    uuid: string;
    name: string;
    address: Address|string;
    url: string;
    created: DateString;
    updated: DateString;
    cached?: DateString;
    admin?: string;
    media: false | {
        enabled: boolean;
        permits: {
            title: string;
        };
        type: string;
        title: string;
        format: string;
        predefined: boolean;
    };
    spaces: false | {
        enabled: boolean;
        permits: boolean;
        predefined: boolean;
        title: 'Space';
    };
    tenants: false | {
        enabled: boolean;
        permits?: {
            title: string;
            selfservice: false | {
                title: string;
            };
        },
        type: string,
        title: string,
        predefined: boolean;
        residents: boolean;
        donotpermit: boolean;
        permittable: {
            negative: boolean;
            positive: boolean;
        },
        authentication: boolean;
        selfservice: {
            enabled: boolean;
        }
    };
    vehicles: {
        enabled: boolean;
        permits: boolean;
        predefined: boolean;
        permittable: {
            negative: boolean;
            positive: boolean;
        },
        title: 'Vehicle'
    };
    attendant: false | {
        permits: {
            create: boolean;
            title: string;
            selfservice?: {
                title: string;
                list: false | {
                    enabled: boolean;
                    exempt: boolean;
                };
            };
            list: boolean;
        };
        title: string;
        url: string;
        qr: string;
    };
    violations: {
        enabled: boolean;
        thresholds?: string;
        reasons: string[];
        photos: {
            request: boolean;
            required: number;
        };
        vehicle: {
            request: boolean;
            required: boolean;
            infer: {
                tenant: boolean;
            }
        };
        tenant: {
            request: boolean;
            required: boolean;
        };
    };
    users: {
        fieldagents: boolean;
        patrollers: false | {
            enabled: boolean;
            tenants: boolean;
            temporary: boolean;
            continuous: boolean;
        };
    };
    send: {
        email: boolean;
        sms: boolean;
    };
    reports: boolean;
    paid: boolean;
    contact: {
        custom?: string;
        name?: string;
        phone?: string;
        tel?: string;
    };
}
