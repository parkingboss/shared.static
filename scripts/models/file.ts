import { DateString } from '../utils/date_string';

export interface File {
    id: string;
    uuid: string;
    title: string;
    name: string;
    type: string;
    length: number;
    created: DateString;
    url: string;
    upload: null | string;
}