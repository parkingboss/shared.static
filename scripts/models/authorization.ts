import { Role } from '.';
import { DateRangeString } from '../utils/date_string';
import { Action } from './action';

export interface Authorization {
    type: 'authorization';
    id: string;
    uuid: string;
    target: string;
    subject: string;
    scope: string;
    roles: { [K in Role ]?: boolean };
    valid: {
        interval: DateRangeString;
        min: null | false | Action;
        max: null | false | Action;
    };
}