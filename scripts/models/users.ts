import { Authorization, Contact, Property, Payload } from '.';
import { Items } from '../utils/helpers';

export interface Users extends Payload {
    frontdesk: boolean;
    users: {
        count: number;
        items: Items<Contact>;
    };
    locations: {
        count: number;
        items: Items<Property>
    };
    authorizations: {
        count: number;
        items: Items<Authorization>;
    };
}