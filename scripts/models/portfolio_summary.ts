import { DateRangeString } from '../utils/date_string';
import { Property } from './property';
import { Payload } from '.';

export interface PortfoliosSummary extends Payload {
    effective: DateRangeString;

    locations: {
        count: number;
        items: {
            [key: string]: Property;
        };
    };

    portfolios: {
        count: number;
        items: {
            [id: string]: {
                id: string;
                uuid: string;
                name: string;
                src: string;
            },
        };
    };
}
