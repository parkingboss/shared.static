import { DateString } from '../utils/date_string';

export interface Action {
    title: string;
    datetime: DateString;
    by: null | string;
}