export type Role = 'owner' | 'admin' | 'patrol';

export function isRole(x: any): x is Role {
    return x === 'owner' || x === 'admin' || x === 'patrol';
}
