import { Items } from '../utils/helpers';
import { Payload, Address } from '.';

export interface PropertiesSummary extends Payload {
    locations: {
        count: number;
        items: Items<string>;
    };
    addresses: {
        count: number;
        items: Items<Address>;
    };
}
