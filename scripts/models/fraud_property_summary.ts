import { DateRangeString } from '../utils/date_string';
import { Items } from '../utils/helpers';
import { PropertySummary } from './property_summary';

export interface FlaggedPlates {
    permit: string;
    tenant: string;
    evaluated: Array<{
        fingerprints: {
            items: string;
        };
        observation: {
            confirmed: boolean;
            count: number;
        };
        subject: boolean;
        vehicle: string;
        version: number;
    }>;
}

export interface Displayable {
    id: string;
    display: string;
    key: string;
    scope: string;
    type: string;
    uuid: string;
}

export interface FraudPropertySummary extends PropertySummary {
    attendants: {
        item: string;
        items: Items<any>;
    };
    suspected: {
        count: number;
        enabled: boolean;
        scope: string;
        valid: DateRangeString;
        items?: Items<FlaggedPlates>;
    };
    permits: Items<any>;
    vehicles: {
        count: number;
        items: Items<Displayable>;
    };
    tenants: {
        residents: boolean;
        predefined: boolean;
        type: string;
        items: Items<Displayable>;
    };
}