import { DateRangeString } from '../utils/date_string';
import { Items } from '../utils/helpers';
import { Authorization, Payload, Bearer, Contact } from '.';

export interface Authorizations extends Payload {
    authorizations: {
        count: number,
        items: Items<Authorization>,
        valid: DateRangeString|null,
        scopes?: [string],
        granted?: string[],
        revoked?: string[],
    };
    users: {
        count: number,
        items: Items<Contact>,
        auth?: {
            items: Items<Bearer>,
        },
    };
}
