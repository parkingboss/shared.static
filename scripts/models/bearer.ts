import { DateString } from '../utils/date_string';

export interface Bearer {
    type: 'bearer';
    token: string;
    lifetime: number;
    expires: DateString;
    subject: string;
}