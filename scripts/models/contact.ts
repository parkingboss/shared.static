import { DateString } from '../utils/date_string';

export interface Contact {
    type: 'contact';
    id: string;
    uuid: string;
    scope: 'AAAAAAAAAAAAAAAAAAAAAA'; // all 0s GUID
    name: string;
    display: string;
    email: string;
    created: DateString;
    updated: DateString;
}