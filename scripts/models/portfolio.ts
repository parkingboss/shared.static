import { DateRangeString } from '../utils/date_string';
import { Items } from '../utils/helpers';
import { Payload, Property, Address } from '.';

export interface Portfolio extends Payload {
    effective: DateRangeString;

    portfolios: {
        item: string;
        items: Items<{ id: string, uuid: string, name: string }>;
    };

    locations: {
        count: number;
        items: Items<Property>;
    };

    addresses: {
        count: number;
        items: {
            [id: string]: Address;
        }
    };
}
