import { div, script } from './dom';

declare global {
    const MAPS_KEY: string;
    interface Window {
        _mapsInit: () => void;
    }
}

export interface MapConstants {
    map: google.maps.Map;
    info: google.maps.InfoWindow;
    markers: ReadonlyArray<google.maps.Marker>;
    setMarkers: (newMarkers: google.maps.Marker[]) => void;
    setBounds: (positions: Array<google.maps.LatLngLiteral | google.maps.LatLng>) => void;
}

export const mapDiv = div({ id: 'map' });

export const loadGoogleMaps = new Promise<MapConstants>(resolve => {
    window._mapsInit = function() {
        const map = new google.maps.Map(mapDiv, {
            mapTypeControl: false,
            center: { lat: 0, lng: 0 },
            zoom: 8,
            zoomControl: false,
            scaleControl: false,
            streetViewControl: false,
            fullscreenControl: false,
        });
        const info = new google.maps.InfoWindow();
        const markers: google.maps.Marker[] = [];
        resolve({
            map,
            info,
            markers,
            setMarkers(newMarkers: google.maps.Marker[]): void {
                markers.forEach(m => m.setMap(null));
                markers.splice(0, markers.length, ...newMarkers);
                markers.forEach(m => m.setMap(map));
            },
            setBounds(positions: Array<google.maps.LatLngLiteral|google.maps.LatLng>) {
                const bounds = new google.maps.LatLngBounds();
                positions.forEach(p => bounds.extend(p));
                map.fitBounds(bounds);
                map.panToBounds(bounds);
            },
        });
    };
});


document.body.appendChild(
    script({
        src: `https://maps.googleapis.com/maps/api/js?key=${MAPS_KEY}&callback=_mapsInit`,
        async: true,
        defer: true,
    }),
);
