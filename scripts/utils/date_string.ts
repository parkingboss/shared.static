import { isAfter, isValid, isWithinRange } from 'date-fns';

// This error is thrown when date strings are not correctly formatted
export class DateFormatError extends Error {
}

/* DateString: a string, formatted like an ISO_8601 date */

enum DateStringBrand { }

export type DateString = string & DateStringBrand;

function checkValidDateString(str: string, emptyOk?: boolean): str is DateString {
    if (emptyOk && str === '') return true;
    return !isNaN(Date.parse(str));
}

export function toDateString(date: Date | string): DateString {
    if (date instanceof Date) {
        date = date.toISOString();
    }
    if (checkValidDateString(date)) {
        return date;
    }
    throw new DateFormatError(`'${date}' is not an ISO_8601 date string`);
}

/* DateRangeString: a string formatted like a pair of ISO_8601 date joined by '/' */

enum DateRangeStringBrand { }

export type DateRangeString = string & DateRangeStringBrand;

function checkValidDateRangeString(str: string): str is DateRangeString {
    const [start, end, ...extra] = str.split('/');
    return extra.length === 0 && checkValidDateString(start, true) && checkValidDateString(end, true);
}

export function toDateRangeString(dateRange: string): DateRangeString {
    if (checkValidDateRangeString(dateRange)) {
        return dateRange;
    }

    throw new DateFormatError(`${dateRange} is not a valid date range string (two ISO_8601 dates separated by a '/')`);
}

export function toDateRange(input: DateRangeString): [Date, Date] {
    const [start, end] = input.split('/');
    return [
        new Date(start),
        new Date(end),
    ];
}

export function inRange(input: DateRangeString, date: Date): boolean {
    const [ start, end ] = toDateRange(input);

    return isValid(end) ? isWithinRange(date, start, end) : isAfter(date, start);
}