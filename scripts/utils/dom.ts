import { CustomEventHandler } from './events';
import { cl, Items } from './helpers';

export type Child = string | Node | ElChild | ChildArray;

interface ElChild {
    el: Child;
}

interface ChildArray extends Array<Child | false> {}

function isChildArray(x: any): x is ChildArray {
    return (x instanceof Array && x.every(isChild));
}

export function isChild(x: any): x is Child {
    return x != null &&
        (
            typeof x === 'string' ||
            x instanceof Node ||
            x.el instanceof Node ||
            isChildArray(x)
        );
}

type EventHandler = EventListener | CustomEventHandler<any>;

export interface EventOptions {
    events?: Items<EventHandler|EventHandler[]>;
}

function addEvents(target: EventTarget, opts: EventOptions) {
    const evts = opts.events || {};
    Object.keys(evts).forEach(evtName => {
        if (evts[evtName] instanceof Array) {
            const events = evts[evtName] as EventHandler[];
            events.forEach(evtHandler => {
                target.addEventListener(evtName, evtHandler as EventListener);
            });
        } else {
            target.addEventListener(evtName, evts[evtName] as EventListener);
        }
    });
}

export interface NodeOptions extends EventOptions {
    parent?: Node;
    children?: Array<Child | false>;
}

function updateNode(node: Node, opts: NodeOptions) {
    addEvents(node, opts);
    if (opts.children) {
        opts.children.forEach(c => {
            if (c instanceof Node) {
                node.appendChild(c);
            } else if (typeof c === 'string') {
                node.appendChild(text(c));
            } else if (c instanceof Array) {
                node.appendChild(fragment(...c));
            } else if (c) {
                node.appendChild(fragment(c.el));
            }
        });
    }
    if (opts.parent) opts.parent.appendChild(node);
}

export interface ElementOptions extends NodeOptions {
    id?: string;
    class?: string | Array<string | null | false | undefined>;
    scrollLeft?: number;
    scrollTop?: number;
    attrs?: Items<string>;
}

function updateElement(el: Element, opts: ElementOptions) {
    updateNode(el, opts);
    if (opts.id) el.id = opts.id;
    if (opts.class) {
        if (typeof opts.class === 'string') {
            el.className = opts.class;
        } else {
            el.className = cl(...opts.class);
        }
    }
    if (opts.scrollLeft) el.scrollLeft = opts.scrollLeft;
    if (opts.scrollTop) el.scrollTop = opts.scrollTop;
    if (opts.attrs) {
        const attrs = opts.attrs;
        Object.keys(attrs).forEach(attr => {
            el.setAttribute(attr, attrs[attr]);
        });
    }
}

export type Direction = 'ltr' | 'rtl' | 'auto';

export interface HTMLElementOptions extends ElementOptions {
    dataset?: Items<string>;
    accessKey?: string;
    contentEditable?: boolean;
    dir?: Direction;
    hidden?: boolean;
    lang?: string;
    spellcheck?: boolean;
    style?: Partial<CSSStyleDeclaration>;
    tabIndex?: number;
    title?: string;
}

function updateHtmlElement(el: HTMLElement, opts: HTMLElementOptions) {
    updateElement(el, opts);
    if (opts.accessKey) el.accessKey = opts.accessKey;
    if (opts.contentEditable != null) el.contentEditable = opts.contentEditable ? 'true' : 'false';
    if (opts.dir) el.dir = opts.dir;
    if (opts.hidden != null) el.hidden = opts.hidden;
    if (opts.lang) el.lang = opts.lang;
    if (opts.spellcheck != null) el.spellcheck = opts.spellcheck;
    if (opts.style) Object.assign(el.style, opts.style);
    if (opts.tabIndex) el.tabIndex = opts.tabIndex;
    if (opts.title) el.title = opts.title;
    if (opts.dataset) {
        const ds = opts.dataset;
        Object.keys(opts.dataset).forEach(key => {
            el.dataset[key] = ds[key];
        });
    }
}

type Updater<K, O> = (k: K, o: O) => void;
function updater<T extends HTMLElement, U extends HTMLElementOptions>(extraUpdater: (t: T, u: U) => void) {
    return (el: T, opts: U) => {
        updateHtmlElement(el, opts);
        extraUpdater(el, opts);
    };
}

type ElementCreator<U extends NodeOptions, V> = (opts?: U | Child | false, ...children: Array<Child | false>) => V;

/* NOTE! The following three functions are near identical. There's nearly certainly a way to merge them */

export function makeSvg<K extends keyof SVGElementTagNameMap, U extends NodeOptions>(
        name: K,
        defaultOpts: U,
        update: Updater<SVGElementTagNameMap[K], U> = updateNode,
    ): ElementCreator<U, SVGElementTagNameMap[K]> {
    return (opts?: U | Child | false, ...children: Array<Child|false>) => {
        if (isChild(opts) || typeof opts === 'boolean') {
            children.unshift(opts);
            opts = defaultOpts;
        } else if (typeof opts === 'undefined') {
            opts = defaultOpts;
        }
        opts.children = children;
        const val = document.createElementNS('http://www.w3.org/2000/svg', name);
        update(val, opts);
        return val;
    };
}

function make<K extends keyof HTMLElementTagNameMap, U extends HTMLElementOptions>(
        name: K,
        defaultOpts: U,
        update: Updater<HTMLElementTagNameMap[K], U>,
    ) {
    return (opts?: U | Child | false, ...children: Array<Child | false>) => {
        if (isChild(opts) || typeof opts === 'boolean') {
            children.unshift(opts);
            opts = defaultOpts;
        } else if (typeof opts === 'undefined') {
            opts = defaultOpts;
        }
        opts.children = children;

        const val = document.createElement(name);
        update(val, opts);
        return val;
    };
}

function build(name: string) {
    return (opts?: HTMLElementOptions | Child | false, ...children: Array<Child | false>) => {
        if (isChild(opts) || typeof opts === 'boolean') {
            children.unshift(opts);
            opts = {};
        } else if (typeof opts === 'undefined') {
            opts = {};
        }
        opts.children = children;

        const val = document.createElement(name);
        updateHtmlElement(val, opts);
        return val;
    };
}

interface LinkOptions extends HTMLElementOptions {
    href?: string;
    target?: string;
}
interface AnchorOptions extends LinkOptions {
    download?: string;
    rel?: string;
}
interface CitedOptions extends HTMLElementOptions {
    cite?: string;
}
interface FormElementOptions extends HTMLElementOptions {
    disabled?: boolean;
    autofocus?: boolean;
    name?: string;
    value?: string;
}
interface ButtonOptions extends FormElementOptions {
    type?: string;
    formEnctype?: string;
    formAction?: string;
}
interface InputOptions extends FormElementOptions {
    autocomplete?: 'on'|'off';
    placeholder?: string;
    checked?: boolean;
    type?: string;
    required?: boolean;
}
interface THOptions extends HTMLElementOptions {
    scope?: string;
}
interface ScriptOptions extends HTMLElementOptions {
    async?: boolean;
    defer?: boolean;
    src?: string;
    text?: string;
    type?: string;
}
interface LabelOptions extends HTMLElementOptions {
    for?: string;
}
interface FormOptions extends HTMLElementOptions {
    name?: string;
    method?: 'GET'|'POST'|'DELETE'|'PUT';
    target?: string;
    action?: string;
    enctype?: string;
    encoding?: string;
    acceptCharset?: string;
    autocomplete?: string;
    noValidate?: boolean;
}
interface SelectOptions extends FormElementOptions {
    multiple?: boolean;
    selectedIndex?: number;
    required?: boolean;
}
interface OptionOptions extends HTMLElementOptions {
    defaultSelected?: boolean;
    disabled?: boolean;
    label?: string;
    selected?: boolean;
    text?: string;
    value?: string;
}

interface ImageOptions extends HTMLElementOptions {
    src?: string;
    srcset?: string;
    sizes?: string;
}

interface IframeOptions extends HTMLElementOptions {
    src?: string;
    allow?: string;
}

interface VideoOptions extends HTMLElementOptions {
    autoplay?: boolean;
    muted?: boolean;
    playsinline?: boolean;
}

interface CanvasOptions extends HTMLElementOptions {
    width?: number;
    height?: number;
}

interface StyleOptions extends HTMLElementOptions {
    css?: string;
    media?: string;
    type?: string;
}

export const fragment = (...children: Array<false | Child>) => {
    const result = document.createDocumentFragment();
    updateNode(result, { children });
    return result;
};

export const text = (content: string) => document.createTextNode(content);

export const a = make('a', {}, updater((el, opts: AnchorOptions) => {
    if (opts.href) el.href = opts.href;
    if (opts.download) el.download = opts.download;
    if (opts.rel) el.rel = opts.rel;
    if (opts.target) el.target = opts.target;
}));
export const abbr = make('abbr', {}, updateHtmlElement);
export const address = make('address', {}, updateHtmlElement);
//export const area = make('area', {}, updateHtmlElement);
export const article = make('article', {}, updateHtmlElement);
export const aside = make('aside', {}, updateHtmlElement);
//export const audio = make('audio', {}, updateHtmlElement);
export const b = make('b', {}, updateHtmlElement);
export const base = make('base', {}, updater((el, opts: LinkOptions) => {
    if (opts.href) el.href = opts.href;
    if (opts.target) el.target = opts.target;
}));
export const bdo = make('bdo', {}, updateHtmlElement);
export const blockquote = make('blockquote', {}, updater((el, opts: CitedOptions) => {
    if (opts.cite) el.cite = opts.cite;
}));
export const body = make('body', {}, updateHtmlElement);
export const br = make('br', {}, updateHtmlElement);
export const button = make('button', {}, updater((el, opts: ButtonOptions) => {
    if (opts.autofocus) el.autofocus = opts.autofocus;
    if (opts.disabled) el.disabled = opts.disabled;
    if (opts.formAction) el.formAction = opts.formAction;
    if (opts.formEnctype) el.formEnctype = opts.formEnctype;
    if (opts.type) el.type = opts.type;
    if (opts.name) el.name = opts.name;
    if (opts.value) el.value = opts.value;
}));
export const canvas = make('canvas', {}, updater((el, opts: CanvasOptions) => {
    if (opts.width) el.width = opts.width;
    if (opts.height) el.height = opts.height;
}));
export const caption = make('caption', {}, updateHtmlElement);
export const cite = make('cite', {}, updateHtmlElement);
export const code = make('code', {}, updateHtmlElement);
export const col = make('col', {}, updateHtmlElement);
export const colgroup = make('colgroup', {}, updateHtmlElement);
export const data = make('data', {}, updateHtmlElement);
export const datalist = make('datalist', {}, updateHtmlElement);
export const dd = make('dd', {}, updateHtmlElement);
export const del = make('del', {}, updateHtmlElement);
export const details = make('details', {}, updateHtmlElement);
export const dfn = make('dfn', {}, updateHtmlElement);
export const dialog = make('dialog', {}, updateHtmlElement);
export const dir = make('dir', {}, updateHtmlElement);
export const div = make('div', {}, updateHtmlElement);
export const dl = make('dl', {}, updateHtmlElement);
export const dt = make('dt', {}, updateHtmlElement);
export const em = make('em', {}, updateHtmlElement);
export const embed = make('embed', {}, updateHtmlElement);
export const fieldset = make('fieldset', {}, updateHtmlElement);
export const figcaption = make('figcaption', {}, updateHtmlElement);
export const figure = make('figure', {}, updateHtmlElement);
export const font = make('font', {}, updateHtmlElement);
export const footer = make('footer', {}, updateHtmlElement);
export const form = make('form', {}, updater((el, opts: FormOptions) => {
    if (opts.name) el.name = opts.name;
    if (opts.method) el.method = opts.method;
    if (opts.target) el.target = opts.target;
    if (opts.action) el.action = opts.action;
    if (opts.enctype) el.enctype = opts.enctype;
    if (opts.encoding) el.encoding = opts.encoding;
    if (opts.acceptCharset) el.acceptCharset = opts.acceptCharset;
    if (opts.autocomplete) el.autocomplete = opts.autocomplete;
    if (opts.noValidate) el.noValidate = opts.noValidate;
}));
export const frame = make('frame', {}, updateHtmlElement);
export const frameset = make('frameset', {}, updateHtmlElement);
export const h1 = make('h1', {}, updateHtmlElement);
export const h2 = make('h2', {}, updateHtmlElement);
export const h3 = make('h3', {}, updateHtmlElement);
export const h4 = make('h4', {}, updateHtmlElement);
export const h5 = make('h5', {}, updateHtmlElement);
export const h6 = make('h6', {}, updateHtmlElement);
export const head = make('head', {}, updateHtmlElement);
export const header = make('header', {}, updateHtmlElement);
export const hgroup = make('hgroup', {}, updateHtmlElement);
export const hr = make('hr', {}, updateHtmlElement);
export const html = make('html', {}, updateHtmlElement);
export const i = make('i', {}, updateHtmlElement);
export const iframe = make('iframe', {}, updater((el, opts: IframeOptions) => {
    if (opts.src) el.src = opts.src;
    if (opts.allow) el.setAttribute('allow', opts.allow);
}));
export const img = make('img', {}, updater((el, opts: ImageOptions) => {
    if (opts.src) el.src = opts.src;
    if (opts.srcset) el.srcset = opts.srcset;
    if (opts.sizes) el.sizes = opts.sizes;
}));
export const input = make('input', {}, updater((el, opts: InputOptions) => {
    if (opts.autocomplete) el.autocomplete = opts.autocomplete;
    if (opts.autofocus) el.autofocus = opts.autofocus;
    if (opts.disabled) el.disabled = opts.disabled;
    if (opts.name) el.name = opts.name;
    if (opts.type) el.type = opts.type;
    if (opts.checked) el.checked = opts.checked;
    if (opts.value) el.value = opts.value;
    if (opts.placeholder) el.placeholder = opts.placeholder;
    if (opts.required) el.required = opts.required;
}));
export const ins = make('ins', {}, updateHtmlElement);
export const kbd = make('kbd', {}, updateHtmlElement);
export const label = make('label', {}, updater((el, opts: LabelOptions) => {
    if (opts.for) el.htmlFor = opts.for;
}));
export const legend = make('legend', {}, updateHtmlElement);
export const li = make('li', {}, updateHtmlElement);
export const link = make('link', {}, updateHtmlElement);
//export const map = make('map', {}, updateHtmlElement);
export const mark = make('mark', {}, updateHtmlElement);
export const marquee = make('marquee', {}, updateHtmlElement);
export const menu = make('menu', {}, updateHtmlElement);
export const meta = make('meta', {}, updateHtmlElement);
export const meter = make('meter', {}, updateHtmlElement);
export const nav = make('nav', {}, updateHtmlElement);
export const noscript = make('noscript', {}, updateHtmlElement);
export const object = make('object', {}, updateHtmlElement);
export const ol = make('ol', {}, updateHtmlElement);
export const optgroup = make('optgroup', {}, updateHtmlElement);
export const option = make('option', {}, updater((el, opts: OptionOptions) => {
    if (opts.defaultSelected) el.defaultSelected = opts.defaultSelected;
    if (opts.disabled) el.disabled = opts.disabled;
    if (opts.label) el.label = opts.label;
    if (opts.selected) el.selected = opts.selected;
    if (opts.text) el.text = opts.text;
    if (opts.value) el.value = opts.value;
}));
export const output = make('output', {}, updateHtmlElement);
export const p = make('p', {}, updateHtmlElement);
export const param = make('param', {}, updateHtmlElement);
export const picture = make('picture', {}, updateHtmlElement);
export const pre = make('pre', {}, updateHtmlElement);
export const progress = make('progress', {}, updateHtmlElement);
export const q = make('q', {}, updateHtmlElement);
export const rt = make('rt', {}, updateHtmlElement);
export const ruby = make('ruby', {}, updateHtmlElement);
export const s = make('s', {}, updateHtmlElement);
export const samp = make('samp', {}, updateHtmlElement);
export const script = make('script', {}, updater((el, opts: ScriptOptions) => {
    if (opts.async) el.async = opts.async;
    if (opts.defer) el.defer = opts.defer;
    if (opts.src) el.src = opts.src;
    if (opts.text) el.text = opts.text;
    if (opts.type) el.type = opts.type;
}));
export const section = make('section', {}, updateHtmlElement);
export const select = make('select', {}, updater((el, opts: SelectOptions) => {
    if (opts.multiple) el.multiple = opts.multiple;
    if (opts.selectedIndex) el.selectedIndex = opts.selectedIndex;
    if (opts.required) el.required = opts.required;
    if (opts.disabled) el.disabled = opts.disabled;
    if (opts.autofocus) el.autofocus = opts.autofocus;
    if (opts.name) el.name = opts.name;
    if (opts.value) el.value = opts.value;
}));
export const slot = make('slot', {}, updateHtmlElement);
export const small = make('small', {}, updateHtmlElement);
export const source = make('source', {}, updateHtmlElement);
export const span = make('span', {}, updateHtmlElement);
export const strong = make('strong', {}, updateHtmlElement);
export const style = make('style', {}, updater((el, opts: StyleOptions) => {
    if (opts.media) el.media = opts.media;
    if (opts.type) el.type = opts.type;
    if (opts.css) el.innerHTML = opts.css;
}));
export const sub = make('sub', {}, updateHtmlElement);
export const sup = make('sup', {}, updateHtmlElement);
export const table = make('table', {}, updateHtmlElement);
export const tbody = make('tbody', {}, updateHtmlElement);
export const td = make('td', {}, updateHtmlElement);
export const template = make('template', {}, updateHtmlElement);
export const textarea = make('textarea', {}, updateHtmlElement);
export const tfoot = make('tfoot', {}, updateHtmlElement);
export const th = make('th', {}, updater((el, opts: THOptions) => {
    if (opts.scope) el.scope = opts.scope;

}));
export const thead = make('thead', {}, updateHtmlElement);
export const time = make('time', {}, updateHtmlElement);
export const title = make('title', {}, updateHtmlElement);
export const tr = make('tr', {}, updateHtmlElement);
export const track = make('track', {}, updateHtmlElement);
export const u = make('u', {}, updateHtmlElement);
export const ul = make('ul', {}, updateHtmlElement);
export const variable = make('var', {}, updateHtmlElement);
export const video = make('video', {}, updater((el, opts: VideoOptions) => {
    if (opts.autoplay) el.autoplay = opts.autoplay;
    if (opts.muted) el.muted = opts.muted;
    if (opts.playsinline) {
        el.setAttribute('playsinline', '');
    } else {
        el.removeAttribute('playsinline');
    }
}));
export const wbr = make('wbr', {}, updateHtmlElement);

export const main = build('main');

export const svg = makeSvg('svg', {});

export function clear(node: Node): void {
    while (node.firstChild) node.removeChild(node.firstChild);
}
export function replaceContent(node: Node, ...children: Array<false | Child>): void {
    clear(node);
    updateNode(node, { children });
}
