import '../api/legacy_api';

export function managerLink(id: string): string {
    return `https://manager.parkingboss.com/${id}`;
}

export function makeUrl(uri: string) {
    const url: URL = new URL(uri);
    url.searchParams.set('version', new Date().toISOString());
    url.searchParams.set('access_token', ParkIQ.API.Auth.get().token);
    return url.toString();
}
