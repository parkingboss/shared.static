import * as _ from 'lodash';
import { api, isHTTPMethod } from '../api';
import { createEvent } from './events';
import { detailedError } from './helpers';

function isButtonLike(el: Element): el is HTMLButtonElement | HTMLInputElement {
    return el instanceof HTMLButtonElement || el instanceof HTMLInputElement;
}

function working(form: HTMLFormElement, isWorking: boolean = true) {
    if (isWorking) {
        form.classList.add('working');
    } else {
        form.classList.remove('working');
    }
    _.map(form.querySelectorAll('input,button[type=submit]'), (el: HTMLInputElement|HTMLButtonElement) => {
        el.disabled = isWorking;
    });
}

async function submitFormByApi(evt: Event) {
    if (evt.target instanceof HTMLFormElement) {

        const form: HTMLFormElement = evt.target;

        const action = form.getAttribute('action');
        if (!action) {
            console.warn(detailedError('Unset action', action, form));
            return;
        }

        const body = new FormData(form);
        const submitter = document.activeElement;

        let method = form.getAttribute('method');
        if (submitter && isButtonLike(submitter)){
            if (submitter.name && submitter.value) {
                body.set(submitter.name, submitter.value);
            }
            if (submitter.hasAttribute('method')) {
                let buttonMethod = submitter.getAttribute('method');
                buttonMethod = buttonMethod && buttonMethod.toUpperCase();
                if (buttonMethod && isHTTPMethod(buttonMethod)) {
                    method = buttonMethod;
                }
            }
        }

        if (!isHTTPMethod(method)) {
            console.warn(detailedError('Unknown HTTP Method', method, form));
            return;
        }
        evt.preventDefault();
        working(form);
        (await api).execute(method, action, { body })
            .then(response => {
                // This is an ugly hack. I think.
                // It allows people to add event listeners to `afterSubmit`
                // They can then handle the result of the submit
                form.dispatchEvent(createEvent('afterSubmit', response));
            })
            .catch(x => {
                detailedError('Server Error', x);
                form.classList.remove('working');
            })
            .then(() => {
                form.reset();
                working(form, false);
            });
    }
}

export function handleAllSubmitByApi() {
    document.addEventListener('submit', submitFormByApi);
}