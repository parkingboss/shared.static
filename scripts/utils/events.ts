declare global {
    interface Window {
        CustomEvent: CustomEvent;
    }
}

(() => {
    if (typeof window.CustomEvent !== 'function') return;

    function CustomEvent<T = any>(event: string, params: CustomEventInit<T>) {
        const actual: { bubbles: boolean, cancelable: boolean, detail: T|null } = {
            bubbles: false,
            cancelable: false,
            detail: null,
        };
        Object.assign(actual, params);

        const evt = document.createEvent('CustomEvent') as CustomEvent<T>;
        (evt as CustomEvent<T | null>).initCustomEvent(event, actual.bubbles, actual.cancelable, actual.detail);
        return evt;
    }
    CustomEvent.prototype = Event.prototype;
    window.CustomEvent = CustomEvent as any;
})();

export function createEvent<T>(name: string, detail?: T) {
    return new CustomEvent<T>(name, detail ? { detail } : undefined);
}

export type CustomEventHandler<T> = (evt: CustomEvent<T>) => void;