export class Color {
    constructor(public name: string) {}

    get bgClass() { return `${this.name}-bg`; }
    get textClass() { return `${this.name}-text`; }
    get borderClass() { return `${this.name}-border`; }
}
export const White = new Color('white');
export const OffWhite = new Color('off-white');
export const Black = new Color('black');
export const Gray = new Color('gray');
export const XLightGray = new Color('xlight-gray');
export const LightGray = new Color('light-gray');
export const DarkGray = new Color('dark-gray');
export const Brown = new Color('brown');
export const LightBrown = new Color('light-brown');
export const DarkBrown = new Color('dark-brown');
export const Red = new Color('red');
export const LightRed = new Color('light-red');
export const DarkRed = new Color('dark-red');
export const LightBlue = new Color('light-blue');
export const LighterBlue = new Color('lighter-blue');
export const Blue = new Color('blue');
export const DarkerBlue = new Color('darker-blue');
export const DarkBlue = new Color('dark-blue');
export const Yellow = new Color('yellow');
export const XLightYellow = new Color('xlight-yellow');
export const LightYellow = new Color('light-yellow');
export const DarkYellow = new Color('dark-yellow');
export const Purple = new Color('purple');
export const LightPurple = new Color('light-purple');
export const DarkPurple = new Color('dark-purple');
export const Orange = new Color('orange');
export const LightOrange = new Color('light-orange');
export const DarkOrange = new Color('dark-orange');
export const Green = new Color('green');
export const XLightGreen = new Color('xlight-green');
export const LightGreen = new Color('light-green');
export const DarkGreen = new Color('dark-green');