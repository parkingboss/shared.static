export type IsA<T> = (x: any) => x is T;

export function checkProperties<T>(...keys: Array<keyof T>): IsA<T> {
    return (x: any): x is T => keys.every(key => x.hasOwnProperty(key));
}

export function isArray<T>(test: IsA<T>): IsA<T[]> {
    return (x: any): x is T[] => {
        return x instanceof Array && x.every(n => test(n));
    };
}

export function stringSort<T>(k: keyof T): (a: T, b: T) => -1 | 0 | 1 {
    return (a, b) => {
        if (a[k] === b[k]) return 0;
        return a[k] < b[k] ? -1 : 1;
    };
}

export interface Items<T> {
    [id: string]: T;
}

export interface ReadonlyItems<T> {
    readonly [id: string]: T;
}

export function parseQuery(input: string): Items<string> {
    const result: Items<string> = {};
    input.replace(/^\?/, '')
        .split('&')
        .map(x => x.split('='))
        .forEach(([key, value]) => {
            result[decodeURIComponent(key)] = decodeURIComponent(value);
        });
    return result;
}

export function cl(...classes: Array<false|null|undefined|string>): string {
    return classes.filter(x => x).join(' ');
}

type IfEquals<X, Y, A = X, B = never> =
    (<T>() => T extends X ? 1 : 2) extends (<T>() => T extends Y ? 1 : 2) ? A : B;

export type WritableKeys<T> = {
    [P in keyof T]-?: IfEquals<{ [Q in P]: T[P] }, { -readonly [Q in P]: T[P] }, P>
}[keyof T];

export type Writable<T> = Pick<T, WritableKeys<T>>;

export function formData(content: Items<string|[Blob, string]>): FormData {
    const data = new FormData();
    Object.keys(content).forEach(key => {
        const val = content[key];
        if (typeof val === 'string') {
            data.set(key, val);
        } else {
            data.set(key, val[0], val[1]);
        }
    });
    return data;
}

export type ErrorHandler = (error: Error) => void;

export function detailedError(name: string, ...args: any[]) {
    console.group(name);
    args.forEach(a => console.error(a));
    console.groupEnd();
    return new Error(name);
}

export interface INamed {
    id: string,
    name: string,
}