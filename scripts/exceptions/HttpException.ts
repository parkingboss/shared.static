export class HttpException {
    constructor(readonly message: string, readonly response: Response) {
    }

    get is404() {
        return this.response.status === 404;
    }

    get statusText() {
        return this.response.statusText;
    }
}

// This stupidity is needed because:
// https://github.com/Microsoft/TypeScript/wiki/Breaking-Changes#extending-built-ins-like-error-array-and-map-may-no-longer-work
export function isHttpException(x: any): x is HttpException {
    if (x == null) return false;
    const hasResponse = x.response instanceof Response;
    const hasIs404 = typeof x.is404 === 'boolean';
    const hasStatusText = typeof x.statusText === 'string';
    return hasResponse && hasIs404 && hasStatusText;
}
