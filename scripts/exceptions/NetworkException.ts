export class NetworkException extends Error {
    constructor(msg: string, readonly parentError: Error) {
        super(msg);
    }
}
