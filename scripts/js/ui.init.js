(function() {

    var Supports = {}
 
    Supports.events = Supports.events || {};
     
     
    // from modernizr
    Supports.events.touch = Supports.touch = (function() {
        var bool;
        if(('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch) {
          bool = true;
        }/*
 else {
          var query = ['@media (',prefixes.join('touch-enabled),('),'heartz',')','{#modernizr{top:9px;position:absolute}}'].join('');
          testStyles(query, function( node ) {
            bool = node.offsetTop === 9;
          });
        }
*/
        return bool;
    })();
     
     
    if(Supports.touch && "classList" in document.documentElement) document.documentElement.classList.add("touch");
    else if("classList" in document.documentElement) document.documentElement.classList.add("hoverable");
    if("addEventListener" in document) document.addEventListener("touchstart", function(){}, true); // empty touchstart for touch
     
})();