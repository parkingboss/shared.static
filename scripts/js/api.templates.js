(function(api) {

    //moment().calendar(referenceTime, formats);

    function calendar(dt, ref) {

        if(!dt) return "";

        ref = ref || new Date();

        if(dateFns.isSameYear(dt, ref)) return dateFns.format(dt, "MMM D h:mm A");

        return dateFns.format(dt, "MMM D YYYY h:mm A");

        // if(dateFns.isSameYear(dt, ref)) return moment(dt).calendar(ref, {
        //     lastDay : 'MMM D h:mm A',
        //     sameDay : 'MMM D h:mm A',
        //     nextDay : 'MMM D h:mm A',
        //     lastWeek : 'MMM D h:mm A',
        //     nextWeek : 'MMM D h:mm A',
        //     sameElse : 'MMM D h:mm A'
        // });

        // return moment(dt).calendar(ref, {
        //     lastDay : 'MMM D YYYY h:mm A',
        //     sameDay : 'MMM D YYYY h:mm A',
        //     nextDay : 'MMM D YYYY h:mm A',
        //     lastWeek : 'MMM D YYYY h:mm A',
        //     nextWeek : 'MMM D YYYY h:mm A',
        //     sameElse : 'MMM D YYYY h:mm A'
        // }); // use default
    }
    
    
    var attendant = '<% if(!!_.get(item, "attendant.id")) { %><data class="attendant" value="<%= item.attendant.id %>" title="<%= _.get(item.attendant, "title", "") %>"><%= _.get(item.attendant, "title", "") %></data><% } %>';    
    var vehicle = '<% if(!!item.vehicle) { %><data class="vehicle id" value="<%= item.vehicle.id %>"><a href="./vehicles/<%= item.vehicle.id %>"><%= item.vehicle.display || item.vehicle %></a></data><% } %>';
    var tenant = '<% if(!!item.tenant) { %><data class="tenant id <%= item.tenant.type %>" value="<%= item.tenant.id %>"><a href="./tenants/<%= item.tenant.id %>"><%= item.tenant.display %></a></data><% } %>';
    var space = '<% if(!!item.space) { %><data class="space id <%= item.space.type %>" value="<%= item.space.id %>"><a href="./spaces/<%= item.space.id %>"><%= item.space.display %></a></data><% } %>';
    var media = '<% if(!!item.media) { %><data class="media id <%= !!item.media.type ? item.media.type.format : item.media.type %>" value="<%= item.media.id %>"><a href="./media/<%= item.media.id %>"><%= item.media.display %></a></data><% } %>';
    var mediaformat = '<% if(!!_.get(item, "media.type")) { %><data class="media type <%= _.get(item, "media.type.format") || _.get(item, "media.type") %>" value="<%= _.get(item, "media.type.id") %>"><%= _.get(item, "media.type.title") %></data><% } %>';
    //var mediatype = '<% if(!!_.get(item, "media.type")) { %><data class="media type <%= _.get(item, "media.type.format") || _.get(item, "media.type") %>" value="<%= _.get(item, "media.type.id") %>"><%= _.get(item, "media.type.title") %></data><% } %>';
    var warning = '<% if(!!item.warning) { %><data class="smart warning id" value="<%= item.warning.id %>"><%= item.warning.display %></data><% } %>';
    var number = '<% if(!!item.number) { %><data class="<%= item.__type %> id" value="<%= item.id %>"><%= item.number %></data><% } %>';
    
    var visiting = '<% if(!!item.visiting) { %><h1 class="visiting id"><%= item.visiting %></h1><% } %>';
    var notify = '<% if(!item.assigned && !!item.url) { %><a href="<%= item.url %>" target="_blank" type="text/html"></a><a href="mailto:?body=<%= item.url %>"></a><% } %>';

    var files = '<% if(!!item.files && item.files.length > 0) { %><ul class="photos files"><% _.each(item.files, function(item) { %><li class="file <%= item.type.split("/").join(" ") %>"><figure><a href="https://parking-uploads.imgix.net/<%= item.uuid %>/<%= item.name %>?auto=compress,enhance" target="_blank"><img src="https://parking-uploads.imgix.net/<%= item.uuid %>/<%= item.name %>?w=80&h=80&fit=crop&crop=entropy&auto=compress,enhance" /></a></figure></li><% }); %></ul><% } %>';

    var mediadesc = '<% var mediatype = _.get(item, "media.type.description") || _.get(item, "media.type.title") || _.filter([ _.get(item, "media.type.color"), _.get(item, "media.type.label") ]).join(" "); %><% if(!!mediatype) { %><li class="type note"><%= mediatype %></li><% } %>';
    var contact = '<% if(!!item.contact && !!item.contact.name) { %><li class="contact name note"><%= item.contact.name %></li><% } %><% if(!!item.contact && !!item.contact.email) { %><li class="contact email note"><a href="mailto:<%= item.contact.email %>"><%= item.contact.email %></a></li><% } %><% if(!!item.contact && !!(item.contact.phone || item.contact.tel)) { %><li class="contact tel note"><%= item.contact.phone || item.contact.tel %></li><% } %>';
    var note = '<blockquote><p><%= item.text %></p></blockquote>';
    var notes = '<ul class="notes">' + mediadesc + contact + '<% if(!!item.notes && item.notes.length > 0) { %><% _.each(item.notes, function(item) { %><li class="user note">' + note + '<% }); %><% } %></ul>';
    var pass = '<% if(!!item.pass && !!item.pass.render) { %><figure class="pass doc preview letter margin0 loaded"><p class="content"><img src="<%= item.pass.doc.pdf.url.replace("passes.parkingboss.com", "parking-passes.imgix.net") %>?fm=png8&w=118&dpr=<%= Math.max(window.devicePixelRatio || 1, 1) %><%= !!item.expired ? "&sat=-100" : "" %>"></p></figure><% } %>';
    
    function issued(title, attr) {
        attr = "";
        return '<time class="issued <%= !!item.issued.today() ? "today" : "" %>" data-calendar="<%= calendar(item.issued.utc).replace(" ", "\xA0") %>" data-relative="<%= item.issued.relative(true).replace(" ", "\xA0") %>" datetime="<%= item.issued.utc %>">' + title + ' <span class="relative"><%= moment(_.get(item, "issued.utc")).fromNow() %></span> <span class="calendar"><%= calendar(_.get(item, "issued.utc")) %></span> <data value="<%= _.get(item, "issued.user.id", "Virtual Attendant") %>" class="user" title="<%= _.get(item, "issued.user.name", "Virtual Attendant") %>">' + attr + '<%= _.get(item, "issued.user.name", "Virtual Attendant") %></data></time>';
    };

    

  var validfrom = '<% if(!!item.valid) { var from = _.get(item, "valid.from"); var fromDate = new Date(from.utc); %><time data-calendar="<%= calendar(fromDate).replace(" ", "\xA0") %>" data-relative="<%= from.relative(true).replace(" ", "\xA0") %>" datetime="<%= from.utc %>" class="valid from <%= fromDate >= new Date() ? "future" : "past" %>"><%= _.invoke(api, "Permits.status", item, false, !item.assigned ? " started " : " activated ", !item.assigned ? " started " : " activated ", " starts ", !item.assigned ? " started " : " activated ", !item.assigned ? " started " : " activated ") %> <span class="relative"><%= moment(fromDate).fromNow() %></span> <span class="calendar"><%= calendar(fromDate) %></span> <data value="<%= _.get(item, "issued.user.id", "Virtual Attendant") %>" class="user" title="<%= _.get(item, "issued.user.name", "Virtual Attendant") %>"><%= _.get(item, "issued.user.name", "Virtual Attendant") %></data></time><% } %>';

  var validto = '<time datetime="<%= _.get(item, "valid.to.utc", "") %>" class="valid to <%= !_.get(item, "valid.to.utc") || new Date(item.valid.to.utc) >= new Date() ? "future" : "past" %>"><%= !_.get(item, "valid.to.utc") ? " active until revoked " : _.invoke(api, "Permits.status", item, false, " active ", " cancelled early ", " active ", " revoked ", " expired ") %><% if(!!_.get(item, "valid.to.utc")) { %><span class="relative"><%= new Date(item.valid.to.utc) >= new Date() ? " for " : " " %><%= moment(_.get(item, "valid.to.utc")).fromNow(dateFns.isAfter(item.valid.to.utc, new Date())) %></span> <span class="calendar"><%= new Date(item.valid.to.utc) >= new Date() ? " until " : " " %><%= calendar(_.get(item, "valid.to.utc")) %></span><% if(!!_.get(item, "updated.user.id")) { %> <data value="<%= _.get(item, "updated.user.id", "") %>" class="user" title="<%= _.get(item, "updated.user.name", "") %>"><%= _.get(item, "updated.user.name", "") %></data><% } %><% } %></time>';

    var valid = '<% if(!!item.valid) { %><p class="valid">' + issued('<%= !!item.assigned ? "Assigned " : !!_.get(item, "issued.user") ? "Special " : "" %>', "by ") + validfrom + validto + '</p><% } %>';

    // items without valid are presumed to be valid
    var meta = '<%= !!item.issued && !!item.issued.user ? " user" : "" %><%= !!item.assigned ? " assigned" : "" %><%= !!item.media ? " media" : "" %><%= !!item.media && !!item.media.type ? " " + item.media.type.format || item.media.type : "" %><%= !!item.vehicle ? " vehicle" : "" %><%= !!item.space ? " space" : "" %><%= !!item.tenant ? " tenant" : "" %><%= !!item.visiting ? " pass" : "" %><%= !!item.attendant ? " attendant" : "" %><%= !!item.grace && !!item.grace.now() ? " grace" : "" %><%= !!item.getStatus ? " " + item.getStatus(true) : "" %>';
   

    var allowed = '<% if(_.get(item, "limit.allowed")) { %><time class="max allowed" datetime="<%= item.limit.allowed.iso %>"><%= item.limit.allowed.display %></time><% } %>';

    var period = '<% if (!!_.get(item, "period")) { %><time class="period" datetime="<%= item.period %>"><%= _.get({ "P1D": "so far today", "P1W": "so far this week", "P1M": "so far this month", "P1Y": "so far this year" }, item.period, "in the last " + item.duration.display) %></time><% } %>';
    
    var used = '<% if(item.used) { %><time class="used" datetime="<%= item.used.iso %>"><%= item.used.zero ? "No time" : item.used.display %></time><% } %>';
    
    var remaining = '<% if(item.remaining) { %><time class="remaining" datetime="<%= item.remaining.iso %>"><%= item.remaining.display %></time><% } %>';
    
    //
    
    var templates = api.Templates = api.Templates || {};
    api.Templates.permit = _.template('<li class="permit' + meta + '" data-record="permits/<%= item.id %>"><header><h1 class="title"><%= _.filter([ _.invoke(api, "Permits.status", item, false, "", "", "", "", ""), _.get(item, "title"), false && !_.get(item, "assigned") && !!_.get(item, "issued.user") ? "(Special)" : "" ]).join(" ") %></h1>' + mediaformat + valid +  '</header>' + attendant + visiting + media + vehicle + space + tenant + number + notes + pass + '<nav><% if(!item.expired && !item.cancelled) { %><% if(!!item.pass) { %><a type="application/pdf" href="<%= item.pass.doc.pdf.url.replace("passes.parkingboss.com", "parking-passes.imgix.net") %>" rel="print" target="_blank"></a><a href="<%= item.pass.doc.pdf.url.replace("passes.parkingboss.com", "parking-passes.imgix.net") %>" target="_blank" type="application/pdf" download="download"></a><% } %><% if(!!item.assigned) { %><a href="permits/<%= item.id %>/edit"></a><% } %><a href="permits/<%= item.id %>/revoke"></a><% } %><a href="permits/<%= item.id %>/duplicate"></a>' + notify + '</nav>' + notify + issued("Issued ", " by ") + '</li>', {
        "variable":"item",
        "imports":{
            api:api,
            dateFns:dateFns,
            moment:moment,
            calendar:calendar,
        },
    });

    api.Templates.permitissued = _.template('<li class="permit issued' + meta + '" data-record="permits/<%= item.id %>">' + attendant + issued() + media + vehicle + space + tenant + notes + '</li>', {
        "variable":"item",
        "imports":{
            //api:api,
            dateFns:dateFns,
            moment:moment,
            calendar:calendar,
        },
    });
    api.Templates.permitvalidfrom = _.template('<li class="permit valid-from' + meta + '" data-record="permits/<%= item.id %>">' + attendant + validfrom + media + vehicle + space + tenant + notes + '</li>', {
    "variable":"item",
    "imports":{
        //api:api,
        dateFns:dateFns,
        moment:moment,
        calendar:calendar,
    },
});
    api.Templates.permitvalidto = _.template('<li class="permit valid-to' + meta + '" data-record="permits/<%= item.id %>">' + attendant + validto + media + vehicle + space + tenant + notes + '</li>', {
    "variable":"item",
    "imports":{
        //api:api,
        dateFns:dateFns,
        moment:moment,
        calendar:calendar,
    },
});
    api.Templates.permittable = _.template('<li class="permittable <%= item.permittable === true ? "dopermit positive" : item.permittable === false ? "donotpermit negative" : "neutral" %>' + meta + '" data-record="permittables/<%= item.id %>"><header><h1 class="title"><%= item.permittable === true ? "Whitelisted" : item.permittable === false ? "Banned" : "Unbanned"  %><% if(_.get(item, "attendant.id")) { %> ' + attendant + '<% } %></h1>' + issued("", " by ") + '</header>' + attendant + media + vehicle + space + tenant + notes + '<nav><% if(item.permittable === false) { %><a href="donotpermit/<%= item.id %>/revoke"></a><% } %></nav></li>', {
        "variable":"item",
        "imports":{
            //api:api,
            dateFns:dateFns,
            moment:moment,
            calendar:calendar,
        },
    });
    api.Templates.attendantstatus = function(item) {
        if(!item) return "";
        if(!!item.permittable) return templates.permittable(item.permittable);
        if(!!item.permit) return templates.assigneddonotpermit(item);
        return templates.timeusage(item);
    };
    api.Templates.timeusage = _.template('<li class="limit usage <%= !!item.remaining && !!item.remaining.zero ? " exceeded" : "" %><%= !!item.remaining ? " remaining" : " used" %><%= item.future === false ? " prev" : "" %>" data-record="attendants/<%= item.attendant.id %>/permits/usage"><header><h1>' + attendant + ' Time</h1></header>' + attendant + vehicle + tenant + '<p>' +used + remaining + ' ' + allowed + ' ' + period + '</p></li>', {
        "variable":"item",
        imports: {
            calendar:calendar,
        }
    });
    api.Templates.authentication = _.template('<li class="authentication" data-record="authentication/<%= item.id %>">' + attendant + tenant + '<header><h1>Passcode</h1></header><p><data class="numeric" value="<%= item.id %>"><%= item.display %></data></p><nav><a href="tenant/authentication">Reset</a></nav></li>', {
        "variable":"item",
    });


    api.Templates.assigneddonotpermit = _.template('<li class="implicit donotpermit <%= _.get(item, "permit.media.id", "") %> <%= _.get(item, "permit.media.type", "") %> <%= _.get(item, "permit.space.id", "") %> ' + meta + '" data-record="attendants/<%= item.attendant.id %>"><header><h1 class="title"><%= _.filter([ "Banned", _.get(item, "attendant.title") ]).join(" from ") %></h1><time class="issued" datetime="<%= _.get(item, "permit.issued.utc") %>"> automatically by active <%= _.get(item, "permit.media.type.title") || _.get(item, "permit.title") %></time></header>' + attendant + vehicle + tenant + '</li>', {
        "variable":"item",
        "imports":{
            //api:api,
            dateFns:dateFns,
            moment:moment,
            calendar:calendar,
        },
    });
    api.Templates.nopermits = _.template('<li class="nopermits' + meta + '" data-record="permits/none"><header><h1>No active permits</h1></header>' + attendant + vehicle + tenant + media + '</li>', {
        "variable":"item",
        "imports":{
            //api:api,
            dateFns:dateFns,
            moment:moment,
            calendar:calendar,
        },
    });

    api.Templates.violation = _.template('<li class="violation' + meta + '" data-record="violations/<%= item.id %>"><header><h1 class="title"><%= !!_.get(item, "warning") ? "Warning" : _.get(item, "title") %></h1>' + issued('<%= !!_.get(item, "warning") ? " left " : " recorded " %>', " by ") + '</header>' + media + vehicle + space + tenant + warning + notes + files + '<nav><a href="violations/<%= item.id %>/revoke"></a>' + notify + '</nav>' + notify + '</li>', {
        "variable":"item",
        "imports":{
            //api:api,
            dateFns:dateFns,
            moment: moment,
            calendar:calendar,
        },
    });

    api.Templates.note = _.template('<li class="note' + meta + '" data-record="notes/<%= item.id %>"><header><h1 class="title">Note</h1>' + issued("", " by ") + '</header>' + media + vehicle + space + tenant + note + '</li>', {
        "variable":"item",
        "imports":{
            //api:api,
            dateFns:dateFns,
            moment: moment,
            calendar:calendar,
        },
    });
    api.Templates.exception = _.template('<li class="violation exception" data-record="violations/thresholds/<%= _.get(item, "threshold.id") %>"><header><h1 class="name"><%= _.get(item, "threshold.name") %><span> flagged for <%= _.get(item, "threshold.display") %></span></h1><time datetime="<%= _.get(item, "latest") %>" class="issued"><span class="calendar"><%= calendar(_.get(item, "latest")) %></span><span class="relative"><%= moment(_.get(item, "latest")).fromNow() %></span></time></header>' + vehicle + tenant + '<p><%= _.get(item, "display") %></p></li>', {
        "variable":"item",
        imports: {
            dateFns: dateFns,
            moment: moment,
            calendar:calendar,
            //numberToWords:numberToWords.toWords,
        }
    });

    api.Templates.sent = _.template('<li class="sent" data-record="sent/<%= item.id %>"><time class="sent" datetime="<% item.sent.utc || sent %>"></time><% if(!!item.delivered) { %><time class="delivered" datetime="<%= item.delivered.utc || item.delivered %>"></time><% } %><% if(!!item.viewed) { %><time class="viewed" datetime="<%= item.viewed.utc || item.viewed %>"></time><% } %><% if(!!item.errored) { %><time class="error" datetime="<%= item.errored.utc || item.errored %>"></time><% } %><% if(!!item.email) { %><a href="mailto:<%= item.email %>"><%= item.email %></a><% } %><% if(!!item.tel) { %><a href="tel:+<%= item.tel.number || item.tel %>"><%= item.tel.display || item.tel %></a><% } %></li>', {
        "variable":"item",
        imports: {
            calendar: calendar,
        }
    });

    function datetime(data) {
        if(!data) return data;
        if(_.isString(data)) return {
            datetime: data,
        };

        // transform
        return {
            title: _.get(data, "title"),
            datetime: _.get(data, "datetime"), // others?
            by: _.get(data, "by"),
        };
    }

    function interval(data) {

        if(_.isString(data)) return _.filter(_.map(_.split(data, "/"), datetime));

        return _.filter(_.map([
            _.get(data, "min"),
            _.get(data, "max")
        ], datetime));
    }

    function timeItem() {
        return '<time datetime="<%= _.get(item, "datetime") %>"><%= _.get(item, "title") %> <%= calendar(_.get(item, "datetime")) %> <data class="by" value="<%= _.get(item, "by.id") || _.get(item, "by") %>"><%= _.get(item, "by.name") %></data></time>';
    }

    function intervalItem(item, path) {
        return '<% interval(_.get(' + item + ', "' + path + '")).forEach(function(item, i) { %>' + timeItem() + '<% }); %>';
    }

    function userItem(item, path) {
        return '<dl data-record="users/<%= _.get(' + item + ', "' + path + '.id") %>"><dt class="name">Name</dt><dd class="name"><%= _.get(' + item + ', "' + path + '.name") %></dd><dt class="email">Email</dt><dd class="email"><a href="mailto:<%= _.get(' + item + ', "' + path + '.email") %>"><%= _.get(' + item + ', "' + path + '.email") %></a></dd></dl>';
    }

    api.Templates.authorized = _.template('<li data-record="authorizations/<%= _.get(item, "id") %>">' + userItem("item", "subject") + '<p>' + intervalItem("item", "valid") + '</p></li>', {
        "variable":"item",
        imports: {
            calendar:calendar,
            interval:interval,
        },
    });

    api.Templates.user = _.template('<li data-record="users/<%= item.id %>" class="user<%= !!item.roles.owner ? " owner" : "" %><%= !!item.roles.admin ? " admin" : "" %><%= !!item.roles.fieldagent ? " fieldagent" : "" %>"><form class="delete user" method="post"><input name="_method" type="hidden" value="delete" /><input name="user" type="hidden" value="<%= item.id %>" /><button type="submit"></button></form><dl><dt class="name">Name</dt><dd class="name"><%= item.name %></dd><dt class="email">Email</dt><dd class="email"><a href="mailto:<%= item.email %>"><%= item.email %></a></dd><dt class="activity">Last Active</dt><dd class="activity"><% if(!!item.login) { %><time class="login" datetime="<%= item.active.utc %>"><%= item.active.relative() %></time><% } %><time class="created" datetime="<%= item.created.utc %>"><%= calendar(item.created.utc) %></time></dt><dd class="active"></dd></dl><% if(!!item.auth) { %><nav><a href="<%= item.url %>?access_token=<%= item.auth.token %>" target="_blank" type="text/html"></a></nav><% } %></li>', {
        "variable":"item",
        imports: {
            calendar:calendar,
        },
    });
    
}(ParkIQ.API));