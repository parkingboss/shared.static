(function(root, Delegate) {

    root.addEventListener("back", function(e) {
    
    //var delegate = new Delegate(root);
    //delegate.on("back", function(e) {
        
        if(!!window.history && window.history.length > 0) window.history.go(-1);
        else window.location.href = document.referrer || document.querySelector("body > nav.main ul a:first-of-type").href; 
        
        
    });
    
}(document.documentElement));