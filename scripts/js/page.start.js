page.start({
	dispatch:true,
	popstate:true,
	click:true,
	hashbang:false,
	decodeURLComponents:false, // disable, breaks nested encoding in query strings
});