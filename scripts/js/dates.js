(function(root) {

	// disabled is a reflected attribute-property
	function disable(elem) {
		elem.disabled = true;
		elem.closest("label").classList.add("disabled");
	}
	function enable(elem) {
		elem.disabled = false;
		elem.closest("label").classList.remove("disabled");
	}

	root.addEventListener("change", function(e) {

		if(!_.invoke(e, "target.matches", "form .datetime select.date")) return;

		var target = e.target

		if(target.options.length <= 0) return; // no options

		var datetime = target.closest(".datetime");

		var inputs = datetime.querySelectorAll("input[type='date']");
		var times = datetime.querySelectorAll("select.time");

		_.each(inputs, disable);

		target.blur();
		
		enable(target);
		var label = target.closest("label");

		if(target.value == "0" && target.selectedIndex === 0 && !!target.querySelector("option[value='0']")) {

			// now
			label.classList.add("now");

			// disable time
			_.each(times, disable);
			_.each(inputs, disable);
			
		} else {

			// not now
			label.classList.remove("now");
			
			// activate time
			_.each(times, function(select) {
				enable(select);
				select.dispatchEvent(new CustomEvent("change", { bubbles:true }));
			});
			_.each(inputs, disable);
		}

		if(target.value != "") return;
		if(!target.options[target.options.length - 1].value && target.selectedIndex < target.options.length - 1) return; //if last item has no value and not selected, don't continue

		// disable target
		if(!!target.name) disable(target);

		var touch = (('ontouchstart' in window) || (window.DocumentTouch && document instanceof DocumentTouch));

		// activate date inputs
		_.each(inputs, function(input) {
			enable(input);
			input.value = input.value || input.getAttribute("value"); // refresh from value;
			input.dispatchEvent(new CustomEvent("change", { bubbles: true }));
			if(!!touch) return;
			try { input.focus(); } catch (ex) { }
		});

    });


    function setDateTime(fieldset, value, setAsDefault, minimum) {
		
        setAsDefault = !(setAsDefault === false);
		
		var now = moment();

		var date = moment(value).format("YYYY-MM-DD");


		//console.log(date);
		var dateset = false;

        var dateSelect = fieldset.querySelector("select.date");
		if(!dateset && !!dateSelect) {

			var selectDate = date;

			if(value.isSame(now, "day")) selectDate = "0";
			if(value.isSame(now.add({ days: 1 }), "day")) selectDate = "+1";

            _.each(dateSelect.options, function(option) {

                if(option.value == selectDate) {
                    if(!!setAsDefault) option.setAttribute("selected", "selected");
                    dateSelect.selectedIndex = option.index;
                    dateSelect.value = option.value; 
                    //dateset = true; 
                } else {
                    if(!!setAsDefault) option.removeAttribute("selected");
                }

            });
            dateSelect.dispatchEvent(new CustomEvent("change", { bubbles: true }));
        }

        var dateInput = fieldset.querySelector("input[type='date']");
		if(!dateset && !!dateInput) {
            if(!!setAsDefault) dateInput.setAttribute("value", date);
            dateInput.value = date;
            dateInput.dispatchEvent(new CustomEvent("change", { bubbles: true }));
			//console.log("minimum", minimum);

			if(!!minimum) {
				var min = moment(minimum).format("YYYY-MM-DD");
				var max = moment(minimum).add({ years: 10 }).format("YYYY-MM-DD");
				dateInput.min = min;
				dateInput.max = max;
				if(!!setAsDefault) dateInput.setAttribute("min", min);
				if(!!setAsDefault) dateInput.setAttribute("max", max);
			}

        }

        var time = value.format("HH:00");
        var timeSelect = fieldset.querySelector("select.time");
        if(!!timeSelect) {
            _.each(timeSelect.options, function(option) {

                if(option.value == time) {
                    if(!!setAsDefault) option.setAttribute("selected", "selected");
                    timeSelect.selectedIndex = option.index;
                    timeSelect.value = option.value; 
                } else {
                    if(!!setAsDefault) option.removeAttribute("selected");
                }

            });
            timeSelect.dispatchEvent(new CustomEvent("change", { bubbles: true }));
        }

	};
 
	// init date pickers - THIS EXECUTES IMMEDIATELY
    var dateInit = new Promise(function(resolve) {
    
		_.each(document.querySelectorAll("select.date:empty"), function(select) {
			//while(select.firstChild) select.firstChild.remove();

			select.innerHTML = '<option value="0" label="Right now" selected="selected">Right now</option><option value="0" label="Later today">Today</option><option value="+1">Tomorrow</option>' + _.reduce(_.range(2, 7), function(html, i) {
				var m = moment().add({days:i});

				return html + ('<option value="' + m.format("YYYY-MM-DD") + '">' + m.format("ddd, MMM D") + '</option>');
			}, "") + '<option value="">Later date&hellip;</option>';
			
			select.dispatchEvent(new CustomEvent("change", { bubbles: true }));
		});
	

		_.each(document.querySelectorAll("select.time"), function(select) {
			//while(select.firstChild) select.firstChild.remove();

			select.innerHTML = _.reduce(_.range(0, 23 + 1), function(html, h) {
				var m = moment({hour: h, minute: 0});
				return html + '<option value="' + m.format("HH:mm") + '">' + m.format("h A") + '</option>';
			}, "");

			select.dispatchEvent(new CustomEvent("change", { bubbles: true }));
		});
		

		var freeFormDateStart = moment().add({ days: 7 }).format("YYYY-MM-DD");

		_.each(document.querySelectorAll("input[type='date']"), function(input) {
			if(!!input.value) return;
			input.setAttribute("value", input.value = freeFormDateStart);
			input.dispatchEvent(new CustomEvent("change", { bubbles: true }));
		});

        _.each(document.querySelectorAll("fieldset.datetime"), function(dateTime) {
            var date = moment();
            var future = dateTime.getAttribute("data-future")
            if(!!future) date = date.add(moment.duration(future));
			var past = dateTime.getAttribute("data-past");
			//console.log("past", past);
			if(!!past) date = date.subtract(moment.duration(past));
            setDateTime(dateTime, date, true, !!future ? new Date() : null);
        });

	
		resolve();

    	    
    });
	
}(document.documentElement));