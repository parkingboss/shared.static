(function(api,postal) {
	
	var store = api.Locations = {
        predefined: true,
	};
	
	// allocate a search engine
	var _lookup = store.Lookup = api.IDKeyValueSearcher(true, false);
	
	_.extend(store, {
        isValid:_lookup.isValid,
		lookup:function(query, omitNoMatchItem, allIfEmpty) {
            
            if(omitNoMatchItem !== true && omitNoMatchItem !== false) omitNoMatchItem = store.predefined;
            if(allIfEmpty !== true && allIfEmpty !== false) allIfEmpty = store.predefined;
			
			query = query || "";
            if(!!query) query = query.toUpperCase();
			
			
			// we need engine
			return _lookup.search(query, allIfEmpty).then(function(result) {
				
				//console.log(results);
                
                var normalized = result.query;
                var results = result.results;
                
                //console.log(results);
						
				if(!omitNoMatchItem && (query != null && query.length >= 1) && (results.length == 0 || !_.find(results, ['value', query]))) results.push({
					id:normalized,
					key:normalized,
					value:query,
				}); // prefill query if no match so far
				
				//var now = Date.now();
				
				var items = _.reduce(results, function(items, item) { // make sure plates unique
					
					if(item.value == query) items.unshift({
						id:item.id,
						key:item.key,
						display:item.value,
                        url:item.url,
					});
					else items.push({
						id:item.id,
						key:item.key,
						display:item.value,
                        url:item.url,
					});
					
					return items;
					
				}, []);
				
				
				
				return {
					query: query,
                    normalized:normalized,
					items: items,
                    updated: result.updated,
				};
						

			});
			
		},
		add:store.Lookup.add,
	});
	
	var last = null;

	function onTopic(data, envelope) {
				
		var ts = new Date(data.generated || data.ts);
		
		if(!!last && last.getTime() >= ts.getTime()) return; // this isn't newer
		
		store.add(_.reduce(data.items, function(list, item, key) {
			
			list.push({
				id:item.id,
				key:item.name,
				value:item.name,
				url:item.admin,
				//display:item.display,
			});
			

			return list;

		}, []), data.generated);

		//last = ts;
	};
	
	_.each([
		"locations.items",
		//"locations.items.updated",
	], function(topic) {
		postal.subscribe({
			topic:topic,
			callback:onTopic,
		});
	});
	
})(ParkIQ.API, postal);