var ParkIQ = window.ParkIQ = ParkIQ || {};

ParkIQ.API = ParkIQ.API || {
	__base:null,
};

//if(window.location.hostname === "localhost") ParkIQ.API.__base = "https://api-dev.parkingboss.com/";
if(window.location.hostname === "homeone.local") ParkIQ.API.__base = "https://api-dev.parkingboss.com/";

(function(api, postal) {

    _.extend(api, {
        base: function(val) {
            if(!!!val) api.__base = val;
            return api.__base || (api.__base = document.querySelector("link[rel~='index'][rel~='api'][type~='application/json']").href);
        },
        cache: {
            _items: {},
            _timestamps:{},
            get:function(id) {
                if(!id) return null;
                var obj = api.cache._items[id];
                if(!obj) return null;
                return {
                    id: id,
                    value: obj,
                    timestamp:api.cache._timestamps[id],
                };
            },
            set:function(id, value, timestamp) {
                if(!id || !value || !timestamp) return null;
                api.cache._items[id] = value;
                api.cache._timestamps[id] = timestamp;
                return api.cache.get(id);
            },
        },
        _error:function(response, json) {

            //console.log("_error", response, json);

            var error = new Error(!!json && !!json.message ? json.message : !!response ? response.status + " " + response.statusText : "Fetch error"); // message text
            if(!!response) {
                error.status = response.status;
                error.statusText = response.statusText;
                error.type = response.type;
                error.url = response.url;
                error.response = response;
            }
            error.data = json;
            var customData = {};
            if(!!json) customData.data = json;
            if(!!response) _.extend(customData, response);

            try {
                throw error;
            } catch (e) {
                if(!!window.rg4js) {
                    //console.log("rg4js sending", e, customData);
                    rg4js('send', {
                        error: e,
                        customData:customData,
                    });
                } else if(!!window.Raygun && Raygun.send) Raygun.send(e, !!json ? _.extend({
                    data:json,
                }, response) : response);
            }



            return error;
            //throw error;
        },
        _status: function(response) {
            if (!!response.ok) return Promise.resolve(response);

            //throw api._error(response);
            return Promise.reject(api._error(response));

        },
        _json: function(response) {
            return response.json();
        },
        _catch:function(error) {

            //console.log("onError", error);

            api._unauthorized(error); // this would succeed if unauthorized, reject if not for continued catching

            // do some automated error catching here..
            /*
            if(!!error && error.status > 0) {
                switch(error.status) {
                    case 404:
                        //alert("Not Found");
                        break;
                    case 401:
                        document.documentElement.dispatchEvent(new CustomEvent("unauthorized", { bubbles: true }));
                        break;
                    default:
                        //alert("Loading Error");
                        break;
                }
                //return error;
                //return;
            }
            */

            if(window.Raygun && Raygun.send) Raygun.send(error, error.response) // no custom data;

            //alert("Network Failure");

            return Promise.reject(error); // need to keep the rejection flowing...

        },
        _notfound:function(error) {
            if(!!error && error.status == 404) return Promise.resolve({ status: error.status });
            return Promise.reject(error);
        },
        _unauthorized:function(error) {
            if(!!error && error.status == 401) {
                document.documentElement.dispatchEvent(new CustomEvent("unauthorized", { bubbles: true }));
                //return Promise.resolve({ status: error.status });
            }
            return Promise.reject(error);
        },
        _process:function(response) {

            //console.log(response);
            //response = response.clone();


            return response.text()
            .then(function(responseText) {

                //console.log("processed text");
                //console.log(responseText);


                return Promise.resolve(responseText)
                .then(JSON.parse)
                .then(function(json) {

                    //console.log("processed json");
                    //console.log(data);
                    //console.log(response.headers);
                    var date = response.headers.get("Date") || response.headers.get("date");
                    //console.log("date", date);
                    if(!!date) json.date = new Date(date).toISOString();
                    var modified = response.headers.get("Last-Modified") || response.headers.get("last-modified");
                    if(!!modified) json.modified = new Date(modified).toISOString();
                    json.status = response.status;

                    if(response.ok) return Promise.resolve(json);

                    //throw api._error(response, json);
                    return Promise.reject(api._error(response, json));

                }, function(jsonParseError) {

                    //console.log("no json");

                    // no json
                    if(response.ok) return Promise.resolve({ status: response.status, body: responseText });

                    /*
                    throw api._error(response, {
                        message: responseText
                    });
                    */
                    return Promise.reject(api._error(response, {
                        message: responseText
                    }));

                });


            }, function(textParseError) {

                // no response text
                if(response.ok) return { status: response.status };

                //throw api._error(response);
                return Promise.reject(api._error(response));

            })
            .catch(api._catch); // this will pass back any rejections

        },
        _entities:function(json) {
            if(!json) return json;

            var location = _.get(json, ["locations", "items", _.get(json, "locations.item")], json.location);

            var user = _.get(json, ["users", "items", _.get(json, "users.item")]) || _.get(json, "users.item", json.user);
            //console.log("entities.user", user);


        //console.log("entities.location", location);
            var attendant = _.get(json, ["attendants", "items", _.get(json, "attendants.item")]);

            // tenants?
            // vehicles?
            // permits?

            if(!!postal && !!user) _.each([
                //"user",
                //"user.updated",
                "user.item"
            ], function(topic) {
                postal.publish({
                    topic   : topic,
                    data    : {
                        generated: json.generated,
                        requested: json.requested,
                        item: user,
                    }
                });
            });

            if(!!postal && !!location) _.each([
                //"location",
               //"location.updated",
                "location.item",
                //"locations.item"
            ], function(topic) {
                postal.publish({
                    topic   : topic,
                    data    : {
                        generated: json.generated,
                        requested: json.requested,
                        item: location,
                    }
                });
            });

            if(!!postal && !!attendant) _.each([
                //"attendant",
                //"attendant.updated",
                "attendant.item"
            ], function(topic) {
                postal.publish({
                    topic   : topic,
                    data    : {
                        generated: json.generated,
                        requested: json.requested,
                        item: attendant,
                    }
                });
            });

            return json;
        },
    });
    api.response = {
        ok: api._status,
        status: api._status,
        json: api._json,
        process: api._process,
        onError:api._catch,
        error:api._catch,
        entities:api._entities,
        notfound:api._notfound,
        unauthorized:api._unauthorized,
    };

})(ParkIQ.API, window.postal);