var ImagePrinter = window.ImagePrinter = ImagePrinter || {};


(function() {

	var printer = ImagePrinter;
    
    var _onload = 'setTimeout(function() { window.focus();window.print();window.parent.focus(); }, 50);';
		
    var _template = _.template('<!DOCTYPE html><html><head><meta charset="utf-8" /><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" /><title><%= title %></title><style>body { padding:0; margin:0; text-align:center; font-size:12pt; font-family:helvetica,arial,sans-serif; } img, object { border:0; } img { width: 100%; max-width:7in; max-height:9.25in; } object { width:7in; height:8.8in; } @page { margin:0.75in 0.75in 0.25in 0.75in; size:8.5in 11in portrait; size:letter portrait; } </style></head><body onload="<%= onload %>"><%= pass %></body></html>');
    
    var print = Promise.method(function(w) {
        
        w.focus();
        w.print();
        try {
            if(w != w.parent) w.parent.focus();
        } catch(error) {
            
        }
        
        return Promise.resolve(w);
    });

	printer.print = function(url, type) {
        var printed = new Promise(function(resolve, reject) {
            
            if(!url) return reject(new Error("No URL"));

            var render = '<img src="' + url + '">';

            var doc = _template({ title: "Print", pass: render, onload: !!render && false ? _onload : "" });				

            var container = document.createElement("div");
            container.innerHTML = '<iframe style="width:0;height:0;margin:0;padding:0;position:absolute;border:0;" sandbox="allow-scripts allow-modals allow-same-origin"></iframe>';
            var iframe = container.childNodes[0];
            
            iframe.addEventListener("load", function(e) {
                
                if("about:blank" == this.src) return null;
                
                //if(!!render) return resolve(url); // everything is pre-rendered
                
                return print(this.contentWindow).then(function() {
                    return resolve(url);
                });
                    
            });

            iframe.setAttribute("srcdoc", doc);
            (document.body || document.querySelector("body")).appendChild(iframe);
            
            if(!("srcdoc" in document.createElement("iframe"))) {
                var jsSrc = "javascript: window.frameElement.getAttribute('srcdoc');";

                iframe.setAttribute("src", jsSrc);
                if (!!iframe.contentWindow) {
                    iframe.contentWindow.location = jsSrc;
                }
            }

        });
        
        printed.then(function(url) {
            
            document.documentElement.dispatchEvent(new CustomEvent("track", {
                bubbles: true,
                detail: {
                    category: "image.printer", 
                    action: "printed", 
                    label: url,
                },
            }));
            document.documentElement.dispatchEvent(new CustomEvent("printed", {
                bubbles: true,
                detail: {
                    url: url,
                },
            }));
            
        });
        
        return printed;
    };
}());