(function(api) {

    // this only supports a single location set...SPA-changing locations isn't supported
    // to do so we'd have to replace the simple promise with an eventing system

    //console.log("initing location");

    api.Location = api.Location || {};

	var resolveID;
    var resolvedID = new Promise(function() {
        resolveID = arguments[0];
    });

    var init = api.Location.id = function(val) {
        if(!!val) resolveID(val);
		return resolvedID;
    };

    // function resolve(ctx, next) {
    //     var id = ctx.params.location;
    //     if(!!id) _.defer(init, id)
    //     next();
    // }

    // use page to init as fast as possible - this doesn't work right
    // page("*", resolve);
    // page("/:location/*", resolve);

    function basify(location) {

        if(!!location) {
            var base = document.querySelector("base");
            if(!base) {
                base = document.createElement("base");
                base.setAttribute("href", "/" + location + "/");
                document.querySelector("head").prepend(base);
            }
        }

    }

    // function load(location) {
    //     //console.log("loading location");
    //     var requested = new Date().toISOString();
    //     var url = "v1/locations/" + location + "/attendant";
    //     return Promise.resolve(api.base())
    //     .then(function(base) {
    //         return api.fetch("GET", base + url);
    //     })
    //     .catch(function(error) {
    //         console.log("caught error", error);
    //     });
    // };
    

    var _location;

    // run as soon as we have an ID
    init().then(function(location) {
        basify(location); // asap
        //_.defer(load, location);
        _location = location;
    });
    
    // update every 12 min
    // window.setInterval(function() {
    //     if(!!_location) load(_location);
    // }, 12 * 60 * 1000);

}(ParkIQ.API));