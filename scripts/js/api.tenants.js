(function(api, postal) {
	
	var store = api.Tenants = {
        predefined: true,
	};

    if(!api.IDKeyValueSearcher) return;
	
	// allocate a search engine
	var _lookup = store.Lookup = api.IDKeyValueSearcher(true, false);
	
	_.extend(store, {
        isValid:_lookup.isValid,
		lookup:function(query, omitNoMatchItem, allIfEmpty) {
            
            if(omitNoMatchItem !== true && omitNoMatchItem !== false) omitNoMatchItem = store.predefined;
            if(allIfEmpty !== true && allIfEmpty !== false) allIfEmpty = store.predefined;
			
			query = query || "";
            if(!!query) query = query.toUpperCase();
			
			
			// we need engine
			return _lookup.search(query, allIfEmpty).then(function(result) {
				
				//console.log(results);
                
                var normalized = result.query;
                var results = result.results;
						
				if(!omitNoMatchItem && (query != null && query.length >= 1) && (results.length == 0 || !_.find(results, ['value', query]))) results.push({
					id:normalized,
					key:normalized,
					value:query,
				}); // prefill query if no match so far
				
				//var now = Date.now();
				
				var items = _.reduce(results, function(items, item) { // make sure plates unique
					
					if(item.value == query) items.unshift({
						id:item.id,
						key:item.key,
						display:item.value,
					});
					else items.push({
						id:item.id,
						key:item.key,
						display:item.value,
					});
					
					return items;
					
				}, []);
				
				
				
				return {
					query: query,
                    normalized:normalized,
					items: items,
                    updated: result.updated,
				};
						

			});
			
		},
		add:store.Lookup.add,
	});
	
    var last = null;

    function updater(tenants) {
        var ts = new Date(tenants.generated || tenants.ts);
        
        if(!!last && last.getTime() >= ts.getTime()) return; // this isn't newer
        
        if(!!tenants.predefined) store.predefined = true;

        //console.log("tenants updater", tenants);
        
        store.add(_.reduce(tenants.items, function(list, item, key) {
            
            list.push({
                id:_.isString(item) && _.isString(key) && !item.key ? key : _.get(item, "key", item),
                key:_.isString(item) && _.isString(key) && !item.key ? key : _.get(item, "key", item),
                value:_.isString(item) ? item : _.get(item, "display", item),
                //display:item.display,
            });

            return list;

        }, []), tenants.generated);
    };

    _.each([ "tenants.items" ], function(topic) {
        postal.subscribe({
            topic:topic,
            callback:updater,
        });
    });
    
    // postal.subscribe({
	// 	topic:"tenants.items.updated",
	// 	callback:function(data, envelope) {
			
	// 		var ts = new Date(data.generated || data.ts);
			
	// 		if(!!last && last.getTime() >= ts.getTime()) return; // this isn't newer
            
    //         if(!!data.predefined) store.predefined = true;
			
	// 		api.Tenants.add(_.reduce(data.items, function(list, item, key) {
				
	// 			list.push({
	// 				id:item.key,
	// 				key:item.key,
	// 				value:item.display,
	// 				//display:item.display,
	// 			});
				
	
	// 			return list;
	
	// 		}, []), data.generated);

    //         //last = ts;
	// 	},
	// });

})(ParkIQ.API, postal);

(function(api, postal, page) {
    
    _.extend(api.Tenants, {
        all:Promise.method(function(location) {

                
            var requested = new Date().toISOString();
            var url = "v1/locations/" + location + "/tenants";
            return Promise.resolve(api.base())
            .then(function(base) {
                return api.fetch("GET", base + url, null, _.invoke(api, "Auth.header", location));
            })
            .then(function(json) {

                json.requested = requested;
                
                //var items = _.get(json, "tenants.items");

                var items = _.map(_.get(json, "tenants.items"), function(val, key) {
                    return {
                        id: _.get(val, "id") || (_.isString(key) ? key : null),
                        key: _.get(val, "key") || (_.isString(key) ? key : null) || (_.isString(val) ? val : null),
                        display: _.get(val, "display") || (_.isString(val) ? val : null),
                    };
                    
                });
                
                postal.publish({
				    topic   : "tenants.items",
				    data    : {
				       generated: json.generated,
					   items: items,
				    }
				});

                return {
                    items:items,
                    generated:json.generated,
                };
 

            })
            //.catch(api.response.error)
            .catch(function(error) {
                // custom error handing
                console.log(".catch logging tenants error", error);
            });
         }),
    });
    
    var location;
    
    function load(location) {
        return api.Tenants.all(location);
    };
    
    // only run this once
    var init = _.once(load);

    if(!!page) {

        function resolve(ctx, next) {
            location = ctx.params.location;
            if(!!location) init(location);
            next();
        }

        page("*", resolve);
        page("/:location/*", resolve);

    }
    if(!!postal) {
        function updater(data) {
            //if(!!data && !!data.item) location = data.item.id;
            location = _.get(data, "item.id") || _.get(data, "id");
            if(!!location) init(location);
        };
        _.each([ "location.item" ], function(topic) {
            postal.subscribe({
                topic    : topic,
                callback : updater,
            });
        });
        
    }
    
    window.setInterval(function() {
        if(!location) return;
        load(location);
    }, 10 * 60 * 1000);
    
}(ParkIQ.API, window.postal));

(function(api, postal) {
	
    var pickDate = function(item) {
					
        // we only care about past expired dates
        if(!!item.valid && !!item.valid.to && Date.parse(item.valid.to.utc || item.valid.to) < Date.now()) return item.valid.to.utc || item.valid.to;
        
        // don't care about start date
        
        return item.issued.utc || item.issued;
    };
    
    _.extend(api.Tenants, {
        get:Promise.method(function(location, id, subset) {

            var pastDays = subset ? 30 : 365;
            var valid = dateFns.format(dateFns.subHours(new Date(), (pastDays * 24) - 6)) + "/" + dateFns.format(dateFns.addHours(new Date(), 6));
            var contact = !!api.contact;
            var sent = !subset && !!api.sent;
            
            var requested = new Date().toISOString();

            return Promise.join(api.base(), location, id, function(base, location, tenant) {
                var url = base + "v1/locations/" + location + "/tenants/" + tenant + "?valid=" + valid + "&usage=true&contact=" + contact + "&sent=" + sent;
                return api.fetch("GET", url, null, api.Auth.header(location));
			}).then(function(json) {
                
            // var requested = new Date().toISOString();
            // var url = (!!location ? "locations/" + location + "/tenants/" : "vehicles/") + id + "?files=true&used=true&ts=" + requested + (!!subset ? "&past=30.00:00&future=06:00" : "") + "&contact=" + (!!api.contact) + "&sent=" + (!!api.sent);
            // return Promise.resolve(api.base())
            // .then(function(base) {
            //     return api.fetch("GET", base + url, null, api.Auth.header(location));
            // })
            // .then(function(json) {

                json.requested = requested;
				
				var item = _.get(json, "tenants.item");
				if(!!item) item = _.get(json, ["tenants", "items", item], item);

                if(!item) return json;
                
                var tenantType = _.get(json, "tenants.type");
				
				if(!!item) item.type = item.type || tenantType;
				
				if(!!item) item.latest = _.get(json, "tenants.latest", item.id);
                
                var location = _.get(json, "locations.items." + _.get(json, "locations.item"), json.location);
                var permits = _.get(json, "permits.items");
                var violations = _.get(json, "violations.items");
                var types = _.get(json, "media.types.items") || _.get(json, "media.types");
				var users = _.get(json, "users.items");
				var vehicles = _.get(json, "vehicles.items");
				var tenants = _.get(json, "tenants.items");
                var spaces = _.get(json, "spaces.items");
                var media = _.get(json, "media.items");
                var attendants = _.get(json, "attendants.items", []);
                var status = _.get(json, "usage.items", []);
                var attendants = _.get(json, "attendants.items", []);
                
                var notes = json.notes;
                var files = _.get(json, "files.items");
                var permittables = _.get(json, "permittables.items");

                var exceptions = _.map(_.get(json, "violations.exceptions.items"), function(item) {
                    item.__type = "exception";
                    item.threshold = _.get(json, ["violations", "thresholds", "items", item.threshold], item.threshold);
                    if(!!item.threshold.period) {
                        var val = item.threshold.period.iso || item.threshold.period;
                        var dur = moment.duration(val);
                        //console.log(dur);
                        item.threshold.period = {
                            iso:dur.toJSON(),
                            //value:val,
                            display:(dur.asHours() <= 72 ? Math.ceil(dur.asHours()) + "h" : dur.format(dur.hours() == 0 ? "d[ days]" : "d[d], h[h]")),
                        };
                    }
                    return item;
                });
                
                permits = _.map(permits, function(item) {
                    
                    item.__type = "permit";
					
					api.Items.vehicle(item, vehicles);
					api.Items.tenant(item, tenants);
                    api.Items.space(item, spaces);
                    api.Items.media(item, media);
					
					api.Items.user(item.issued, users);
                    api.Items.user(item.updated, users);
					api.Items.notes(item, notes);
                    api.Items.sent(item, json.sent);

                    item.contact = _.get(json, ["contacts", "items", item.id]);
                    
                    if(!!item.tenant) item.tenant.type = item.tenant.type || tenantType;
                    if(!!item.media && item.media.type && !!types && _.isString(item.media.type)) item.media.type = types[item.media.type];

                    if(!!item.attendant) item.attendant = _.get(attendants, item.attendant) || _.find(attendants, ["id", item.attendant]) || item.attendant;
                    
                    return api.Permits.normalize(item);
                    
                });
                
                violations = _.map(violations, function(item) {
                    item.__type = "violation";
			
                    api.Items.vehicle(item, vehicles);
					api.Items.tenant(item, tenants);
					api.Items.space(item, spaces);

                    api.Items.user(item.issued, users);
                    api.Items.notes(item, notes);
                    api.Items.files(item, files);
                    api.Items.sent(item, json.sent);
                    
                    if(!!item.tenant) item.tenant.type = item.tenant.type || tenantType;
                    
                    return api.Items.normalize(item);
                });
                
                var latestFor = {};
                permittables = _.map(permittables, function(item) {
                    var latest = latestFor[item.container];
                    if(!latest || Date.parse(item.issued.utc) > Date.parse(latest.issued.utc)) latestFor[item.container] = item;
                    return item;
                });
		
                // map again
                permittables = _.map(permittables, function(item) {
                    item.__type = "permittable";
                    
                    api.Items.user(item.issued, users);
                    api.Items.notes(item, notes);
                    
                    var latest = latestFor[item.container];
                    item.latest = !!latest && latest.id == item.id;
                    
                    if(item.scope) item.attendant = _.get(attendants, item.scope) || _.find(attendants, ["id", item.scope]);
                    
                    api.Items.tenant(item, tenants, "container");
                    
                    if(!!item.tenant) item.tenant.type = item.tenant.type || tenantType;

                    return api.Items.normalize(item);
                });
                
                latestFor = null;

                var status = _.chain(status).map(function(item) {

                    item.__type = "attendantstatus";

                    api.Items.vehicle(item, vehicles);
                    api.Items.tenant(item, tenants);

                    if(!!item.attendant) item.attendant = _.get(attendants, item.attendant) || _.find(attendants, ["id", item.attendant]) || item.attendant;
                    if(!!item.permittable) {
                        var permittable = _.get(permittables, item.permittable, _.find(permittables, ["id", item.permittable]));
                        if(!!permittable) {
                            item.permittable = permittable;
                            permittable.attendant = item.attendant;
                        }
                    }
                    if(!!item.permit) item.permit = _.get(permits, item.permit, _.find(permits, ["id", item.permit]) || item.permit);

                    _.each(["used", "duration", "allowed", "remaining"], function(key) {
                        //if(!item[key]) return;
                        api.Items.duration(item, key);
                    });

                    if(!!item.limit) _.each(["allowed"], function(key) {
                        api.Items.duration(item.limit, key);
                    });

                    return item;
                }).value();
                
                var itemNotes = _.map(api.Items.notes(item, notes).notes, function(item) {
                    item.__type = "note";
                    api.Items.user(item.issued, users);
                    return api.Items.normalize(item);
                });
                
                if(!!location) postal.publish({
				    topic   : "location.item",
				    data    : {
				        requested:json.requested,
                        generated:json.generated,
					    item: location,
				    }
				});
                
                postal.publish({
				    topic   : "tenants.items",
				    data    : {
                        requested:json.requested,
                        generated:json.generated,
                        items: _.filter(tenants, "id"),
                        predefined: _.get(json, "tenants.predefined"),
				    }
				});
                
                postal.publish({
				    topic   : "vehicles.items",
				    data    : {
				        requested:json.requested,
                        generated:json.generated,
					    items: _.map(vehicles),
				    }
				});
                
                postal.publish({
				    topic   : "spaces.items",
				    data    : {
				        requested:json.requested,
                        generated:json.generated,
					    items: _.map(spaces),
                        predefined: _.get(json, "spaces.predefined"),
				    }
                });
                
                var alerts = [].concat(_.filter(permittables, { latest: true, permittable: false }), exceptions);
                var validPermits = _.filter(permits, api.Permits.isValidOrGrace);
                
                return {
                    type:"tenant",
                    item:item,

                    requested:json.requested,
                    generated:json.generated,

                    activity: {
                        partial:false,
                        items: [].concat(
                            //_.filter(permits, api.Permits.isValidOrPendingOrGrace)
                            _.chain(permits).filter(api.Permits.isValidOrGrace).orderBy([ "issued.utc" ],[ "desc" ]).value()
                            , status
                            , exceptions
                            //, _.filter(permittables, { "permittable": false, "latest" : true })
                        ),
                        /*.sort(function (a, b) {
                            
                                // sort banned top
                                if(a.permittable === false) return -1;
                                if(b.permittable === false) return 1;

                                if(!!a.threshold) return 1;
                                if(!!b.threshold) return -1;

                                return(function (x, y) {
                                    return x < y ? 1 : x > y ? -1 : 0;
                                }(a.issued.utc, b.issued.utc));
                                
                                //return Sorter.desc(a.issued.utc, b.issued.utc);
                
                            }).concat(exceptions, status),*/
                    },
                    history: {
                        partial:!!subset,
                        items: [].concat(
                            //_.filter(permits, api.Permits.isExpired),
                            //_.filter(permittables, "issued.user"),
                            //permitsTimeline,
                            _.filter(violations, "issued.user"),
                            itemNotes).sort(function (a, b) {
                        
                            return(function (x, y) {
                                return x < y ? 1 : x > y ? -1 : 0;
                            }(pickDate(a), pickDate(b)));
                            //return Sorter.desc(pickDate(a), pickDate(b))
            
                        }),
                    },

                    status: {
                        count:_.size(status),
                        items: status,
                    },

                    alerts: {
                        count:_.size(alerts),
                        items: alerts,
                    },

                    notes: {
                        count:_.size(itemNotes),
                        items:itemNotes,
                    },

                    permits: _.assign(json.permits, {
                        items:[].concat(_.orderBy(permits, [ "issued.utc" ], [ "desc" ])),
                        valid: {
                            count:_.size(validPermits),
                            items:validPermits,
                        },
                    }),

                    violations: _.assign(json.violations, {
                        items: _.orderBy(violations, [ "issued.utc" ], [ "desc" ]),
                    }),
                    
                };

            })
            .catch(function(error) {
                console.log("caught error", error);
            });
         }),
    });
    
}(ParkIQ.API, postal));
