(function(exports) {
	
	var normalize = function(val, relaxed) {
		if(!!relaxed) return (val || "").toUpperCase().replace(/[^A-Z0-9\s]/gi, "");
		return (val || "").toUpperCase().replace(/[^A-Z0-9]/gi, "").replace("O", "0");
	};
	
	exports.KeyValueSearcher = function(relaxed, numericSort) {
        
        var sorter = function(a,b) {
						
            var x = a.value;
            var y = b.value;
            
            if(!numericSort) return x < y ? -1 : x > y ? 1 : 0; // string sort
            
            if(x.length == y.length) return x < y ? -1 : x > y ? 1 : 0; // numeric type
            else if (x.length < y.length) return -1;
            else return 1;
        };
		
		var searcher = {
            _lunr:Promise.resolve(lunr(function () {
                this.field("value");
                this.ref("key");

                this.pipeline.remove(lunr.Pipeline.registeredFunctions['stopWordFilter']); // no stop word filter
            })),
		};
        
        var all = {
            cache: {},
            result: Promise.resolve({
                query:"",
                results:[],
            }),
        };
        
        searcher.isValid = function(keyValue) {
            
            if(!keyValue) return true;
            if(!all.cache) return true;
            if(_.isEmpty(all.cache)) return true;
            
            //console.log(idKeyValue);
            //console.log(idKeyValue.toUpperCase().replace(/\W+/g, ""));
            
            return !!_.find(all.cache, function(item) {
                //console.debug(item);
                return (item.key || "") === keyValue.toUpperCase().replace(/\W+/g, "");
            });

            //return true;
        };
		
		searcher.add = function(itemOrCollection) {
            
			var data = itemOrCollection.key ? [ itemOrCollection ] : _.map(itemOrCollection, function(item, key) {
				if(!!item.key) return item;
				
				// we be prepared to handle string-string maps
				return {
					//id:key,
                    key:key,
					value:item,	
				};
			});
            
            //console.debug(data);
            
            _.each(data, function(item) {
                all.cache[item.key] = item;
            });
            
            //console.log(all.map);
            
            // resort
            all.result = Promise.resolve({
                query:"",
                results:_.map(all.cache).sort(sorter),
            });
            
            searcher._lunr.then(function(index) {
                _.each(data, function(item) {
                    var newItem = _.clone(item);
                    newItem.value = normalize(newItem.value, relaxed); // normalize value before adding
                    index.add(newItem);
                    //console.log(newItem);
                });
            });
			
		};
		
		searcher.search = function(query, allIfEmpty) {
			
			query = normalize(query, relaxed);
			//console.log("normalized query = " + query);			
			// we need engine
            //console.log(all);
            if(!query && !!allIfEmpty) return all.result;
            
            return searcher._lunr.then(function(index) {
                return index.search(query);
            }).then(function(results) {
                //console.debug(results);
                // map the source onto results, sort by value
                // these are pre-sorted by score desc
                return _.map(results, function(item) {
                    return _.extend({
                        score:item.score,
                    }, all.cache[item.ref]);
                }).sort(sorter);
                
            }).then(function(results) {
                //console.debug(results);
                return {
                    query:query,
                    results:results,
                };
            });
			
		};
		
		return searcher;
		
	};
		
})(window);