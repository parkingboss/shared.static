(function(page, root) {
    
    page.exit("*", function(ctx, next) {
        //document.title = "Loading...";
        root.removeAttribute("data-records");
        root.removeAttribute("data-record");
        root.removeAttribute("data-record-view");
        next();
    });

    
}(window.page, document.documentElement));