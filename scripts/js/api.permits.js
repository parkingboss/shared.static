(function(api) {   
    
    var permits = api.Permits = api.Permits || {};
    
    function utc(obj) {
		return new Date(obj.utc || obj);	
	}

	var numeric = /[^0-9]/g;
	var timezone = /[-+]\d{2}:\d{2}$/g;

	function local(datestr) {
		if(!datestr || !datestr.replace) return datestr;
		return datestr.replace(timezone, "");
	}
    
    _.extend(permits, {
		validFrom:function(permit) {
			return _.get(permit, "valid.interval", "").split('/')[0];
		},
		validFromLocal:function(permit) {
			return local(permits.validFrom(permit));
		},
		validTo:function(permit) {
			return _.get(permit, "valid.interval", "").split('/')[1];
		},
		validToLocal:function(permit) {
			return local(permits.validTo(permit));
		},
		graceFrom:function(permit) {
			return _.get(permit, "grace.interval", "").split('/')[0];
		},
		graceTo:function(permit) {
			return _.get(permit, "grace.interval", "").split('/')[1];
		},
		issued:function(permit) {
			return _.get(permit, "issued.utc", "")
		},
		issuedLocal:function(permit) {
			return local(permits.issued(permit));
		},
		expired:function(permit) {
			return _.get(permit, "valid.expired", permit.expired);
		},
		// null/undefined ends are considered indefinite
		containsNow:function(aDate, bDate) {
			if(!aDate && !bDate) return true; // all time
			var now = Date.now();
			if(!aDate && !!bDate) return now <= utc(bDate).getTime();
			if(!!aDate && !bDate) return  utc(aDate).getTime() <= now;
			return utc(aDate).getTime() <= now && now <= utc(bDate).getTime();
		},
		isValidExcludingGrace:function(permit) {
			if(!!permits.expired(permit)) return false; // fast check
			return permits.containsNow(permits.validFrom(permit), permits.validTo(permit));
		},
		isValidIncludingGrace:function(permit) {
			return permits.containsNow(permits.graceFrom(permit) || permits.validFrom(permit), permits.graceTo(permit) || permits.validTo(permit));
		},
		isValidOrGrace:function(permit) {
			return permits.isValidIncludingGrace(permit);
		},
        // defaults to including grace
		isValid:function(permit, grace) {
			if(grace === false) return permits.isValidExcludingGrace(permit);
            return permits.isValidIncludingGrace(permit);
		},
		isNotValid:function(permit, grace) {
			if(grace === false) return !permits.isValidExcludingGrace(permit);
            return !permits.isValidIncludingGrace(permit);
		},
		isInGracePeriod:function(permit) {
			return permits.isValidIncludingGrace(permit) && !permits.isValidExcludingGrace(permit); // is only in before/after grace period
		},
        // defaults to including grace
		isPending:function(permit, grace) {
			if(!!permits.expired(permit)) return false; // expired can't be pending in any way
			if(!permits.validFrom(permit)) return false; // no start, can't be pending
			if(grace === false) return utc(permits.validFrom(permit)).getTime() > Date.now();
			return utc(permits.graceFrom(permit) || permits.validFrom(permit)).getTime() > Date.now(); // is only in before/after grace period
		},

        // defaults to excluding grace
		isExpired:function(permit, grace) {
			if(grace === false && !!permits.expired(permit)) return true;
			if(!permits.validTo(permit)) return false; // no end, can't be expired
			if(grace === false) return utc(permits.validTo(permit)).getTime() < Date.now(); // only consider valid to
			return utc(permits.graceTo(permit) || permits.validTo(permit)).getTime() < Date.now(); // try grace first
		},
		isRevoked:function(permit) {
			return !!permit.assigned && permits.isExpired(permit, false);
		},
		isValidOrPending:function(permit, grace) {
			return permits.isValid(permit, grace) || permits.isPending(permit, grace);
		},
		isValidOrPendingOrGrace:function(permit) {
			return permits.isValidOrPending(permit, true);
		},
		isCancelled:function(permit) {
			if(!!_.get(permit, "cancelled") || !!_.get(permit, "valid.cancelled")) return true;
			if(!permits.validFrom(permit) || !permits.validTo(permit)) return false; // must be fully valid
			return utc(permits.validFrom(permit)).getTime() == utc(permits.validTo(permit)).getTime(); // start and end are the same
		},
        status:function(permit, grace, valid, cancelled, pending, revoked, expired) {
			if(permits.isValid(permit, grace)) return !!valid || "" === valid ? valid : "valid";
            if(permits.isCancelled(permit, grace)) return !!cancelled || "" === cancelled ? cancelled : "cancelled";
			if(permits.isPending(permit, grace)) return !!pending || "" === pending ? pending : "pending";
			if(permits.isRevoked(permit, grace)) return !!revoked || "" === revoked ? revoked : "revoked";
            if(permits.isExpired(permit, grace)) return !!expired || "" === expired ? expired : "expired";
            return "";
        },

        normalize:function(item) {
			if(!item) return item;

			// if(!item.grace && !!item.valid && item.valid.grace) {
			// 	var ms = moment.duration(item.valid.grace).asMilliseconds();
			// 	item.valid.grace = item.grace = {
			// 		duration:item.valid.grace,
			// 		from:!!item.valid.from ? (new Date(Date.parse(item.valid.from.utc || item.valid.from) - ms)).toISOString() : null,
			// 		to:!!item.valid.to ? (new Date(Date.parse(item.valid.to.utc || item.valid.to) + ms)).toISOString() : null
			// 	}
			// 	//console.log(item.grace);
			// }
			
			api.Items.normalize(item);
			
			if(!!item.grace) {
				item.grace.now = permits.isInGracePeriod.bind(item, item); // consider replacing these with bind
			}
            
			item.cancelled = item.cancelled || permits.isCancelled(item);
            item.expired = item.expired || permits.isExpired(item, false);

			item.getStatus = permits.status.bind(item, item);
            
			if(!!item.valid) {
				item.valid.now = permits.isValid.bind(item, item);
				item.valid.pending = permits.isPending.bind(item, item);
				//item.valid.status = permits.status.bind(item, item);
                
				if(!!item.valid.duration) {
					var val = item.valid.duration.iso || item.valid.duration;
					var dur = moment.duration(val);
					item.valid.duration = {
						iso:dur.toJSON(),
						//value:val,
						display:(dur.asHours() <= 72 ? Math.ceil(dur.asHours()) + "h" : dur.format(dur.hours() == 0 ? "d[d]" : "d[d], h[h]")),
					};
				}
			}

			if(!!item.contact && !!item.contact.phone) {
				var test = item.contact.phone.replace(numeric, "");
				if(test.length == 10) item.contact.phone = test.substr(0,3) + "-" + test.substr(3,3) + "-" + test.substr(6, 4);
				if(test.length == 7) item.contact.phone = test.substr(0,3) + "-" + test.substr(3,4);
			}
			
			return item;
		},
        
    });
}(ParkIQ.API));