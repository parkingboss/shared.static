(function(api, postal, page) {
    
    var location;
    
    function load(location) {
        var requested = new Date().toISOString();
        var url = "v1/users/current";
        return Promise.join(api.base(), url, api.Auth.header(location), function(base, url, auth) {

            return api.fetch("GET", base + url, null, auth)
            .then(function(json) {

                json.requested = requested;

                var currentUser = _.get(json, ["users", "items", _.get(json, "users.item") ]);
                
                var locations = _.get(json, "locations.items", json.locations);

                var token = auth.split(" ")[1];
                var v = (new Date()).toISOString();
                if(!!token) _.each(locations, function(item) {
                    //item.admin = item.admin + "?access_token=" + token;

                    if(!item.admin || _.includes(item.admin, "frontdesk.")) return;                
                    item.admin = item.admin.split('?')[0] + "?version=" + v + "&access_token=" + token;
                });

                // if(!!currrentUser) postal.publish({
                //     topic   : "user.updated",
                //     data    : {
                //         generated: json.generated,
                //         requested: json.requested,
                //         items: currentUser,
                //     }
                // });

                // postal.publish({
                //     topic   : "user.locations",
                //     data    : {
                //         generated: json.generated,
                //         requested: json.requested,
                //         user: currentUser,
                //         items: _.map(locations),
                //     }
                // });

                postal.publish({
                    topic   : "user.locations.items",
                    data    : {
                        generated: json.generated,
                        requested: json.requested,
                        user: currentUser,
                        items: _.map(locations),
                    }
                });

                // postal.publish({
                //     topic   : "user.locations.items.updated",
                //     data    : {
                //         generated: json.generated,
                //         requested: json.requested,
                //         items: _.map(locations),
                //     }
                // });

                postal.publish({
                    topic   : "locations.items",
                    data    : {
                        generated: json.generated,
                        requested: json.requested,
                        items: _.map(locations),
                    }
                });

                return {
                    locations:locations,
                    generated:json.generated,
                };

            });
        })
        .catch(function(error) {
            console.log("caught error", error);
        });
    };
    
    // only run this once
    var init = _.once(load);

    if(!!page) {

        function resolve(ctx, next) {
            location = ctx.params.location;
            if(!!location) init(location);
            next();
        }

        page("*", resolve);
        page("/:location/*", resolve);

    }
    if(!!postal) postal.subscribe({
        topic    : "location.item",
        callback : function(data, envelope) {
            
            if(!!data && !!data.item) location = data.item.id;
            if(!!location) init(location);

            
        },
    });
    
}(ParkIQ.API, window.postal));