(function(api,postal) {
	
	var store = api.Vehicles = {
        predefined: false,
	};

    if(!api.IDKeyValueSearcher) return;
	
	// allocate a search engine
	var _lookup = store.Lookup = api.IDKeyValueSearcher(false, false);
	
	_.extend(store, {
        isValid:_lookup.isValid,
		lookup:function(query, omitNoMatchItem, allIfEmpty) {
            
            if(omitNoMatchItem !== true && omitNoMatchItem !== false) omitNoMatchItem = store.predefined;
            if(allIfEmpty !== true && allIfEmpty !== false) allIfEmpty = store.predefined;
			
			query = query || "";
            if(!!query) query = query.toUpperCase();
			
			
			// we need engine
			return _lookup.search(query, allIfEmpty).then(function(result) {
				
				//console.log(results);
                
                var normalized = result.query;
                var results = result.results;
                
                //console.log(results);
						
				if(!omitNoMatchItem && (query != null && query.length >= 1) && (results.length == 0 || !_.find(results, ['value', query]))) results.push({
					id:normalized,
					key:normalized,
					value:query,
				}); // prefill query if no match so far
				
				//var now = Date.now();
				
				var items = _.reduce(results, function(items, item) { // make sure plates unique
					
					if(item.value == query) items.unshift({
						id:item.id,
						key:item.key,
						display:item.value,
					});
					else items.push({
						id:item.id,
						key:item.key,
						display:item.value,
					});
					
					return items;
					
				}, []);
				
				
				
				return {
					query: query,
                    normalized:normalized,
					items: items,
                    updated: result.updated,
				};
						

			});
			
		},
		add:store.Lookup.add,
	});
	
	var last = null;
	
	postal.subscribe({
		topic:"vehicles.items",
		callback:function(data, envelope) {
			
			var ts = new Date(data.generated || data.ts);
			
			if(!!last && last.getTime() >= ts.getTime()) return; // this isn't newer
			
			api.Vehicles.add(_.reduce(data.items, function(list, item, key) {
				
				list.push({
					id:item.key, //item.id,
					key:item.key,
					value:item.display,
					//display:item.display,
				});
				
	
				return list;
	
			}, []), data.generated);

            //last = ts;
		},
	});
})(ParkIQ.API, postal);

(function(api, postal, page) {
    
    _.extend(api.Vehicles, {
        all:Promise.method(function(location) {

                
            var requested = new Date().toISOString();
            var url = "v1/locations/" + location + "/vehicles";
            return Promise.resolve(api.base())
            .then(function(base) {
                return api.fetch("GET", base + url, null, api.Auth.header(location));
            })
            .then(function(json) {

                json.requested = requested;
                
                var items = _.map(_.get(json, "vehicles.items"), function(val, key) {
                    return {
                        id: _.get(val, "id") || (_.isString(key) ? key : null),
                        key: _.get(val, "key") || (_.isString(key) ? key : null) || (_.isString(val) ? val : null),
                        display: _.get(val, "display") || (_.isString(val) ? val : null),
                    };
                    
                });
                
                postal.publish({
				    topic   : "vehicles.items",
				    data    : {
				       generated: json.generated,
					   items: items,
				    }
				});

                return {
                    items:items,
                    generated:json.generated,
                };
 

            })
            //.catch(api.response.error)
            .catch(function(error) {
                // custom error handing
                console.log(".catch logging vehicles error", error);
            });
         }),
    });
    
    var location;
    
    function load(location) {
        return api.Vehicles.all(location);
    };
    
    // only run this once
    var init = _.once(load);

    if(!!page) {

        function resolve(ctx, next) {
            location = ctx.params.location;
            if(!!location) init(location);
            next();
        }

        page("*", resolve);
        page("/:location/*", resolve);

    }
    if(!!postal) postal.subscribe({
        topic    : "location.item",
        callback : function(data, envelope) {
            
            if(!!data && !!data.item) location = data.item.id;
            if(!!location) init(location);

            
        },
    });
    
    window.setInterval(function() {
        if(!location) return;
        load(location);
    }, 3 * 60 * 1000);
    
}(ParkIQ.API, window.postal));

(function(api, postal) {
	
    var timelineDate = function(item) {

        if(!!item.sort) return item.sort.utc || item.sort;
					
        // we only care about past expired dates
        //if(!!item.valid && !!item.valid.to && Date.parse(item.valid.to.utc || item.valid.to) < Date.now()) return item.valid.to.utc || item.valid.to;
        
        // don't care about start date
        
        return item.issued.utc || item.issued;
    };
    
    _.extend(api.Vehicles, {
        get:Promise.method(function(location, id, subset) {

            var pastDays = subset ? 30 : 365;
            var valid = dateFns.format(dateFns.subHours(new Date(), (pastDays * 24) - 6)) + "/" + dateFns.format(dateFns.addHours(new Date(), 6));
            var contact = !!api.contact;
            var sent = !subset && !!api.sent;
            
            var requested = new Date().toISOString();

            return Promise.join(api.base(), location, id, function(base, location, vehicle) {
                var url = base + "v1/locations/" + location + "/vehicles/" + vehicle + "?valid=" + valid + "&usage=true&contact=" + contact + "&sent=" + sent;
                return api.fetch("GET", url, null, api.Auth.header(location));
			}).then(function(json) {
                
            // var requested = new Date().toISOString();
            // return Promise.resolve(api.base())
            // .then(function(base) {
            //     return base + (!!location ? "locations/" + location + "/vehicles/" : "vehicles/") + id + "?files=true&used=true&ts=" + requested + (!!subset ? "&past=30.00:00&future=06:00" : "") + "&contact=" + (!!api.contact) + "&sent=" + (!!api.sent) + (!subset ? "&similar=true" : "");
            // })
            // .then(function(url) {
            //     return api.fetch("GET", url, null, api.Auth.header(location));
            // })
            // .then(function(json) {

                json.requested = requested;
                
				var item = api.Items.get(json, "vehicles");
                var vehicle = item;

                if(!item) return json;
				
				/*var item = _.get(json, "vehicles.item");
				item = _.get(json, ["vehicles", "items", item], item);*/

                if(!!item) item.latest = _.get(json, "vehicles.latest", item.id);
                
                var tenantType = _.get(json, "tenants.type");
                
                var location = _.get(json, "locations.items." + _.get(json, "locations.item"), json.location);
                var permits = _.get(json, "permits.items");
                var violations = _.get(json, "violations.items");
                var types = _.get(json, "media.types.items") || _.get(json, "media.types");
				var users = _.get(json, "users.items");
				var vehicles = _.get(json, "vehicles.items");
				var tenants = _.get(json, "tenants.items");
                var spaces = _.get(json, "spaces.items");
                var media = _.get(json, "media.items");
                var notes = json.notes;
                var files = _.get(json, "files.items");
                var permittables = _.get(json, "permittables.items");
                var attendants = _.get(json, "attendants.items", []);
                var status = _.get(json, "usage.items", []);

                var exceptions = _.map(_.get(json, "violations.exceptions.items"), function(item) {
                    item.__type = "exception";
                    item.threshold = _.get(json, ["violations", "thresholds", "items", item.threshold], item.threshold);
                    if(!!item.threshold.period) {
                        var val = item.threshold.period.iso || item.threshold.period;
                        var dur = moment.duration(val);
                        //console.log(dur);
                        item.threshold.period = {
                            iso:dur.toJSON(),
                            //value:val,
                            display:(dur.asHours() <= 72 ? Math.ceil(dur.asHours()) + "h" : dur.format(dur.hours() == 0 ? "d[ days]" : "d[d], h[h]")),
                        };
                    }
                    return item;
                });
                
                permits = _.map(permits, function(item) {
                    
                    item.__type = "permit";
					
					api.Items.vehicle(item, vehicles);
					api.Items.tenant(item, tenants);
                    api.Items.space(item, spaces);
                    api.Items.media(item, media);
					
					api.Items.user(item.issued, users);
                    api.Items.user(item.updated, users);
					api.Items.notes(item, notes);
                    api.Items.sent(item, json.sent);

                    item.contact = _.get(json, ["contacts", "items", item.id]);
                    
                    if(!!item.tenant) item.tenant.type = item.tenant.type || tenantType;

                    if(!!item.attendant) item.attendant = _.get(attendants, item.attendant) || _.find(attendants, ["id", item.attendant]) || item.attendant;
                    
                    if(!!item.media && item.media.type && !!types && _.isString(item.media.type)) item.media.type = types[item.media.type];
                    
                    return api.Permits.normalize(item);
                    
                });

                // var permitsTimeline = _.reduce(permits, function(list, item) {

                //     // expired?
                //     if(!api.Permits.isValidOrPending(item)) list.push(_.extend(_.clone(item), {
                //         sort: item.valid.to,
                //         __type:"permitvalidto",
                //     }));// issued item

                //     // upcoming?
                //     if(!api.Permits.isPending(item)) list.push(_.extend(_.clone(item), {
                //         sort: item.valid.from,
                //         __type:"permitvalidfrom",
                //     }));// issued item

                //     /*
                //     list.push(_.extend(_.clone(item), {
                //         sort: item.issued,
                //         __type:"permitissued",
                //     }));// issued item
                //     */
                //     return list;
                // }, []);
                
                violations = _.map(violations, function(item) {
                    item.__type = "violation";
			
                    api.Items.vehicle(item, vehicles);
					api.Items.tenant(item, tenants);
					api.Items.space(item, spaces);

                    api.Items.user(item.issued, users);
                    api.Items.notes(item, notes);
                    api.Items.files(item, files);
                    api.Items.sent(item, json.sent);
                    
                    if(!!item.tenant) item.tenant.type = item.tenant.type || tenantType;
                    
                    return api.Items.normalize(item);
                });
                

                var latestFor = {};
                permittables = _.map(permittables, function(item) {
                    var latest = latestFor[item.container];
                    if(!latest || Date.parse(item.issued.utc) > Date.parse(latest.issued.utc)) latestFor[item.container] = item;
                    return item;
                });
		
                // map again
                permittables = _.map(permittables, function(item) {
                    item.__type = "permittable";
                    
                    api.Items.user(item.issued, users);
                    api.Items.notes(item, notes);
                    
                    var latest = latestFor[item.container];
                    item.latest = !!latest && latest.id == item.id;
                    
                    if(item.scope) item.attendant = _.get(attendants, item.scope) ||  _.find(attendants, ["id", item.scope]);
                    
                    api.Items.vehicle(item, vehicles, "container");

                    return api.Items.normalize(item);
                });
                
                latestFor = null;

                var attendantPermits = _.chain(attendants).map(function(attendant) {

                    // valid permits will get swept up in normal...
                    if(!attendant) return null;

                    // check if banned, if so, no further permit checking
                    if(!!_.find(permittables, { "scope":attendant.id, "latest": true, "permittable":false })) return null;

                    // no permits means we can skip all of this
                    if(permits.length <= 0) return {
                        __type: "nopermits",
                        attendant: attendant,
                        vehicle: vehicle,
                    };

                    if(!!_.find(permits, function(permit) {
                        //console.log("eval for valid", permit);
                        if(!permit.attendant || permit.attendant.id != attendant.id) return false; // doesn't match attendant
                        return api.Permits.isValidOrGrace(permit);
                    })) return null;

                    // check if has assigned permit
                    if(!!_.find(permits, function(permit) {
                        if(!!permit.expired) return false;
                        if(!permit.assigned) return false;
                        return true;
                    })) return null;

                    // shortcut, we've eliminated valid permits
                    return {
                        __type: "nopermits",
                        attendant: attendant,
                        vehicle: vehicle,
                    };

                    // // find next pending, last expired
                    // return _.chain(permits).filter(function(permit) {
                    //     //console.log("eval for pending", permit);
                    //     if(!permit.attendant || permit.attendant.id != attendant.id) return false; // doesn't match attendant
                    //     return api.Permits.isPending(permit, false);
                    // }).sortBy("valid.from.utc").first().value()
                    // || _.chain(permits).filter(function(permit) {
                    //     //console.log("eval for expired", permit);
                    //     if(!permit.attendant || permit.attendant.id != attendant.id) return false; // doesn't match attendant
                    //     return api.Permits.isExpired(permit, false);
                    // }).sortBy("valid.to.utc").last().value()
                    // || {
                    //     __type: "nopermits",
                    //     attendant: attendant,
                    // };

                }).filter().value();

                status = _.chain(status).map(function(item) {

                    item.__type = "attendantstatus";

                    api.Items.vehicle(item, vehicles);
                    api.Items.tenant(item, tenants);

                    if(!!item.attendant) item.attendant = _.get(attendants, item.attendant) || _.find(attendants, ["id", item.attendant]) || item.attendant;
                    if(!!item.permittable) {
                        var permittable = _.get(permittables, item.permittable, _.find(permittables, ["id", item.permittable]));
                        if(!!permittable) {
                            item.permittable = permittable;
                            permittable.attendant = item.attendant;
                        }
                    }
                    if(!!item.permit) item.permit = _.get(permits, item.permit, _.find(permits, ["id", item.permit]) || item.permit);

                    _.each(["used", "duration", "allowed", "remaining"], function(key) {
                        //if(!item[key]) return;
                        api.Items.duration(item, key);
                    });

                    if(!!item.limit) _.each(["allowed"], function(key) {
                        api.Items.duration(item.limit, key);
                    });
                    //console.log("item", item);

                    return item;
                }).filter(function(item) {
                    return !!item.permit || !!item.permittable || !!item.used;
                }).value();
                
                var itemNotes = _.map(api.Items.notes(item, notes).notes, function(item) {
                    item.__type = "note";
                    api.Items.user(item.issued, users);
                    return api.Items.normalize(item);
                });
                
                if(!!location) postal.publish({
				    topic   : "location.item",
				    data    : {
				        requested:json.requested,
                        generated:json.generated,
					    item: location,
				    }
				});
                
                
                postal.publish({
				    topic   : "tenants.items",
				    data    : {
				        requested:json.requested,
                        generated:json.generated,
					    items: _.map(tenants),
                        predefined: _.get(json, "tenants.predefined"),
				    }
				});
                
                postal.publish({
				    topic   : "vehicles.items",
				    data    : {
                        requested:json.requested,
                        generated:json.generated,
					    items: _.filter(vehicles, "id"),
				    }
				});
                
                postal.publish({
				    topic   : "spaces.items",
				    data    : {
				        requested:json.requested,
                        generated:json.generated,
					    items: _.map(spaces),
                        predefined: _.get(json, "spaces.predefined"),
				    }
				});
                
                /*
                postal.publish({
				    topic   : "media.items.updated",
				    data    : {
				        requested:json.requested,
                        generated:json.generated,
					    items: _.map(media),
                        predefined: _.get(json, "media.predefined"),
				    }
				});
                */

                //console.log(permitsTimeline);

                

                var validPermits = _.filter(permits, api.Permits.isValidOrGrace);

                var similar = validPermits.length >= 1 ? null : _.map(_.get(json, "vehicles.similar", []), function(id) {
                        return _.get(vehicles, id);
                    });

                var alerts = [].concat(_.filter(permittables, { latest: true, permittable: false }), exceptions);
                
                return {
                    type:"vehicle",
                    item:item,
                    requested:json.requested,
                    generated:json.generated,
                    similar: similar,
                    
                    activity: {
                        partial:false,
                        items: [].concat(
                            attendantPermits
                            , _.orderBy(validPermits, [ "issued.utc" ],[ "desc" ])
                            //, _.chain(permits).filter(api.Permits.isValidOrGrace).orderBy([ "issued.utc" ],[ "desc" ]).value()
                            //, _.filter(permits, api.Permits.isValidIncludingGrace) // only valid or grace
                            //, _.filter(permittables, { "permittable": false, "latest" : true })
                            , exceptions
                            , status
                        ),
                        /*
                        .sort(function (a, b) {
                            
                                // sort banned top
                                if(a.permittable === false) return -1;
                                if(b.permittable === false) return 1;

                                if(!!a.threshold) return 1;
                                if(!!b.threshold) return -1;

                                return(function (x, y) {
                                    return x < y ? 1 : x > y ? -1 : 0;
                                }(a.issued.utc, b.issued.utc));
                                
                                //return Sorter.desc(a.issued.utc, b.issued.utc);
                
                            }).concat(exceptions, status),
                            */
                    },
                    history: {
                        partial:!!subset,
                        items: [].concat(
                            //_.filter(permits, api.Permits.isExpired),
                            //_.filter(permittables, "issued.user"),
                            //permitsTimeline,
                            _.filter(violations, "issued.user"),
                            itemNotes
                            ).sort(function (a, b) {
                        
                                return(function (x, y) {
                                    return x < y ? 1 : x > y ? -1 : 0;
                                }(timelineDate(a), timelineDate(b)));
                                //return Sorter.desc(pickDate(a), pickDate(b))
                
                            }),
                    },

                    status: {
                        count:_.size(status),
                        items: status,
                    },

                    alerts: {
                        count:_.size(alerts),
                        items: alerts,
                    },

                    notes: {
                        count:_.size(itemNotes),
                        items:itemNotes,
                    },

                    permits: _.assign(json.permits, {
                        items:[].concat(attendantPermits, _.orderBy(permits, [ "issued.utc" ], [ "desc" ])),
                        valid: {
                            count:_.size(validPermits),
                            items:validPermits,
                        },
                    }),

                    violations: _.assign(json.violations, {
                        items: _.orderBy(violations, [ "issued.utc" ], [ "desc" ]),
                    }),
                };
                
                /*postal.publish({
                    topic   : "location.updated",
                    data    : {
                        generated: data.generated,
                        item:location,
                    }
                });
                
                postal.publish({
                    topic   : "address.updated",
                    data    : {
                        generated: data.generated,
                        item:address,
                    }
                });*/

            });
            /*
            .catch(function(error) {
                console.log("caught error", error);
            });
            */
         }),
    });
        
}(ParkIQ.API, postal));