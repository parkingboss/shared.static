(function(api) {

    var cal = {
        sameDay: 'ddd MMM D h:mm A',
        nextDay: 'ddd MMM D h:mm A',
        nextWeek: 'ddd MMM D h:mm A',
        lastDay: 'ddd MMM D h:mm A',
        lastWeek: 'ddd MMM D h:mm A',
        sameElse: 'MMM D YYYY h:mm A'
    };

    function calendar(dt, ref) {

        if(!dt) return "";

        ref = ref || new Date();

        if(dateFns.isSameYear(dt, ref)) return dateFns.format(dt, "MMM D h:mm A");

        return dateFns.format(dt, "MMM D YYYY h:mm A");

        // if(dateFns.isSameYear(dt, ref)) return moment(dt).calendar(ref, {
        //     lastDay : 'MMM D h:mm A',
        //     sameDay : 'MMM D h:mm A',
        //     nextDay : 'MMM D h:mm A',
        //     lastWeek : 'MMM D h:mm A',
        //     nextWeek : 'MMM D h:mm A',
        //     sameElse : 'MMM D h:mm A'
        // });

        // return moment(dt).calendar(ref, {
        //     lastDay : 'MMM D YYYY h:mm A',
        //     sameDay : 'MMM D YYYY h:mm A',
        //     nextDay : 'MMM D YYYY h:mm A',
        //     lastWeek : 'MMM D YYYY h:mm A',
        //     nextWeek : 'MMM D YYYY h:mm A',
        //     sameElse : 'MMM D YYYY h:mm A'
        // }); // use default
    }
    
    var vehicle = '<% if(!!item.vehicle) { %><data class="vehicle id" value="<%= item.vehicle.id %>"><%= item.vehicle.display || item.vehicle %></data><% } %>';
    var tenant = '<% if(!!item.tenant) { %><data class="tenant id <%= item.tenant.type %>" value="<%= item.tenant.id %>"><%= item.tenant.display %></data><% } %>';
    var space = '<% if(!!item.space) { %><data class="space id <%= item.space.type %>" value="<%= item.space.id %>"><%= item.space.display %></data><% } %>';
    var media = '<% if(!!item.media) { %><data class="media id <%= _.get(item, "media.type.format") %>" value="<%= item.media.id %>"><%= item.media.display %></data><% } %>';

    var validfrom = '<% if(!!validFrom) { %><time datetime="<%= validFrom %>" class="valid start from <%= new Date(validFrom) >= new Date() ? "future" : "past" %>"><%= moment(util.validFromLocal(item)).calendar(null, cal) %></time><% } %>';

    var validto = '<% if(!!validTo) { %><time datetime="<%= validTo %>" class="valid end to <%= new Date(validTo) >= new Date() ? "future" : "past" %>"><%= moment(util.validToLocal(item)).calendar(null, cal) %></time><% } %>';

    var valid = '<% var validFrom = util.validFrom(item), validTo = util.validTo(item); %><% if(!!validFrom || !!validTo) { %><p class="valid">' + validfrom + validto + '</p><% } %>';
    
    var issued = '<time class="issued" datetime="<%= util.issued(item) %>"><%= moment(util.issuedLocal(item)).calendar(null, cal) %><% if(!!_.get(item, "issued.user")) { %><data value="<%= _.get(item, "issued.user.id") %>" class="user" title="<%= _.get(item, "issued.user.display") %>"></data><% } %></time>';

    var url = '<% if(_.get(item, "url")) { %><a href="<%= _.get(item, "url") %>">Details</a><% } %>';
    
    // items without valid are presumed to be valid
    var meta = '<%= !!_.get(item, "issued.user") ? " user" : "" %><%= !!item.assigned ? " assigned" : "" %><%= !!item.media ? " media" : "" %><%= !!item.vehicle ? " vehicle" : "" %><%= !!item.space ? " space" : "" %><%= !!item.tenant ? " tenant" : "" %><%= !!item.visiting ? " pass" : "" %><%= util.isInGracePeriod(item) ? " grace" : "" %><%= !!item.getStatus ? " " + item.getStatus(true) : "" %>';
    var note = '<blockquote><p><%= item.text %></p></blockquote>';
    var notes = '<% if(!!item.notes && item.notes.length > 0) { %><ul class="notes"><% _.each(item.notes, function(item) { %><li class="user note">' + note + '<% }); %></ul><% } %>';
    
    api.Templates = api.Templates || {};
    api.Templates.permit = _.template('<li class="permit' + meta + '" data-record="permits/<%= item.id %>"><h1><%= _.get(item, "title") %></h1>' + media + vehicle + space + tenant + notes + url + '</li>', {
        variable:"item",
        imports: {
            //moment:moment,
            calendar: calendar,
            util: api.Permits,
        },
    });

    api.Templates.permitdetail = _.template('<dl class="permit' + meta + '" data-record="permits/<%= item.id %>"><dt class="valid">Valid</dt><dd class="valid">' + valid + '</dd><dt class="media <%= _.get(item, "media.type.format") %>">Media</dt><dd class="media <%= _.get(item, "media.type.format") %>">' + media + '</dd><dt class="vehicle">Vehicle</dt><dd class="vehicle">' + vehicle + '</dd><dt class="space">Space</dt><dd class="space">' + space + '</dd><dt class="tenant <%= _.get(item, "tenant.type") %>">Tenant</dt><dd class="tenant <%= _.get(item, "tenant.type") %>">' + tenant + '</dd><dt class="notes">Notes</dt><dd class="notes">' + notes + '</dd><dt class="issued">Issued</dt><dd class="issued">' + issued + '</dd></dl>', {
        variable:"item",
        imports: {
            moment:moment,
            cal: cal,
            calendar: calendar,
            util: api.Permits,
        },
    });


    api.Templates.permittable = _.template('<li class="permittable <%= item.permittable === true ? "dopermit" : item.permittable === false ? "donotpermit" : "" %>' + meta + '" data-record="permittables/<%= item.id %>">' + issued + media + vehicle + space + tenant + notes + '</li>', {
        "variable":"item",
    });
    api.Templates.violation = _.template('<li class="violation' + meta + '" data-record="violations/<%= item.id %>">' + issued + media + vehicle + space + tenant + notes + '<% if(!!item.files && item.files.length > 0) { %><ul class="photos files"><% _.each(item.files, function(item) { %><li class="file <%= item.type.split("/").join(" ") %>"><figure><a href="https://parking-uploads.imgix.net/<%= item.uuid %>/<%= item.name %>?auto=compress,enhance" target="_blank"><img src="https://parking-uploads.imgix.net/<%= item.uuid %>/<%= item.name %>?w=60&h=60&fit=crop&crop=entropy&auto=compress,enhance" /></a></figure></li><% }); %></ul><% } %></li>', {
        variable:"item",
        imports: {
            moment:moment,
            cal: cal,
            calendar: calendar,
            util: api.Permits,
        },
    });
    api.Templates.note = _.template('<li class="note' + meta + '" data-record="notes/<%= item.id %>">' + issued + media + vehicle + space + tenant + note + '</li>', {
        variable:"item",
        imports: {
            moment:moment,
            cal: cal,
            calendar: calendar,
            util: api.Permits,
        },
    });
    
}(ParkIQ.API));