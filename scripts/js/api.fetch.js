(function(api) {
    
    function json(response) {
        if(!response) return {};
        return response.text()
        .then(function(text) {

            if(!text) return {
                status: response.status
            };

            return Promise.resolve(text)
            .then(JSON.parse)
            .catch(function(error) {
                return {
                    status: response.status,
                    message: text
                };
            });

        })
        .catch(function(error) {
            return {
                status: response.status
            };
        });
    }

    api.fetch = function(method, url, body, auth) {

        // simple process
        //var requested = new Date().toISOString();
        return Promise.resolve(auth === true ? api.Auth.header() : auth === false ? null : auth) // we want to catch unauthorized auth issues
        .then(function(auth){

            return Promise.join(
            method,
            url,
            body, 
            auth,
            function(method, url, body, auth) {

                var options = {};
                if(!!method) _.set(options, "method", method);
                //if(!!auth) _.set(options, "headers.Authorization", auth);
                if(!!auth) url = url + (url.indexOf("?") >= 0 ? "&" : "?") + "Authorization=" + auth;
                if(!!body) _.set(options, "body", body);

                return fetch(url, options);
                    
            });
            

        })
        .then(function(response) {
            //if(!!response.ok) return response.json();
            if(!!response.ok) return json(response);

            var e = _.assign(new Error(response.status+""), {
                name:response.status+"",
                status:response.status,
                response: response,
            });

            //_.extend(e, response);
            //_.set(e, "response", response);

            // return response.json()
            // .catch(function(e) {
            //     return null;
            // })
            return json(response)
            .then(function(json) {
                //console.log("error json", e, json);
                if(!!json) _.set(e, "data", json);
                _.set(e, "message", _.get(json, "message") || e.message); // update message
                //console.log("error data", _.get(e, "data"));
                return Promise.reject(e);
            });
        })
        // .then(function(response) {
        //     if(!!response.ok) return response.json();

        //     var e = new Error(response.status);
        //     _.extend(e, response);
        //     _.set(e, "response", response);

        //     return response.json()
        //     .catch(function(e) {
        //         return null;
        //     })
        //     .then(function(json) {
        //         if(!!json) e.data = json;
        //         return Promise.reject(e);
        //     });
        // })
        //.then(api.response.entities)
        .catch(function(error) { // this is to catch a bug in Edge fetch
            //console.log("catching");
            //console.log("typemismatcherror", error);

            if(_.get(error, "name", error) === "TypeMismatchError") return Promise.reject(_.assign(new Error("401"), {
                name:"401",
                status:401,
            }));
            return Promise.reject(error);
            
        });
        // .catch(function(error) {
        //     if(!!error && error.status == 401) {
        //         document.documentElement.dispatchEvent(new CustomEvent("unauthorized", { bubbles: true }));
        //         return Promise.resolve({ status: error.status });
        //     }
        //     return Promise.reject(error);
        // });

    };

}(ParkIQ.API));