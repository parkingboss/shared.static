// DOM Events
(function(root) {

    //console.log("init events");

    //console.log(root);
    //var delegate = new Delegate(root);

    //console.log(root.root);

    //var root = new Delegate(document.documentElement);

    /*document.documentElement.addEventListener("change", function(e) {
       console.log("global",e);
    });

    delegate.on("change", function(e) {
       console.log("global",e);
    });*/

    root.addEventListener("change", function(e) {
        //console.log("change", e);
        if(!_.invoke(e, "target.matches", "select")) return;

        var select = e.target;
        select.blur();
        //var options = this.getElementsByTagName("option");
        var txt = select.parentNode.querySelector("select+span");
        if(!txt) select.after(txt = document.createElement("span"));
        //console.log("text=" + select.options[select.selectedIndex || 0].innerText || select.options[select.selectedIndex || 0].text);
        var option = select.options[select.selectedIndex || 0];
        if(!!option) txt.textContent = option.title || option.textContent || option.text;

    });
    
    // delegate.on("change", "select", function(e) {
        
    //     //console.log(e);
        
    //     var select = this;
    //     select.blur();
    //     //var options = this.getElementsByTagName("option");
    //     var txt = this.parentNode.querySelector("select+span");
    //     if(!txt) select.after(txt = document.createElement("span"));
    //     //console.log("text=" + select.options[select.selectedIndex || 0].innerText || select.options[select.selectedIndex || 0].text);
    //     var option = select.options[select.selectedIndex || 0];
    //     if(!!option) txt.textContent = option.title || option.textContent || option.text;

    //     //console.log(option);
        
    // });

    root.addEventListener("change", function(e) {

        if(!_.invoke(e, "target.matches", "input[type='date']")) return;

        //delegate.on("change", "input[type='date']", function (e) {
    	//console.log("input date change");

        //console.log("date change", this, e.target);

        //console.log("input date change", this.value);

        var target = e.target;

    	var date = moment(target.value);

        //var format = date.isSame(moment(), 'year') ? "ddd, MMM D" : "ddd, MMM D, YYYY";
        //console.log("moment", date);

    	var txt = date.isValid() ? date.format(target.getAttribute("data-format") || "ddd, MMM D, YYYY") : "";
    	var span = target.parentNode.querySelector("input[type='date'] + span");
        if (!span) {
			span = document.createElement("span");
			target.after(span);
		}
        //console.log(txt);
        span.textContent = txt; // only change if something new
    });
    
    var onchange = function(e) {
        //console.log("onchange", e);
        //console.log(this);
        var target = e.target;
        var label = target.closest("label");
        if(!!label) label.classList[!!target.value ? "add" : "remove"]("value");
	};
    
    _.each([ "input", "textinput", "change", "keypress", "keyup", "keydown"], function(ev) {
        //delegate.on(ev, "input,textarea,select", onchange);
        root.addEventListener(ev, function(e) {

            if(!_.invoke(e, "target.matches", "input,textarea,select")) return;

            onchange(e);

        });
    });
    
    root.addEventListener("focus", function(e) {

        if(!_.invoke(e, "target.matches", "input,textarea,select")) return;
        //delegate.on("focus", "input,textarea,select", function(e) {

        var target = e.target;

        var label = target.closest("label");
        if(!!label) label.classList.add("focused");
        var f = target.closest("fieldset");
        if(!!f) target.closest("fieldset").classList.add("focused");
    });
    root.addEventListener("focusin", function(e) {

        if(!_.invoke(e, "target.matches", "input,textarea,select")) return;
        //delegate.on("focusin", "input,textarea,select", function(e) {

        var target = e.target;
        var label = target.closest("label");
        if(!!label) label.classList.add("focused");
        var f = target.closest("fieldset");
        if(!!f) target.closest("fieldset").classList.add("focused");
    });

    
    root.addEventListener("blur", function(e) {

        if(!_.invoke(e, "target.matches", "input,textarea,select")) return;
        //delegate.on("blur", "input,textarea,select", function(e) {

        var target = e.target;

        var label = target.closest("label");
        if(!!label) label.classList.remove("focused");
        
        var f = target.closest("fieldset");
        if(!!f) target.closest("fieldset").classList.remove("focused");
    });

    root.addEventListener("focusout", function(e) {

        if(!_.invoke(e, "target.matches", "input,textarea,select")) return;
        //delegate.on("focusout", "input,textarea,select", function(e) {

        var target = e.target;

        var label = target.closest("label");
        if(!!label) label.classList.remove("focused");
        
        var f = target.closest("fieldset");
        if(!!f) target.closest("fieldset").classList.remove("focused");
    });
    
    root.addEventListener("reset", function(e) {

        if(!_.invoke(e, "target.matches", "form")) return;
        //delegate.on("reset", "form", function(e) {
        //if(window.console) console.log("doc-bound reset");

        var target = e.target;

        // force the reset - circular?
        //this.reset();

        //console.log("form.reset");
        
        _.each(target.querySelectorAll("input,textarea,select,button"), function(input) {
            input.dispatchEvent(new CustomEvent("change", { "bubbles": true }));
            window.setTimeout(function () { input.dispatchEvent(new CustomEvent("change", { "bubbles": true })); }, 10);
        });
        
    });
    
    root.addEventListener("keydown", function(e) {

        // if(!_.invoke(e, "target.matches", "input,textarea,select")) return;
        //delegate.on("keydown", function(e) {
        if(e.ctrlKey || e.metaKey) switch (String.fromCharCode(e.which).toLowerCase()) {
            case 'p':
                e.preventDefault();
                
                (document.body || document.getElementsByTagName("body")[0]).dispatchEvent(new CustomEvent("print", { "bubbles": true }));
                
                document.documentElement.dispatchEvent(new CustomEvent("track", {
                    bubbles: true,
                    detail: {
                        category: "keyboard", 
                        action: "print", 
                        label: window.location.href,
                    },
                }));
                
                return false;
                break;
        }
    });
    
}(document.documentElement));