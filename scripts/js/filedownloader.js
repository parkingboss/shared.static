var FileDownloader = FileDownloader || {
    download:function(url) {
        
        if(!url) return Promise.reject(new Error("No URL"));
        
        var container = document.createElement("div");
        container.innerHTML = '<iframe style="width:0;height:0;position:absolute;visibility:hidden;border:0;display:none;" src="about:blank"></iframe>';
        var iframe = container.childNodes[0];
        
        return new Promise(function(resolve, reject) {
            
            iframe.addEventListener("load", function(e) {
                
                if(!this.src || "about:blank" == this.src) return null;

                return resolve(url);
                    
            });
            
            
            document.body.appendChild(iframe);
            iframe.setAttribute("src", url);
            
        });
	},
};