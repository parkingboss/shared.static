moment.relativeTimeThreshold('m', 60);
moment.relativeTimeThreshold('h', 25);
moment.updateLocale('en', {
    relativeTime : {
        future: "in %s",
        past:   "%s ago",
        s:  "seconds",
        m:  "%d min",
        mm: "%d min",
        h:  "an hour",
        hh: "%d hours",
        d:  "a day",
        dd: "%d days",
        M:  "a month",
        MM: "%d months",
        y:  "a year",
        yy: "%d years"
    }
});
moment.updateLocale('en', {
    calendar : {
        lastDay : '[yesterday] h:mm A',
        sameDay : '[today] h:mm A',
        nextDay : '[tomorrow] h:mm A',
        lastWeek : 'ddd h:mm A',
        nextWeek : 'ddd h:mm A',
        sameElse : 'MMM D YYYY h:mm A'
    },
});