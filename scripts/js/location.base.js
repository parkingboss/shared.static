(function(postal, page) {

    var base;
    var head;

    function basify(ctx, next) {
        var id = ctx.params.location; 

        if(!!id) {
            base = base || document.querySelector("base");
            if(!base) {
                base = document.createElement("base");
                base.setAttribute("href", "/" + id + "/");
                head = head || document.querySelector("head");
                head.prepend(base);
            }
        }

        next();
    }

    page("*", basify);
    page("/:location/*", basify);

}(window.postal, page));