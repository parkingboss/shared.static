(function(auth, page, qs, jwtdecode) {

    function authFromUrl(path, query, hash) {


        if(typeof query === "string") query = qs.parse(query);

        if(typeof hash === "string") hash = qs.parse(hash);

        var strip = [];

        var source = _.defaults({}, hash, query);

        var token = _.get(source, "access_token");
        if(!!token) strip.push("access_token");

        if(!token) {
            token = _.get(source, "token");
            if(!!token) strip.push("token");
        }

        if(!token) return null;

        var extra = [ "lifetime", "expires", "scope", "user" ];
        Array.prototype.push.apply(strip, extra);

        var data = _.assign({
            token: token,
            type: "bearer",
        }, _.pick(extra));

        if(!!data) {

            _.each(strip, function(key) {
                _.unset(query, key);
                _.unset(hash, key);
            });
            var newQuery = qs.stringify(query);
            var newHash = qs.stringify(hash);
            window.history.replaceState(null, null, path + (!!newQuery ? "?" : "") + newQuery + (!!newHash ? "#" : "") + newHash);

        }

        if(!!data && !!jwtdecode) {

            var jwt = null;
            try {
                jwt = jwtdecode(data.token);
            } catch(e) {}

            if(!!jwt) {

                if(!!jwt.exp) {
                    _.set(data, "expires", new Date(jwt.exp * 1000).toISOString());
                    _.unset(jwt, "exp");
                }

                if(!!jwt.sub) {
                    _.set(data, "user", jwt.sub);
                    _.unset(jwt, "exp");
                }

                // copy the rest
                _.assign(data, jwt);
            }

        }

        return data;

    }

    var data = authFromUrl(location.pathname, (location.search || "?").substr(1), (location.hash || "#").substr(1));
    if(!!data) auth.set(data, data.user);

    // this will have rewritten the url already, never picked up by the page module

    function authFromContext(ctx, next) {

        var auth = authFromUrl(location.pathname, (location.search || "?").substr(1), (location.hash || "#").substr(1));

        if(!!auth) {
            auth.user = _.get(ctx, "params.user") || auth.user;
            api.Auth.set(auth, auth.user);
            console.log("auth", auth);
            ctx.auth = auth;
        }

        next();
    };



}(ParkIQ.API.Auth, page, window.qs || window.Qs, window.jwt_decode));