(function(api, postal) {
	
	var store = api.Spaces = {
        predefined: true,
	};
	
	// allocate a search engine
	var _lookup = store.Lookup = api.IDKeyValueSearcher(false, true);
	
	_.extend(store, {
        isValid:_lookup.isValid,
		lookup:function(query, omitNoMatchItem, allIfEmpty) {
            
            if(omitNoMatchItem !== true && omitNoMatchItem !== false) omitNoMatchItem = store.predefined;
            if(allIfEmpty !== true && allIfEmpty !== false) allIfEmpty = store.predefined;
			
			query = query || "";
            if(!!query) query = query.toUpperCase();
			
			
			// we need engine
			return _lookup.search(query, allIfEmpty).then(function(result) {
				
				//console.log(results);
                
                var normalized = result.query;
                var results = result.results;
						
				if(!omitNoMatchItem && (query != null && query.length >= 1) && (results.length == 0 || !_.find(results, ['value', query]))) results.push({
					id:normalized,
					key:normalized,
					value:query,
				}); // prefill query if no match so far
				
				//var now = Date.now();
				
				var items = _.reduce(results, function(items, item) { // make sure plates unique
					
					if(item.value == query) items.unshift({
						id:item.id,
						key:item.key,
						display:item.value,
					});
					else items.push({
						id:item.id,
						key:item.key,
						display:item.value,
					});
					
					return items;
					
				}, []);
				
				
				
				return {
					query: query,
                    normalized:normalized,
					items: items,
                    updated: result.updated,
				};
						

			});
			
		},
		add:store.Lookup.add,
	});
	
    var last = null;
    
    function updater(spaces) {
        var ts = new Date(spaces.generated || spaces.ts);
        
        if(!!last && last.getTime() >= ts.getTime()) return; // this isn't newer
        
        if(!!spaces.predefined) store.predefined = true;

        //console.log("spaces updater", spaces);
        
        store.add(_.reduce(spaces.items, function(list, item, key) {
            
            list.push({
                id:_.isString(item) && _.isString(key) && !item.key ? key : _.get(item, "id", item),
                key:_.isString(item) && _.isString(key) && !item.key ? key : _.get(item, "key", item),
                value:_.isString(item) ? item : _.get(item, "display", item),
                //display:item.display,
            });

            return list;

        }, []), spaces.generated);
    };

    _.each([ "spaces.items" ], function(topic) {
        postal.subscribe({
            topic:topic,
            callback:updater,
        });
    });
	
	// postal.subscribe({
	// 	topic:"spaces.items",
	// 	callback:function(data, envelope) {
			
	// 		var ts = new Date(data.generated || data.ts);
			
	// 		if(!!last && last.getTime() >= ts.getTime()) return; // this isn't newer
            
    //         if(!!data.predefined) store.predefined = true;
			
	// 		api.Spaces.add(_.reduce(data.items, function(list, item, key) {
				
	// 			list.push({
	// 				id:item.id,
	// 				key:item.key,
	// 				value:item.display,
	// 				//display:item.display,
	// 			});
				
	
	// 			return list;
	
	// 		}, []), data.generated);
	// 	},
	// });
})(ParkIQ.API, postal);

(function(api, postal, page) {
    
    _.extend(api.Spaces, {
        all:Promise.method(function(location) {

            var requested = new Date().toISOString();
            var url = "v1/locations/" + location + "/spaces";
            return Promise.resolve(api.base())
            .then(function(base) {
                return api.fetch("GET", base + url, null, _.invoke(api, "Auth.header", location));
            })
            .then(function(json) {

                json.requested = requested;
                
                //var items = _.get(json, "spaces.items");

                var items = _.map(_.get(json, "vehicles.items"), function(val, key) {
                    return {
                        id: _.get(val, "id") || (_.isString(key) ? key : null),
                        key: _.get(val, "key") || (_.isString(key) ? key : null) || (_.isString(val) ? val : null),
                        display: _.get(val, "display") || (_.isString(val) ? val : null),
                    };
                    
                });
                
                var predefined = _.get(json, "spaces.predefined");

                _.each([
                    //"spaces.items.updated",
                    "spaces.items",
                ], function(topic) {
                    postal.publish({
                        topic   : topic,
                        data    : {
                           generated: json.generated,
                           items: items,
                           predefined: predefined,
                        }
                    });
                });

                return {
                    items:items,
                    generated:json.generated,
                    predefined: predefined,
                };
 

            })
            //.catch(api.response.error)
            .catch(function(error) {
                // my own error handling
                console.log(".catch logging spaces error", error);
            });
         }),
    });
    
    var location;
    
    function load(location) {
        return api.Spaces.all(location);
    };
    
    // only run this once
    var init = _.once(load);

    if(!!page) {

        function resolve(ctx, next) {
            location = ctx.params.location;
            if(!!location) init(location);
            next();
        }

        page("*", resolve);
        page("/:location/*", resolve);

    }
    if(!!postal) {
        function updater(data) {
            //if(!!data && !!data.item) location = data.item.id;
            location = _.get(data, "item.id") || _.get(data, "id");
            if(!!location) init(location);
        };
        _.each([ "location.item" ], function(topic) {
            postal.subscribe({
                topic    : topic,
                callback : updater,
            });
        });
        
    }
    
    // update every 10 min
    window.setInterval(function() {
        if(!location) return;
        load(location);
    }, 10 * 60 * 1000);
    
}(ParkIQ.API, window.postal));

(function(api, postal) {
	
    var pickDate = function(item) {
					
        // we only care about past expired dates
        if(!!item.valid && !!item.valid.to && Date.parse(item.valid.to.utc || item.valid.to) < Date.now()) return item.valid.to.utc || item.valid.to;
        
        // don't care about start date
        
        return item.issued.utc || item.issued;
    };
    
    _.extend(api.Spaces, {
        get:Promise.method(function(location, id, subset) {

            var pastDays = subset ? 30 : 365;
            var valid = dateFns.format(dateFns.subHours(new Date(), (pastDays * 24) - 6)) + "/" + dateFns.format(dateFns.addHours(new Date(), 6));
            var contact = !!api.contact;
            var sent = !subset && !!api.sent;
            
            var requested = new Date().toISOString();

            return Promise.join(api.base(), location, id, function(base, location, space) {
                var url = base + "v1/locations/" + location + "/spaces/" + space + "?valid=" + valid + "&contact=" + contact + "&sent=" + sent;
                return api.fetch("GET", url, null, api.Auth.header(location));
			}).then(function(json) {

                json.requested = requested;
				
				var item = _.get(json, "spaces.item");
				if(!!item) item = _.get(json, [ "spaces", "items", item ], item);

                if(!item) return json;
                
                //var item = _.get(json, ["spaces", "items", _.get(json, "spaces.item")]);
                
                var tenantType = _.get(json, "tenants.type");
                
                var location = _.get(json, "locations.items." + _.get(json, "locations.item"), json.location);
                var permits = _.get(json, "permits.items");
                var violations = _.get(json, "violations.items");
                var media = _.get(json, "media.items");
                var types = _.get(json, "media.types.items") || _.get(json, "media.types");
				var users = _.get(json, "users.items");
				var vehicles = _.get(json, "vehicles.items");
				var tenants = _.get(json, "tenants.items");
				var spaces = _.get(json, "spaces.items");
                var notes = json.notes;
                var files = _.get(json, "files.items");
                var permittables = _.get(json, "permittables.items");
                
                permits = _.map(permits, function(item) {
                    
                    item.__type = "permit";
					
					api.Items.vehicle(item, vehicles);
					api.Items.tenant(item, tenants);
                    api.Items.space(item, spaces);
                    api.Items.media(item, media);
					
					api.Items.user(item.issued, users);
                    api.Items.user(item.updated, users);
					api.Items.notes(item, notes);
                    api.Items.sent(item, json.sent);

                    item.contact = _.get(json, ["contacts", "items", item.id]);
                    
                    if(!!item.tenant) item.tenant.type = item.tenant.type || tenantType;
                    
                    if(!!item.media && item.media.type && !!types && _.isString(item.media.type)) item.media.type = types[item.media.type];

                    if(!!item.attendant) item.attendant = _.get(json, [ "attendants", "items", item.attendant ], item.attendant);
                    
                    return api.Permits.normalize(item);
                    
                });
                
                violations = _.map(violations, function(item) {
                    item.__type = "violation";
			
                    api.Items.vehicle(item, vehicles);
					api.Items.tenant(item, tenants);
					api.Items.space(item, spaces);

                    api.Items.user(item.issued, users);
                    api.Items.notes(item, notes);
                    api.Items.files(item, files);
                    api.Items.sent(item, json.sent);
                    
                    if(!!item.tenant) item.tenant.type = item.tenant.type || tenantType;
                    
                    return api.Items.normalize(item);
                });
                

                var latestFor = {};
                permittables = _.map(permittables, function(item) {
                    var latest = latestFor[item.container];
                    if(!latest || Date.parse(item.issued.utc) > Date.parse(latest.issued.utc)) latestFor[item.container] = item;
                    return item;
                });
		
                // map again
                permittables = _.map(permittables, function(item) {
                    item.__type = "permittable";
                    
                    api.Items.user(item.issued, users);
                    api.Items.notes(item, notes);
                    
                    var latest = latestFor[item.container];
                    item.latest = !!latest && latest.id == item.id;
                    
                    if(!!item.scope) item.attendant = _.get(json, [ "attendants", "items", item.scope ]);
                    
                    api.Items.space(item, spaces, "container");

                    return api.Items.normalize(item);
                });
                
                latestFor = null;
                
                var itemNotes = _.map(api.Items.notes(item, notes).notes, function(item) {
                    item.__type = "note";
                    api.Items.user(item.issued, users);
                    return api.Items.normalize(item);
                });
                
                if(!!location) postal.publish({
				    topic   : "location.item",
				    data    : {
				       requested:json.requested,
                        generated:json.generated,
					   item: location,
				    }
				});
                
                postal.publish({
				    topic   : "tenants.items",
				    data    : {
				       requested:json.requested,
                        generated:json.generated,
					   items: _.map(tenants),
                       predefined: _.get(json, "tenants.predefined"),
				    }
				});
                
                postal.publish({
				    topic   : "vehicles.items",
				    data    : {
				       requested:json.requested,
                        generated:json.generated,
					   items: _.map(vehicles),
				    }
				});
                
                postal.publish({
				    topic   : "spaces.items",
				    data    : {
				       requested:json.requested,
                        generated:json.generated,
					   items: _.map(spaces),
                       predefined: _.get(json, "spaces.predefined"),
				    }
				});
                
                var validPermits = _.filter(permits, api.Permits.isValidOrGrace);
                
                return {
                    type:"space",
                    item:item,

                    requested:json.requested,
                    generated:json.generated,

                    activity: {
                        partial:false,
                        items: [].concat(
                            _.chain(permits).filter(api.Permits.isValidOrGrace).orderBy([ "issued.utc" ],[ "desc" ]).value()
                        ),
                    },
                    history: {
                        partial:!!subset,
                        items: [].concat(
                            //_.filter(permits, api.Permits.isExpired),
                            //_.filter(permittables, "issued.user"),
                            //permitsTimeline,
                            _.filter(violations, "issued.user"),
                            itemNotes).sort(function (a, b) {
                        
                            return(function (x, y) {
                                return x < y ? 1 : x > y ? -1 : 0;
                            }(pickDate(a), pickDate(b)));
                            //return Sorter.desc(pickDate(a), pickDate(b))
            
                        }),
                    },

                    permits: _.assign(json.permits, {
                        items:[].concat(_.orderBy(permits, [ "issued.utc" ], [ "desc" ])),
                        valid: {
                            count:_.size(validPermits),
                            items:validPermits,
                        },
                    }),

                    violations: _.assign(json.violations, {
                        items: _.orderBy(violations, [ "issued.utc" ], [ "desc" ]),
                    }),

                };

            })
            .catch(function(error) {
                // my own error handling
                console.log(".then logging error", error);
            });
         }),
    });
        
}(ParkIQ.API, postal));