(function(page, qs) {
    
    var prev;

    page("*", function(ctx, next) {

		if(!!qs) ctx.query = qs.parse(ctx.querystring);

		ctx.referer = prev;
        prev = ctx.path;

		next();
	
	});
    
}(window.page, window.qs || window.Qs));