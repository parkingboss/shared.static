(function(exports, storage, jwt_decode) {

    var api = exports;
    storage = storage || api.Storage;

    var auth = {};

    // this could use some additional jwt mechanics

    function keys(scope) {
        if(!scope) return [ "user/auth" ];
        return [
            "user/auth/" + scope,
            "user/auth",
        ];
    }

    function get(scope, checkExpired) {

        var data = _.find(_.map(keys(scope), function(key) {
            
            var data = storage.get(key);
            if(!data || !data.token || !jwt_decode || !checkExpired) return data;

            try {
                jwt = jwt_decode(data.token);
                if(!jwt || !jwt.exp) return data;

                if(jwt.exp * 1000 < Date.now()) return null; // expiration is past
                
            } catch(error) {
                return data;
            }

            return data;

        }));

        return data;

    };

   function promise(scope, checkExpired) {
        return Promise.resolve(get(scope, checkExpired))
        .then(function(data) {
            if(!data || !data.token || !data.type) return Promise.reject(_.extend(new Error("No auth data"), { status: 401 }));
            return data;
            
        });
    };

    function header(scope, checkExpired) {
        return promise(scope, false !== checkExpired)
        .then(function(data) {
            return data.type + " " + data.token;
        });
    };

    function set(data, scope, domain, expires) {
        _.each(keys(scope), function(key) {
            storage.set(key, data, domain, expires);
        });
    };

    function remove(scope, domain) {
        _.each(keys(scope), function(key) {
            storage.remove(key, domain);
        });
    };

    _.extend(auth, {
        get: get,
        set: set,
        remove: remove,
        promise: promise,
        header: header,
    });

    exports.Auth = auth;

}(ParkIQ.API, ParkIQ.API.Storage, window.jwt_decode));