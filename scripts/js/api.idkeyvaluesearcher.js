var ParkIQ = window.ParkIQ = ParkIQ || {};

ParkIQ.API = ParkIQ.API || {};

(function(exports) {
	
	var normalize = function(val, relaxed) {
        //console.log("val", val);
        //console.trace();

        val = val || "";
        if(!val) return val;
		//if(!!relaxed) return val.toUpperCase().replace(/[^A-Z0-9\s]/gi, "");
		return val.toUpperCase().replace(/[^A-Z0-9]/gi, "").replace("O", "0");
        //return val;
	};
	
	exports.IDKeyValueSearcher = function(relaxed, numericSort) {

        
        
        var sorter = function(a,b) {
						
            var x = a.value;
            var y = b.value;

            // first si
            
            if(!numericSort) return x < y ? -1 : x > y ? 1 : 0; // string sort
            
            if(x.length == y.length) return x < y ? -1 : x > y ? 1 : 0; // numeric type
            else if (x.length < y.length) return -1;
            else return 1;
        };

        var sort = function(arr) {

                //console.log(arr);
                //if(!numericSort) return _.sortBy(arr, [ "index", "value" ]);
                //if(!numericSort) return _.orderBy(arr, [ "score", "index", "value" ], [ "desc", "asc", "asc"]);
                if(!numericSort) return _.orderBy(arr, [ "score", "value" ], [ "desc", "asc"]);
    
                return arr.sort(sorter);
    
            };

        var _lunr = function(data) {
            return Promise.resolve(lunr(function() {
                this.field("key");
                this.field("value");
                this.ref("id");

                //console.log("lunr data", data);

                data.forEach(function(item) {
                    this.add(item);
                }, this);

                this.pipeline.remove(lunr.stopWordFilter); // no stop word filter
                this.pipeline.remove(lunr.stemmer); // no stemmer
            }))
        }
		
		var searcher = {
            // _lunr:Promise.resolve(lunr(function () {
            //     this.field("value");
            //     this.ref("id");

            //     this.pipeline.remove(lunr.Pipeline.registeredFunctions['stopWordFilter']); // no stop word filter
            // })),
            //_lunr:_lunr([]),
		};
        
        var all = {
            cache: {},
            result: Promise.resolve({
                query:"",
                results:[],
            }),
        };
        
        searcher.isValid = function(idKeyValue) {
            
            if(!idKeyValue) return true;
            if(!all.cache) return true;
            if(_.isEmpty(all.cache)) return true;
            
            //console.log("idKeyValue", idKeyValue);
            //console.log(idKeyValue.toUpperCase().replace(/\W+/g, ""));
            
            return !!_.find(all.cache, function(item) {
                //console.debug(item);
                return item.id === idKeyValue || (item.key || "") === normalize(idKeyValue, relaxed);
            });

            //return true;
        };
		
		searcher.add = function(itemOrCollection) {

            //console.log(itemOrCollection);
            //console.trace();
            
			var data = !!itemOrCollection.id && !!itemOrCollection.key ? [ itemOrCollection ] : _.map(itemOrCollection, function(item, key) {
                if(!item) return;

                // this isn't an indexable item
                if(!item.id && !item.key && !item.value && !!key) return {
					id:key + "",
                    key:key + "",
					value:item + "",	
				}; 

                if(!!item.id && !!item.key && !!item.value) return item;

                /*
                if(!!item.id && !item.key) return null; // id with no key isn't searchable
                if((!!item.id || !!item.key) && !item.value) return null; // 
                if(!!item.id && !!item.key) return item;

                //handle ? { id: "TF7rwdfYXkWuhJn27O5w6g", key: "", value: "" }
                // {id: "", key: "", value: "*"}

                if(!key) return null;
				
				// we be prepared to handle string-string maps
				return {
					id:key,
                    key:key,
					value:item,	
				};
                */

                return null;
			});
            
            //console.debug(data);
            
            _.each(data, function(item) {
                if(!item) return;
                if(!!item.key) item.key = normalize(item.key, relaxed);
                all.cache[item.id] = item;
            });
            
            //console.log(all.map);
            
            // resort
            all.result = Promise.resolve({
                query:"",
                results:_.map(all.cache).sort(sorter),
            });

            // rebuild our lunr
            //searcher._lunr = _lunr(_.values(all.cache));
            
            // searcher._lunr.then(function(index) {
            //     //console.log(data);
            //     _.each(data, function(item) {
            //         //console.log(item);
            //         if(!item) return;

            //         var newItem = _.clone(item);
            //         newItem.value = normalize(newItem.value, relaxed); // normalize value before adding
            //         index.add(newItem);
            //         //console.log(newItem);
            //     });
            // });
			
		};
		
		searcher.search = function(query, allIfEmpty) {
			
			query = normalize(query, relaxed);
			//console.log("normalized query = " + query);			
			// we need engine
            //console.log(all);

            //console.log(query);

            //allIfEmpty = false; // no more support...
            if(!query && !!allIfEmpty) return all.result;
            
            return all.result
            .then(function(all) {

                //console.log(all);

                if(query.length <= 0) return []; // no results

                //var results = [];

                return _.reduce(all.results, function(results, item) {

                    var score = 0 - item.key.toUpperCase().indexOf(query);

                    if(score < 1) results.push(_.extend({
                        score: score,
                    }, item)); // contains

                    return results;
                    
                }, []);

            })
        
            //return searcher._lunr
            // .then(function(index) {
            //     //console.log(index);
            //     if(query.length <= 0) return []; // no results

            //     // return index.query(function(q) {
            //     //     // build the query, skip text parsing

            //     //     //exact matches should have the highest boost
            //     //     q.term(query, { boost: 100, usePipeline: true });
                    
            //     //     // prefix matches should be boosted slightly
            //     //     q.term(query, { boost: 10, usePipeline: true, wildcard: lunr.Query.wildcard.TRAILING });

            //     //     // prefix matches should be boosted slightly
            //     //     q.term(query, { boost: 2, usePipeline: true, wildcard: lunr.Query.wildcard.LEADING | lunr.Query.wildcard.TRAILING });
                    
            //     //     // finally, try a fuzzy search, without any boost
            //     //     if(query.length > 2) q.term(query, { boost: 1, usePipeline: true, editDistance: 1 });
                    
            //     //     //q.term(query, { wildcard: lunr.Query.wildcard.LEADING | lunr.Query.wildcard.TRAILING });
            //     //     console.log(q);
            //     // });

            //     return index.search("*" + query + "*"); // mid-word matches
            //     // console.log("r", r);
            //     // return r;
            // })
            // .then(function(results) {
            //     //console.log("results", results);
            //     // map the source onto results, sort by value
            //     // these are pre-sorted by score desc
            //     return _.map(results, function(item) {
            //         //console.log(item);
            //         return _.extend({
            //             //index:item.
            //             // custom scorer based on index
            //             score:1 - _.min(_.map(item.matchData.metadata, function(data, term) {
            //                 return term.toUpperCase().indexOf(query);
            //             })),
            //             //score:item.score,
            //         }, all.cache[item.ref]);
            //     });
                
            // })
            .then(function(results) {
                //console.debug(results);
                return {
                    query:query,
                    results:sort(results),
                };
            });
			
		};
		
		return searcher;
		
	};
		
})(ParkIQ.API);