// DOM Events
(function() {

    // send
    document.addEventListener("track", function(e) {
       var data = e.detail;
       if(!data) return;
       
       // analytics
        if (!!window._gaq) _gaq.push(['_trackEvent', data.category, data.action, data.label]);
        
        // need to add support for universal analytics
        if(!!window.ga) ga('send', 'event', data.category, data.action, data.label);  // value is a number.
    });
    
}());

(function(page, ga, appInsights) {

    if(!page) return;
    

    page("*", function(ctx, next) {
	
		//console.log("*");
	
        // run analytics
        
        _.invoke(ga, "push", ['_trackPageview', ctx.canonicalPath]);
        _.invoke(appInsights, "trackPageView", ctx.canonicalPath);
		
        //if (!!window._gaq) _gaq.push(['_trackPageview', ctx.canonicalPath]);
        // if (!!window.ga) {
        //     ga('set', 'page', ctx.canonicalPath);
        //     ga('send', 'pageview');
        // }
		

        //if(!!window.appInsights && appInsights.trackPageView) appInsights.trackPageView(ctx.canonicalPath);
        // if(!!window.rg4js) rg4js("trackEvent", {
        //     type: "pageView",
        //     path: ctx.canonicalPath,
        // });
        // else if(!!window.Raygun && !!Raygun.trackEvent) Raygun.trackEvent('pageView', { path: ctx.canonicalPath });

	
		next();
	
	});

    
}(window.page, window._gaq, window.appInsights));