// DOM Events
(function(root) {

    var cal = {
        sameDay: 'ddd MMM D h:mm A [(]Z[)]',
        nextDay: 'ddd MMM D h:mm A [(]Z[)]',
        nextWeek: 'ddd MMM D h:mm A [(]Z[)]',
        lastDay: 'ddd MMM D h:mm A [(]Z[)]',
        lastWeek: 'ddd MMM D h:mm A [(]Z[)]',
        sameElse: 'MMM D YYYY h:mm A [(]Z[)]'
    };
    
    function expired(time) {
        
        if(!time) return;
			
        var datetime = time.getAttribute("datetime");
            
		var invalidate = time.getAttribute("data-valid");

        var ms = 0;
        if(!!invalidate) ms = moment.duration(invalidate).asMilliseconds();
			//console.log(ms);

        if(!datetime || ms <= 0) {
            time.classList.remove("invalid");
            return;
        }
			
        // if now ms is after datetime ms + period ms
        time.classList[Date.now() > new Date(datetime).getTime() + ms ? "add" : "remove"]("invalid");
			

	}
    
    window.setInterval(function() {
		// check for recency
        
        Array.prototype.slice.call(document.querySelectorAll("time[data-valid]")).forEach(expired);
		
	}, 1 * 60 * 1000);
    
    _.each([ "change", "update"], function(evt) {
        root.addEventListener(evt, function(e) {
            if(!_.invoke(e, "target.matches", "time[data-valid]")) return;

            var time = e.target;

            var datetime = time.getAttribute("datetime");
            //console.log(datetime);
            time.innerHTML = !!datetime ? moment(datetime).calendar(null, cal) : "";
                
            expired(time);

        });

    });
    
}(document.documentElement));