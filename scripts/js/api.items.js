(function(api) {
	
	// is the date "today" in local system time
	function today(datetime) {
		var now = new Date();
		//console.log(now);
		var test = new Date(datetime); // eat anything date-y
		//console.log(test);
		return now.getFullYear() == test.getFullYear() && now.getMonth() == test.getMonth() && now.getDate() == test.getDate();
	}
	
	function fromNow(datetime, nosuffix) {
		return moment(datetime).fromNow(nosuffix);
	}
	function calendar(datetime) {
		return moment(datetime).calendar();
	}
	function format(datetime, format) {
		return moment(datetime).format(format);
	}
	
	function setFromCollection(item, prop, collection, srcprop) {
		if(!item) return item;
		var val = _.get(item, srcprop || prop);
		if(!val) return item;
		if(!val.id && collection) {
			// find new val by map to self or find an item with a matching "key" prop
			var newVal = _.get(collection, val);
			if(!!newVal) _.set(item, prop, newVal);
		}
		return item;
	}
	_.extend(api.Items = api.Items || {}, {
        get:function(data, name) {
			var item = _.get(data, [name, "item"]);
			return _.get(data, [ name, "items", item], item);
		},
		vehicle:function(item, collection, srckey) {
			return setFromCollection(item, "vehicle", collection, srckey);
		},
		tenant:function(item, collection, srckey) {
			return setFromCollection(item, "tenant", collection, srckey);
		},
		space:function(item, collection, srckey) {
			return setFromCollection(item, "space", collection, srckey);
		},
		media:function(item, collection, srckey) {
			return setFromCollection(item, "media", collection, srckey);
		},
		/*notes:function(item, collection) {
			if(!!collection && !item.notes) item.notes = _.map(collection[item.id]);
			return item;
		},*/
		notes:function(item, collection) {
			if(!!collection && !item.notes) {
				var target = (collection.containers || collection)[item.id];
				if(!!target) item.notes = _.map(target.items || target);
			}
			return item;
		},
        files:function(item, collection) {
			if(!!collection && !item.files) {
				var target = (collection.containers || collection)[item.id];
				if(!!target) item.files = _.map(target.items || target);
			}
			return item;
		},
		sent:function(item, collection) {
			if(!!collection && !item.sent) {
				var target = (collection.containers || collection)[item.id];
				if(!!target) item.sent = _.map(target.items || target);
			}
			return item;
		},
		user:function(item, collection) {
			return setFromCollection(item, "user", collection);
		},
		expandDates:function(item, props) {
			if(!item) return item;
			
			props = props || [ "created", "updated", "active", "issued", "valid.to", "valid.from", "grace.to", "grace.from"];
				
			_.each(props, function(prop) {
				var val = _.get(item, prop);
				if(!val) return;
				if(_.isString(val)) {
					val = { utc: val, };
					_.set(item, prop, val);
				}
				// set properties
				if(!!val.utc) {
					val.today = today.bind(val.utc, val.utc);
					val.relative = fromNow.bind(val.utc, val.utc);
					//val.display = moment(val.utc).calendar();
					val.calendar = calendar.bind(val.utc, val.utc);
					val.format = format.bind(val.utc, val.utc);
				}
			});
			
			return item;
		},
		normalize:function(item) {
			if(!item) return item;
			api.Items.expandDates(item);
			//console.log(item);
			_.each([ "issued.user", "updated.user"], function(prop) {
				var user = _.get(item, prop);
				//console.log(user);
				if(!user) return;
				if(!user.name && !user.email) return;
				user.name = user.display = user.name || user.email.split("@")[0];
			});
			return item;
		},
		duration:function(item, name) {
			
			var val = !!name ? (_.get(item, [ name, "iso" ]) || _.get(item, [ name, "duration" ]) || _.get(item, [ name, "time" ]) || _.get(item, name, item)) : item;

			if(!val) return null;
			if(!_.isString(val)) return null; // we only process strings...
			var dur = moment.duration(val);

			var newVal = {
				iso:dur.toJSON(),
				zero: dur.asMilliseconds() == 0,
				//value:val,
				display:(dur.asHours() <= 72 ? Math.ceil(dur.asHours()) + " hour" + (dur.asHours() == 1 ? "" : "s") : dur.format(dur.hours() == 0 ? dur.days() == 1 ? "d[ day]" : "d[ days]" : "d[d], h[h]")),
			};

			if(!!name && val != item) item[name] = newVal;

			//console.log(newVal);

			return newVal;
		
		},
	});
	
})(ParkIQ.API);