(function(exports) {

    var docCookies = {
        defaultPath: "/",
        getItem: function (sKey) {
            //if(navigator.cookieEnabled === false) return null;
            if (!sKey) { return null; }
            return decodeURIComponent(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*" + encodeURIComponent(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1")) || null;
        },
        setItem: function (sKey, sValue, vEnd, sPath, sDomain, bSecure) {
            //if(navigator.cookieEnabled === false) return false;
            if (!sKey || /^(?:expires|max\-age|path|domain|secure)$/i.test(sKey)) { return false; }
            var sExpires = "";
            var sPathOrDefault = sPath || docCookies.defaultPath;
            if (vEnd) {
                switch (vEnd.constructor) {
                    case Number:
                        sExpires = vEnd === Infinity ? "; expires=Fri, 31 Dec 9999 23:59:59 GMT" : "; max-age=" + vEnd;
                        break;
                    case String:
                        sExpires = "; expires=" + vEnd;
                        break;
                    case Date:
                        sExpires = "; expires=" + vEnd.toUTCString();
                        break;
                }
            }
            document.cookie = encodeURIComponent(sKey) + "=" + encodeURIComponent(sValue) + sExpires + (sDomain ? "; domain=" + sDomain : "") + (sPathOrDefault ? "; path=" + sPathOrDefault : "") + (bSecure ? "; secure" : "");
            return true;
        },
        removeItem: function (sKey, sPath, sDomain) {
            //if(navigator.cookieEnabled === false) return false;
            if (!this.hasItem(sKey)) { return false; }
            var sPathOrDefault = sPath || docCookies.defaultPath;
            document.cookie = encodeURIComponent(sKey) + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT" + (sDomain ? "; domain=" + sDomain : "") + (sPathOrDefault ? "; path=" + sPathOrDefault : "");
            return true;
        },
        hasItem: function (sKey) {
            //if(navigator.cookieEnabled === false) return false;
            if (!sKey) { return false; }
            return (new RegExp("(?:^|;\\s*)" + encodeURIComponent(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=")).test(document.cookie);
        },
        keys: function () {
            //if(navigator.cookieEnabled === false) return [];
            var aKeys = document.cookie.replace(/((?:^|\s*;)[^\=]+)(?=;|$)|^\s*|\s*(?:\=[^;]*)?(?:\1|$)/g, "").split(/\s*(?:\=[^;]*)?;\s*/);
            for (var nLen = aKeys.length, nIdx = 0; nIdx < nLen; nIdx++) { aKeys[nIdx] = decodeURIComponent(aKeys[nIdx]); }
            return aKeys;
        }
    };

    var __cache = {};
    var memoryStorage = {
        getItem:function(key) {
            if(!key) return null;
            return _.get(__cache, key);
        },
        setItem:function(key, value) {
            if(!key || !value) return null;
            _.set(__cache, key, value);
            return value;
        },
        removeItem:function(key) {
            if(!key) return null;
            return _.unset(__cache, key);
        }
    };

    function storageAvailable(type) {
        try {
            var storage = window[type],
                x = '__storage_test__';
            storage.setItem(x, x);
            storage.removeItem(x);
            return true;
        }
        catch(e) {
            return false;
        }
    }

    function supportsStorageType(type) {
        try {
            var test = "_";
            type.setItem(test, test);
            var supports = type.getItem(test) === test;
            type.removeItem(test);
            return supports;
        } catch (e) {
            return false;
        }
    }

    var supportsCookieStorage = navigator.cookieEnabled !== false && supportsStorageType(docCookies);
    var supportsLocalStorage =  storageAvailable("localStorage") && supportsStorageType(window.localStorage);
    var supportsSessionStorage = storageAvailable("sessionStorage") && supportsStorageType(sessionStorage);

    var session = {
        //__key:storageKey,
        __cached:{},
        __mechanisms:_.filter([ 
            supportsSessionStorage ? sessionStorage : null,
            supportsCookieStorage ? docCookies: null,
            memoryStorage,
        ]),
    };
    session.remove = function(key) {
        _.unset(session.__cached, key);
        _.each(session.__mechanisms, function(store) {
            if(!store) return;
            try {
                store.removeItem(key);
            } catch(e) {

            }
        });
    };
    session.set = function(key, value) {
        if(!value) return session.removeItem(key);

        _.set(session.__cached, key, value);

        // try to set the first avail storage
        return _.find(session.__mechanisms, function(store) {
            if(!store) return null;
            try {
                store.setItem(key, JSON.stringify(value));
                return value;
            } catch(e) {
                return null;
            }
        });
    };
    session.get = function(key) {
        return _.get(session.__cached, key) || _.reduce(session.__mechanisms, function(result, store) {
            if(!!result || !store) return result;
            //if(!store) return result;
            try {
                //console.log("store.getItem", store, key);
                var value = store.getItem(key);
                //console.log("store.getItem.value", value, key);
                if(!!value) return JSON.parse(value);
                return null;
            } catch(e) {
                return null;
            }
        }, null);
    };

    exports.Session = session;


    var storage = {
        //__key:storageKey,
        __cached:{},
        __mechanisms:_.filter([ 
            supportsLocalStorage ? window.localStorage : null,
            supportsCookieStorage ? docCookies: null,
            memoryStorage,
        ]),
    };
    storage.remove = function(key, domain) {
        _.unset(storage.__cached, key);
        _.each(storage.__mechanisms, function(store) {
            if(!store) return;
            try {
                store.removeItem(key);
            } catch(e) {

            }
        });
        if(supportsCookieStorage && !!domain) docCookies.removeItem(key, null, domain);
    };
    storage.set = function(key, value, domain, expires) {
        if(!value) return storage.removeItem(key);

        _.set(storage.__cached, key, value);

        // try to set the first avail storage
        var setValue = _.find(storage.__mechanisms, function(store) {
            if(!store) return null;
            try {
                store.setItem(key, JSON.stringify(value));
                return value;
            } catch(e) {
                return null;
            }
        });

        if(!!domain && supportsCookieStorage) {

            docCookies.setItem(key, JSON.stringify(value), expires, "/", domain, true);

        }

        return setValue;
    };
    storage.get = function(key) {
        return _.get(storage.__cached, key) || _.reduce(storage.__mechanisms, function(result, store) {
            if(!!result || !store) return result;
            //if(!store) return result;
            try {
                //console.log("store.getItem", store, key);
                var value = store.getItem(key);
                //console.log("store.getItem.value", value, key);
                if(!!value) return JSON.parse(value);
                return null;
            } catch(e) {
                return null;
            }
        }, null);
    };

    exports.Storage = storage;

    // durable storage?

}(ParkIQ.API));