(function(page, root, Delegate, api) {
    
    //var delegate = new Delegate(root);
    
    var lookups = {
		"vehicle":function(val) {
			return api.Vehicles.lookup(val, !!api.Vehicles.predefined, false);
		},
		"space":function(val) {
			return api.Spaces.lookup(val, !!api.Spaces.predefined, false);
		},
		"tenant":function(val) {
			return api.Tenants.lookup(val, !!api.Tenants.predefined, false);
		},
		"media":function(val) {
			return api.Media.lookup(val, !!api.Media.predefined, false);
		},
	};
    
    var bases = {
        "vehicle": "vehicles/",
        "tenant": "tenants/",
        "space": "spaces/",
        "media": "media/",
    };
    
    var builders = {
        "vehicle":function(html, item) {
            return html += '<li class="vehicle"><h1 class="vehicle id">' + (item.display || item.key) + '</h1><a href="./vehicles/' + (item.id || item.key) + '"></a></li>';
        },
        "tenant":function(html, item) {
            return html += '<li class="tenant"><h1 class="tenant id">' + (item.display || item.key) + '</h1><a href="./tenants/' + (item.id || item.key) + '"></a></li>';
        },
        "media":function(html, item) {
            return html += '<li class="media"><h1 class="media id">' + (item.display || item.key) + '</h1><a href="./media/' + (item.id || item.key) + '"></a></li>';
        },
        "space":function(html, item) {
            return html += '<li class="space"><h1 class="space id">' + (item.display || item.key) + '</h1><a href="./spaces/' + (item.id || item.key) + '"></a></li>';
        },
    };
    
    var prevVal = "";
    var form = document.querySelector("form.search.id");
    var label = document.querySelector("form.search.id label.id");
    var list = document.querySelector("form.search.id ul.datalist");
    var input = document.querySelector("form.search.id label.id input");
    var updated = document.querySelector("form.search.id time[data-valid]");

    var formDelegate = new Delegate(form);

    function isActive() {
        return root.getAttribute("data-action") === "search";
    }
    function activate() {
        if(!!document.body.scrollIntoView) document.body.scrollIntoView();
        root.setAttribute("data-action", "search");
    }
    function deactivate() {
        root.removeAttribute("data-action");
    }
    
    var searchEvent = "input";
	
	formDelegate.on(searchEvent, "label.id input[name]", function(e) {
        
        //console.log(e);
		
		// oninput takes care of actual changes, not just keypresses
        
        var name = this.getAttribute("name");
		var lookup = lookups[name];
		if(!lookup) return;
		var input = this;
        label = label || this.closest("label");
        form = form = this.closest("form");
        list = list || form.querySelector("ul.datalist");

		var val = input.value || "";
        //console.log("val", val);
        if(!!val) val = val.toUpperCase();
		//if(!val) while(list.firstChild) list.firstChild.remove();

        
		//console.log(val);
	
		lookup(val).then(function(results) {
			
			//console.log(tenants);
			
			var currentVal = input.value || "";
            //console.log("currentVal", currentVal);
            if(!!currentVal) currentVal = currentVal.toUpperCase();
			
            if(input.getAttribute("name") !== name) return; // type has changed
			if(results.query != currentVal) return; // runtime check to account for delay, if any
			
			var items = results.items;
			
			
			// render list
            list.innerHTML = _.reduce(items, builders[name], "");
            
            /*updated = updated || form.querySelector("time[data-valid]");
            updated.setAttribute("datetime", results.updated);
            updated.dispatchEvent(new CustomEvent("update", { bubbles: true, }));*/
			
		});
        
        form.dispatchEvent(new CustomEvent("track", {
            bubbles: true,
            detail: {
                category: "quicksearch", 
                action: "suggest", 
                label: val,
            },
        }));
		
	});
    formDelegate.on("focus", "label.id input[name]",function(e) {
        this.dispatchEvent(new CustomEvent(searchEvent, { bubbles: true, }));
        
        // activate
        activate();
        //form.dispatchEvent(new CustomEvent("activate", { bubbles: true, }));
        
        form.dispatchEvent(new CustomEvent("track", {
            bubbles: true,
            detail: {
                category: "quicksearch", 
                action: "activate", 
                label: "formfocus",
            },
        }));
    });

    /*
    delegate.on("click", function(e) {
        if(!form || !active()) return;
        if(!e.target) return;
        if(e.target.getAttribute("href") === "search") return;
        if(form.contains(e.target)) return;
        form.reset();
    });
    */
    
    // form submit
    form.addEventListener("submit", function(e) {
		e.preventDefault();
		
		//return; // we're not supporting this any futher
		
		form = form || checkbox.closest("form");
        input = input || form.querySelector("label.id input");
		
		
        var name = input.getAttribute("name");
        var base = bases[name];
        var lookup = lookups[name];
        if(!lookup) return; // no searcher
        
        input.blur();
        var val = input.value || "";
        //console.log("val", val);
        if(!val) return; // no value
        if(!!val) val = val.toUpperCase();
        
        lookup(val).then(function(results) {
			

			var items = results.items;
			if(!items || items.length <= 0) return; // no items to resolve to
            
            page.show((function(path) {
                var base = "/";
                var b = document.getElementsByTagName("base")[0];
                if(b) base = b.getAttribute("href");
                
                var url = base + path;
                //console.log(url);
                
                return url;
            })(base + items[0].id));
			
			form.reset();
		    //form.dispatchEvent(new CustomEvent("reset", { bubbles: true, }));
			
		});
        
        form.dispatchEvent(new CustomEvent("track", {
            bubbles: true,
            detail: {
                category: "quicksearch", 
                action: "submit", 
                label: val,
            },
        }));
        
       
	});
    
    // type change
    formDelegate.on("change", "fieldset input[type='radio']", function(e) {
        
        var checkbox = this;
        form = form || checkbox.closest("form");
        label = label || form.querySelector("label.id");
        input = input || label.querySelector("input");
        
        checkbox.blur();
        
        if(!checkbox.checked) return; // not checked
		
		var classes = _.each(checkbox.closest("fieldset").querySelectorAll("input"), function(input) {
			label.classList.remove(input.value);
		});
		label.classList.add(checkbox.value);
        input.setAttribute("name", checkbox.value);
        
        if(isActive()) input.focus();
        
        this.dispatchEvent(new CustomEvent(searchEvent, { bubbles: true, })); // re-search
		
	});
    
    // reset form
    form.addEventListener("reset", function(e) {
        
        //input.blur();
        
        if(!!document.activeElement) document.activeElement.blur();

        deactivate();
       
	});
    
    /*
    form.addEventListener("deactivate", function(e) {

        console.log("deactivate");
        console.log(e.target);
	
		//this.classList.remove("highlight");
		deactivate();
		
	});
    */
    
    formDelegate.on("click", "a", function(e) {
        
		console.log(this.href);
		
        form = form || this.closest("form");
        input = input || form.querySelector("label.id input");
        
        form.dispatchEvent(new CustomEvent("track", {
            bubbles: true,
            detail: {
                category: "quicksearch", 
                action: "suggestionclick", 
                label: input.value + "," + this.pathname,
            },
        }));
        

		form.reset();
		//form.dispatchEvent(new CustomEvent("reset", { bubbles: true, }));
		
	});
    
    /*
    delegate.on("click", "form.id.search:not(.active)", function(e) {
        
        form = form || this;
		
		//return;	
        
        //if(!!e.target.closest("button.deactivate")) return; // deactivate click
		
        form.dispatchEvent(new CustomEvent("activate", { bubbles: true, }));
        
        form.dispatchEvent(new CustomEvent("track", {
            bubbles: true,
            detail: {
                category: "quicksearch", 
                action: "activate", 
                label: "formclick",
            },
        }));
				
	});
    
    */
    
    /*
    form.addEventListener("activate",function(e) {
		
		//window.scrollTo(0, 0); // scroll top first
		form = form || this;
		activate();
        
        input = input || form.querySelector("label.id input");
        
		
		input.focus();
        
        //delegate.on("click", clickOutsideHandler);
		
	});
    */

    // handle global esc
    //delegate.on("keydown"
    root.addEventListener("keydown", function(e) {
		
		 
		
		if(e.keyCode != 27) return;
        if(!form || !isActive()) return;
        
        form.reset(); // triggers reset event
        //form.dispatchEvent(new CustomEvent("reset", { bubbles: true, }));
		
		//form.reset();
        form.dispatchEvent(new CustomEvent("track", {
            bubbles: true,
            detail: {
                category: "quicksearch", 
                action: "deactivate", 
                label: "esc",
            },
        }));
		
	});

    // handle outside click
    /*
    root.addEventListener("click", function(e) {
		if(!form || !e.target || !isActive()) return; // don't continue if not nav active
        if(form.contains(e.target)) return; // don't continue if clicking inside
        if(!!e.target.getAttribute && e.target.getAttribute("href") === "search") return; // search button
		form.reset();
	});
    */

    var rootDelegate = new Delegate(root);
    
    rootDelegate.on("click", "a[href='search']", function(e) {
        e.preventDefault();
        activate();
        input.focus();
        //form.dispatchEvent(new CustomEvent("activate", { bubbles:true }));
        //root.setAttribute("data-records-view", "search");
    });
	
}(page, document.documentElement, window.Delegate || window.domDelegate.Delegate, ParkIQ.API));