(function(root, Delegate, api, postal) {
    
    var delegate = new Delegate(root);
	
	var lookups = {
        "vehicle":!api.Vehicles ? null : function(val) {
			return api.Vehicles.lookup(val, true, false);
		},
		"tenant":!api.Tenants ? null : function(val) {
			return api.Tenants.lookup(val, true, !!api.Tenants.predefined);
		},
        "space":!api.Spaces ? null : function(val) {
			return api.Spaces.lookup(val, true, !!api.Spaces.predefined);
		},
        "media":!api.Media ? null : function(val) {
			return api.Media.lookup(val, true, false);
		},
	};
    
    var validators = {
        "tenant":!api.Tenants ? null : api.Tenants.isValid,
        "space":!api.Spaces ? null : api.Spaces.isValid,
        "media":!api.Media ? null : api.Media.isValid,
    };

    var searchEvent = "input";
	
	delegate.on(searchEvent, "main label.id input[name], aside label.id input[name]", function(e) {
        
        //console.log(e);
		
        // oninput takes care of actual changes, not just keypresses
        
        //console.log(api.Spaces.predefined);
        
        var name = this.getAttribute("name")
		var lookup = lookups[name];
		if(!lookup) return;
		var input = this;
        var label = this.closest("label");
        var list = label.nextSibling;
        if(!list || !list.matches || !list.matches("ul.datalist")) {
            
            list = document.createElement("ul");
            list.classList.add("datalist");
            label.after(list);
            
        }

		var val = input.value || "";
        if(!!val) val = val.toUpperCase();
		if(!val) while(list.firstChild) list.firstChild.remove();
        
        var constrained = label.classList.contains("constrained");

		var key = e.which || e.keyCode || e.charCode;
        var metakey = !!e.metaKey; // this isn't an issue on mobile
        
		//console.log(val);
	
		lookup(val).then(function(results) {
			
			//console.log(tenants);
			
			var currentVal = (input.value || "").toUpperCase();
			
			if(results.query != currentVal) return; // runtime check to account for delay, if any
			
			var items = results.items;
            
            //console.log(items);
            
            // if only one item and we haven't backspaced
            if(constrained && items.length == 1 && !metakey && key !== 8 && key !== 46) {
                input.value = items[0].display || items[0].key;
                while(list.firstChild) list.firstChild.remove(); //list.innerHTML = "";
				return;
            }
			
			// straight match
			if(!!_.find(items, { key: val })) {
				while(list.firstChild) list.firstChild.remove(); //list.innerHTML = "";
				return;
			}
			
			// render list
            list.innerHTML = _.reduce(items, function(html, item) {
				return html += '<li><button tabindex="-1" class="id" type="button" value="' + (item.display || item.key) + '">' + (item.display || item.key) + '</button></li>';
			}, "");
			
			
		})
		
	});
    delegate.on("focus", "label.id input[name]",function(e) {
        this.dispatchEvent(new CustomEvent(searchEvent, { bubbles: true, }));
    });

    delegate.on("click", "ul.datalist button", function(e) {
        
        var button = this;
        
        //console.log(button);
        var list = this.closest("ul");
        
        var el = list;
        while(el = el.previousSibling) {
            if(!el.matches("label.id")) continue;
            var label = el;
            var input = label.querySelector("input");
            input.value = button.value;
            input.dispatchEvent(new CustomEvent("change", { bubbles: true, }));
            
            break;
        }

		while(list.firstChild) list.firstChild.remove();
	});
    
    // check validity
    _.each([ "change", "blur" ], function(ev) {
        delegate.on(ev, "label.id.constrained input[name]:required", function(e) {
            
            // check the validity of the thing...
            var input = this;
            var name = input.getAttribute("name");
            var label = input.closest("label.id.constrained");
            
            var validator = validators[name];
            if(!validator) return;
            
            label.classList[!validator(input.value) ? "add" : "remove"]("invalid");
            
            
        });
    });
    delegate.on("input", "label.id.constrained.invalid input[name]:required", function(e) {
        var input = this;
        var label = input.closest("label.id.constrained");
        if(!!label) label.classList.remove("invalid");
    });

    var init = _.once(function(location) {
        if(!!_.get(location, "tenants.predefined")) {
                
            if(!!api.Tenants) api.Tenants.predefined = true;

            _.each(document.querySelectorAll("label.tenant.id"), function(label) {
                label.classList.add("constrained");
            });

        }
        
        if(!!_.get(location, "tenants.residents")) {

            _.each(document.querySelectorAll("option.tenant-visitor"), function(option) {
                option.textContent = option.textContent.replace("Tenant", "Resident").replace("visitor", "guest");
            });

        }
        
        if(!!_.get(location, "spaces.predefined")) {
            
            if(!!api.Spaces) api.Spaces.predefined = true;
            
                _.each(document.querySelectorAll("label.space.id"), function(label) {
                label.classList.add("constrained");
            });
        }
        
        if(!!_.get(location, "media.predefined")) {
            
            if(!!api.Media) api.Media.predefined = true;
            
                _.each(document.querySelectorAll("label.media.id"), function(label) {
                label.classList.add("constrained");
            });
        }
    });
	
    postal.subscribe({
		topic:"location.item",
		callback : function(data, envelope) {
			
			//return;
            var location = data.item;
            if(!!location) init(location);


	    },
	});

}(document.documentElement, window.Delegate || window.domDelegate.Delegate, ParkIQ.API, postal));