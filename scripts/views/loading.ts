import { Child, div, isChild } from '../utils/dom';
import { logo, LogoStyle, isLogoStyle } from './logo';

interface LoadingOptions {
    style?: LogoStyle;
    loadingContent?: Child;
}

export function loading({ style, loadingContent }: LoadingOptions = {}): HTMLDivElement {
    const logoStyle: LogoStyle = isLogoStyle(style) ? style : 'dark';
    if (loadingContent === undefined) {
        loadingContent = isChild(style) ? style : 'Loading...';
    }
    return div({ class: 'loading' },
        logo(logoStyle),
        div({ class: 'text' }, loadingContent),
    );
}
