import page from 'page';
import { div, p } from '../utils/dom';

export function notFound(path: string) {
    const lastKnownValid = location.pathname.replace(path, '').replace(/\/$/, '');
    page.replace(lastKnownValid, null, false, false);;
    return div({ class: 'not-found' },
        p("Sorry! We couldn't find what you were looking for."),
        p(`'${location.pathname}/${path}' isn't in our system`),
    );
}
