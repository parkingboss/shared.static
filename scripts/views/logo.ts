import { div } from '../utils/dom';

export type LogoStyle = 'light' | 'dark';

export function isLogoStyle(x: any): x is LogoStyle {
    return x === 'light' || x === 'dark';
}

export function logo(style: LogoStyle) {
    return div({ class: `logo logo-${style}`});
}
