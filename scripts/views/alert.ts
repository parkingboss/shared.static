import * as colors from '../utils/colors';
import { Color } from '../utils/colors';
import { Child, div, i } from '../utils/dom';
import { cl } from '../utils/helpers';

export type AlertLevel = 'success' | 'danger' | 'warning' | 'info';

export interface AlertStyle {
    bg: Color;
    fg: Color;
    border?: Color;
}

export type AlertOptions = AlertLevel | AlertStyle;

const levelToStyleMap: { [key in AlertLevel ]: AlertStyle } = {
    success: {
        bg: colors.LightGreen,
        fg: colors.DarkGreen,
    },
    danger: {
        bg: colors.LightRed,
        fg: colors.DarkRed,
    },
    warning: {
        bg: colors.LightYellow,
        fg: colors.DarkYellow,
    },
    info: {
        fg: colors.LightBlue,
        bg: colors.DarkBlue,
    },
};

function levelToStyle(level: AlertLevel): AlertStyle {
    return levelToStyleMap[level];
}

export function alert(style: AlertOptions, content: Child, showClose: boolean = true) {
    style = typeof style === 'string' ? levelToStyle(style) : style;
    const classes = cl(
        'alert',
        style.bg.bgClass,
        style.fg.textClass,
        style.border && style.border.borderClass,
    );

    function closeAlert(evt: Event) {
        if (!(evt.target instanceof Element)) return;

        const isCloseIcon = evt.target.nodeName === 'I' &&
            evt.target.classList.contains('fa-window-close');
        if (!isCloseIcon) return;

        const isDirectChild = evt.target.parentElement === result;
        if (!isDirectChild) return;

        if (result.parentNode) {
            result.parentNode.removeChild(result);
        }
    }

    const result = div({ class: classes, events: { click: closeAlert } },
        showClose && i({ class: 'fas fa-window-close' }),
        content,
    );
    return result;
}
