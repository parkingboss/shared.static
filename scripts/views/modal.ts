import { Child, clear, div, replaceContent } from '../utils/dom';

const lightbox = div({
    id: 'lightbox',
    style: { display: 'none' },
});
document.body.appendChild(lightbox);
lightbox.addEventListener('click', () => closeModal());

export function closeModal() {
    clear(lightbox);
    lightbox.style.display = 'none';
    document.body.style.removeProperty('overflow');
}

export function showModal(...children: Child[]) {
    const content = div({ class: 'modal', events: { click: (evt: Event) => evt.stopPropagation() } }, children);
    replaceContent(lightbox, content);

    lightbox.style.removeProperty('display');
    document.body.style.overflow = 'hidden';
}
