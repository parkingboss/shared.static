// tslint:disable:ordered-imports
import page from 'page';
window.page = page;

import * as postal from 'postal';
window.postal = postal;

import * as qs from 'qs';
window.qs = qs;

import 'shared.static/scripts/js/api.init.js';
import 'shared.static/scripts/js/api.storage.js';
import 'shared.static/scripts/js/api.auth.js';
import 'shared.static/scripts/js/api.auth.context.js';
// tslint:enable:ordered-imports

declare global {
    var ParkIQ: {
        API: any;
    };
    interface Window {
        postal: typeof postal;
        page: typeof page;
        qs: typeof qs;
    }
}
