import { Cancelled } from './Cancelled';

export class Canceller {
    constructor(private readonly cancelEventName?: string) {
        this.cancel = this.cancel.bind(this);
        if (this.cancelEventName) {
            document.body.addEventListener(this.cancelEventName, this.cancel);
        }
    }

    private isCancelled: boolean = false;
    public get cancelled(): boolean {
        return this.isCancelled;
    }

    public cancel(): void {
        this.isCancelled = true;
        this.stopListening();
    }

    public stopListening(): void {
        if (this.cancelEventName) {
            document.body.removeEventListener(this.cancelEventName, this.cancel);
        }
    }

    public throwIfCancelled(): void {
        if (this.cancelled) {
            throw new Cancelled();
        }
    }
}
