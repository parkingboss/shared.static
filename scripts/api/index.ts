import { format } from 'date-fns';
import { Canceller } from './Canceller';
import { HttpException } from '../exceptions/HttpException';
import { NetworkException } from '../exceptions/NetworkException';
import './legacy_api';
import { Items } from '../utils/helpers';

export type HTTPMethod = 'GET' | 'DELETE' | 'POST' | 'PUT';

export function isHTTPMethod(x: any): x is HTTPMethod {
    return x === 'GET' || x === 'DELETE' || x === 'POST' || x === 'PUT';
}

declare global {
    const API_ROOT: string;
    const MODE: string;
    const DEV: boolean;
    const PROD: boolean;
    const NAVIGATE_EVENT_NAME: string;
}

interface FetchOptions {
    body?: BodyInit;
    qs?: Items<string>;
    force?: boolean;
    cancelOnNavigate?: boolean;
}

interface ExecuteParams {
    body?: BodyInit;
    qs?: Items<string>;
}

class Api {
    private cache: Items<string> = {};

    constructor(private bearerToken: string) {
    }

    public async fetch<T>(path: string, { qs, force, cancelOnNavigate }: FetchOptions = {}): Promise<T> {
        let cancel: Canceller|undefined;
        try {
            const fromCache = this.getCached<T>(path, qs, force);

            if (fromCache != null) {
                return fromCache;
            }

            const url = this.buildUrl(path, qs);
            if (cancelOnNavigate) {
                cancel = new Canceller(NAVIGATE_EVENT_NAME);
            }
            let response: Response;
            try {
                response = await fetch(url);
            } catch (e) {
                this.checkCancelled(cancel);
                throw new NetworkException(e.message, e);
            }

            if (!response.ok) {
                this.checkCancelled(cancel);
                throw new HttpException(`Failed to fetch data from ${path}`, response);
            }

            const result = this.setCached<T>(await response.json(), path, qs);
            this.checkCancelled(cancel);
            return result;
        } finally {
            if (cancel) cancel.stopListening();
        }
    }

    public async execute(method: HTTPMethod , path: string, { body, qs }: ExecuteParams): Promise<Response> {
        let response: Response;
        try {
            response = await fetch(this.buildUrl(path, qs), { method, body });
        } catch (e) {
            throw new NetworkException(e.message, e);
        }

        if (!response.ok) {
            throw new HttpException(`Failed to ${method} ${path}`, response);
        }

        return response;
    }

    public async post<R>(path: string, { body, qs }: ExecuteParams): Promise<R> {
        const response  = await this.execute('POST', path, { body, qs });
        return await response.json();
    }

    public async put<R>(path: string, { body, qs }: ExecuteParams): Promise<R> {
        const response = await this.execute('PUT', path, { body, qs });
        return await response.json();
    }

    public async del(path: string, qs?: Items<string>): Promise<void> {
        await this.execute('DELETE', path, { qs });
    }

    public getCached<T>(path: string, qs?: Items<string>, force?: boolean): T | null {
        const key = this.cacheKey(path, qs);

        if (!this.cache[key]) {
            return null;
        } else if (force) {
            delete this.cache[key];
            return null;
        } else {
            return JSON.parse(this.cache[key]) as T;
        }
    }

    public flush(path: string, qs?: Items<string>): void {
        const key = this.cacheKey(path, qs);

        delete this.cache[key];
    }

    public setCached<T>(result: T, path: string, qs?: Items<string> | undefined): T {
        const key = this.cacheKey(path, qs);

        this.cache[key] = JSON.stringify(result);

        return result;
    }

    private cacheKey(path: string, qs?: Items<string>): string {
        const url = new URL(`${API_ROOT}/${path}`);

        if (qs) {
            Object.keys(qs).forEach(key => url.searchParams.append(key, qs[key]));
        }

        return url.toString();
    }

    private buildUrl(path: string, qs?: Items<string>): string {
        const url = new URL(`${API_ROOT}/${path}`);

        url.searchParams.append('Authorization', this.bearerToken);

        if (qs) {
            Object.keys(qs).forEach(key => {
                url.searchParams.append(key, qs[key]);
            });
        }
        if (!url.searchParams.has('viewpoint')) {
            url.searchParams.append('viewpoint', format(new Date()));
        }

        return url.toString();
    }

    private checkCancelled(cancel?: Canceller) {
        if (cancel) cancel.throwIfCancelled();
    }
}

export const api: Promise<Api> = ParkIQ.API.Auth.header()
    .then(
        (t: string) => new Api(t),
        (): Api|undefined => {
            if (PROD) {
                page('https://my.parkingboss.com');
                return;
            } else {
                return new Api(`bearer ${prompt('Enter your bearer token')}`);
            }
        },
    );

// In Dev we want this available for mucking about with
declare global {
    interface Window {
        api: Api;
    }
}
if (DEV) {
    api.then((a: Api) => {
        window.api = a;
    });
}
